package com.lbtek.thinkpositivestaynegative;

import android.app.Application;

import com.airbnb.android.react.maps.MapsPackage;
// import com.facebook.react.BuildConfig;
import com.facebook.react.ReactApplication;
import com.oblador.vectoricons.VectorIconsPackage;
import com.BV.LinearGradient.LinearGradientPackage;
import org.reactnative.camera.RNCameraPackage;
import com.zoontek.rnpermissions.RNPermissionsPackage;
import com.reactnativerestart.RestartPackage;
import com.reactnativecommunity.asyncstorage.AsyncStoragePackage;
import com.facebook.react.ReactNativeHost;
import com.facebook.react.ReactPackage;
import com.facebook.react.shell.MainReactPackage;
import com.facebook.soloader.SoLoader;
import com.imagepicker.ImagePickerPackage;
import com.reactnativecommunity.geolocation.GeolocationPackage;
import com.reactnativecommunity.netinfo.NetInfoPackage;
import com.reactnativecommunity.webview.RNCWebViewPackage;
import com.showlocationservicesdialogbox.LocationServicesDialogBoxPackage;
import com.marianhello.bgloc.react.BackgroundGeolocationPackage;
import java.util.Arrays;
import java.util.List;
import com.horcrux.svg.SvgPackage;
import cl.json.RNSharePackage;
import cl.json.ShareApplication;
import io.invertase.firebase.RNFirebasePackage;
import io.invertase.firebase.auth.RNFirebaseAuthPackage;
import io.invertase.firebase.messaging.RNFirebaseMessagingPackage;
import io.invertase.firebase.notifications.RNFirebaseNotificationsPackage;
import com.apsl.versionnumber.RNVersionNumberPackage;
import com.learnium.RNDeviceInfo.RNDeviceInfo; 
import co.reclub.android.whitelist.RNAndroidWhitelistPackage;
import com.reactlibrary.RNDisableBatteryOptimizationsPackage;
import android.view.WindowManager;
import android.content.res.Configuration;
import android.content.Context;
import android.util.DisplayMetrics;
import com.reactcommunity.rndatetimepicker.RNDateTimePickerPackage;
import com.reactnativecommunity.checkbox.ReactCheckBoxPackage;
import com.henninghall.date_picker.DatePickerPackage;
import com.tuanpm.RCTMqtt.RCTMqttPackage;
import com.th3rdwave.safeareacontext.SafeAreaContextPackage;
import br.com.dopaminamob.gpsstate.GPSStatePackage;
import com.tkporter.sendsms.SendSMSPackage;

public class MainApplication extends Application implements ShareApplication, ReactApplication {

  private final ReactNativeHost mReactNativeHost = new ReactNativeHost(this) {
    @Override
    public boolean getUseDeveloperSupport() {
      return BuildConfig.DEBUG;
    }

    @Override
    protected List<ReactPackage> getPackages() {

      return Arrays.<ReactPackage>asList(
            SendSMSPackage.getInstance(),
            new MainReactPackage(),
            new VectorIconsPackage(),
            new LinearGradientPackage(),
            new RNCameraPackage(),
            new RNPermissionsPackage(),
            new RestartPackage(),
            new RCTMqttPackage(),
            new AsyncStoragePackage(),
            new NetInfoPackage(),
            new RNDisableBatteryOptimizationsPackage(),
            new GeolocationPackage(),
            new RNCWebViewPackage(),
            new RNSharePackage(),
            new MapsPackage(),
            new ImagePickerPackage(),
            new RNFirebasePackage(),
            new LocationServicesDialogBoxPackage(),
            new RNFirebaseAuthPackage(),
            new RNFirebaseMessagingPackage(),
            new RNFirebaseNotificationsPackage(),
            new BackgroundGeolocationPackage(),
            new MyAppPackage(),
            new SvgPackage(),
            new RNDeviceInfo(), 
            new RNVersionNumberPackage(),
            new RNAndroidWhitelistPackage(),
            new RNDateTimePickerPackage(),
            new ReactCheckBoxPackage(),
            new DatePickerPackage(),
            new SafeAreaContextPackage(),
            new GPSStatePackage()
      );
    }

    @Override
    protected String getJSMainModuleName() {
      return "index";
    }
  };

  @Override
  public ReactNativeHost getReactNativeHost() {
    return mReactNativeHost;
  }

  @Override
  public void onCreate() {
    super.onCreate();
    SoLoader.init(this, /* native exopackage */ false);
    adjustFontScale(getApplicationContext(),getResources().getConfiguration());
  }
  
  @Override
  public String getFileProviderAuthority() {
    return "com.lbtek.thinkpositivestaynegative.provider";
  }

  public void adjustFontScale(Context context, Configuration configuration) {
    if (configuration.fontScale != 1) {
      configuration.fontScale = 1;
      DisplayMetrics metrics = context.getResources().getDisplayMetrics();
      WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
      wm.getDefaultDisplay().getMetrics(metrics);
      metrics.scaledDensity = configuration.fontScale * metrics.density;
      context.getResources().updateConfiguration(configuration, metrics);
    }
  }
}
