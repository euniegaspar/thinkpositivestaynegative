/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React from "react";
import { BASE_STACK_NAVIGATOR } from "./app/components/RootNavigator";
import { createStore, combineReducers } from "redux";
import { Provider } from "react-redux";
import { userOperations } from "./app/redux/reducers/UserReducer";
import { navigationOperation } from "./app/redux/reducers/NavigationReducer";
import { appVersionOperation } from "./app/redux/reducers/AppVersionReducer";
import firebase from "react-native-firebase";
import { View } from "react-native";
import { showDialogue } from "./app/utils/CMAlert";
import { StackActions, NavigationActions, createStackNavigator } from "react-navigation";
import AsyncStorage from "@react-native-community/async-storage";
import NavigationService from "./NavigationService";
import {
  NOTIFICATION_TYPE,
  DEFAULT_TYPE,
} from "./app/utils/Constants";
import AndroidWhitelist from 'react-native-android-whitelist';
import RNDisableBatteryOptimizationsAndroid from 'react-native-disable-battery-optimizations-android';
import store from "./app/redux/store";
import { saveUserFCM } from "./app/utils/AsyncStorageHelper";


const config = {
  title: 'Think Positive, Stay Negative',
  text: 'To ensure timely delivery of push notifications, please whitelist our app.',
  doNotShowAgainText: 'Do not show again',
  positiveText: 'Whitelist',
  negativeText: 'Cancel'
}
AndroidWhitelist.alert(config)

RNDisableBatteryOptimizationsAndroid.isBatteryOptimizationEnabled().then((isEnabled)=>{
	if(isEnabled){
		RNDisableBatteryOptimizationsAndroid.openBatteryModal();
	}
});
RNDisableBatteryOptimizationsAndroid;

export default class App extends React.Component {
  constructor(props) {
    super(props);
    this.isNotification = undefined;
  }

  state = {
    isRefresh: false
  };

 
  //1
  async checkPermission() {
    const enabled = await firebase.messaging().hasPermission();
    if (enabled) {
      this.getToken();
    } else {
      this.requestPermission();
    }
  }

  //3
  async getToken() {
    fcmToken = await firebase.messaging().getToken();
  }

  //2
  async requestPermission() {
    try {
      await firebase.messaging().requestPermission();
      this.getToken();
    } catch (error) {
      // User has rejected permissions
    }
  }

  componentWillUnmount() {
    try {
      this.notificationListener();
      this.notificationOpenedListener();
      AppState.removeEventListener('change', this._handleAppStateChange);
    } catch (error) { }
  }

  async createNotificationListeners() {
    /*
     * Triggered when a particular notification has been received in foreground
     * */
    this.notificationListener = firebase
      .notifications()
      .onNotification(notification => {
        const { title, body, data } = notification;
        console.log("APP FOREGROUND NOTIFICATION TYPE :::::::::::::::: ", notification)
        showDialogue(body, [], "", () => {
          if (data.screenType === "noti") {
            NavigationService.navigateToSpecificRoute("NotificationContainer");
          } 
        })
      });

    /*
     * If your app is in background, you can listen for when a notification is clicked / tapped / opened as follows:
     * */
    
    this.notificationOpenedListener = firebase
      .notifications()
      .onNotificationOpened(notificationOpen => {
        const { title, body, data } = notificationOpen.notification;
        console.log("APP BACKGROUND NOTIFICATION OPEN TYPE :::::::::::::::: ", notificationOpen.notification)        
        if (data.screenType === "noti") {
          NavigationService.navigateToSpecificRoute("NotificationContainer")
        }
    });

    /*
     * If your app is closed, you can check if it was opened by a notification being clicked / tapped / opened as follows:
     * */
    const notificationOpen = await firebase
      .notifications()
      .getInitialNotification();
    if (notificationOpen) {
      const {
        title,
        body,
        data,
        notificationId
      } = notificationOpen.notification;

      const lastNotification = await AsyncStorage.getItem("lastNotification");
      console.log("APP CLOSE NOTIFICATION DATA :::::::::::::::: ", data)
      if (lastNotification !== notificationId) {
        if (data.screenType === "noti") {
          this.isNotification = NOTIFICATION_TYPE;
          this.setState({ isRefresh: this.state.isRefresh ? false : true });
          NavigationService.navigate("NotificationContainer");
        } 
        await AsyncStorage.setItem("lastNotification", notificationId);
      }
    }
    /*
     * Triggered for data only payload in foreground
     * */
    this.messageListener = firebase.messaging().onMessage(message => {
      //process data message
    });

    if (this.isNotification == undefined) {
      this.isNotification = DEFAULT_TYPE;
      this.setState({ isRefresh: this.state.isRefresh ? false : true });
    }
  }

  async componentDidMount() {
    try {
      this.checkPermission();
      this.createNotificationListeners();  
      AppState.addEventListener('change', this._handleAppStateChange);
    } catch (error) { }
  }

  _handleAppStateChange = async (nextAppState) => {
    if (this.state.appState.match(/inactive|background/) && nextAppState === 'active') {
      // firebase.notifications().setBadge(0);
      this.checkPermission();
      this.createNotificationListeners();
      console.log("it went here _handleAppStateChange in app.js");
    }
  };

  render() {
    return (
      <Provider store={store}>
        {this.isNotification != undefined ? (
          <BASE_STACK_NAVIGATOR
            ref={navigatorRef => {
              NavigationService.setTopLevelNavigator(navigatorRef);
            }}
            screenProps={this.isNotification}
          />
          ) : (
            <View />
          )}
      </Provider>
    );
  }
}