import React from "react";
import { View, Text, StyleSheet } from "react-native";
import { ETFonts } from "../assets/FontConstants";
import { EDColors } from "../assets/Colors";
import GlobalFont from 'react-native-global-font'
import RadioGroupButton from "./RadioGroupButton";

export default class RadioGroupWithHeader extends React.PureComponent {

  constructor(props) {
    super(props);
    GlobalFont.applyGlobal(ETFonts.regular)
  }

  state = {
    data: this.props.data,
    selected: ""
  };

  componentDidMount() {
    GlobalFont.applyGlobal(ETFonts.regular)
  }

  onPress = data => {};

  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.titleTxt}>{this.props.titleTxt}</Text>
        <RadioGroupButton 
          radioButtons={this.state.data}
          onPress={data => {
            this.setState({ data });
            this.state.data.find(e => {
              if (e.selected == true) {
                this.props.onSelected(e.label);
              }
            });
          }}/>
      </View>
    );
  }
}

export const styles = StyleSheet.create({
  container: {
    borderRadius: 10,
    padding: 10,
    backgroundColor: EDColors.superPaleGray,
    margin: 10,
    borderWidth: 0.5,
    borderColor: EDColors.buttonBorderColor
  },
  titleTxt: {
    fontFamily: ETFonts.bold,
    color: EDColors.black,
    fontSize: 16
  }
});
