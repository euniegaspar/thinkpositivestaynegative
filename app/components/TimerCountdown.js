import React, { Component } from 'react';
import { Text, StyleSheet, TouchableOpacity, View } from 'react-native';
import { EDColors } from "../assets/Colors";
import { ETFonts } from "../assets/FontConstants";

class TimerCountdown extends Component {
    constructor(props) {
        super(props);
        this.state = {
            timer: props.initialTime,
        }
        this.tickingInterval = true;
        this.reset = false;
        this.interval = null;
    }

    startTimeInterval() {
        if(this.interval == null) {
            this.interval = setInterval(
                () => this.setState((prevState) => ({ timer: prevState.timer - 1000 })),
                1000
            );
        }
    }

    resetTimeInterval() {
        if(!this.reset) {
            this.reset = true;
            this.tickingInterval = true;
            this.setState({ timer: this.props.initialTime });
            setTimeout(() => {
                this.startTimeInterval();
            }, 1000);
        }
    }

    componentDidMount() {
        this.startTimeInterval();
    }

    componentDidUpdate() {
        if (this.state.timer === 0) {
            clearInterval(this.interval);
            if(this.tickingInterval) {
                this.tickingInterval = false;
                this.reset = false;
                this.interval = null;
                this.setState({ timer: 0 });
            }
        }
    }

    componentWillUnmount() {
        clearInterval(this.interval);
        if(this.tickingInterval) {
            this.tickingInterval = false;
            this.reset = false;
            this.interval = null;
            this.setState({ timer: 0 });
        }
    }

    format(duration) {
        let seconds = Math.floor((duration / 1000) % 60);
        let minutes = Math.floor((duration / (1000 * 60)) % 60);

        minutes = minutes < 1 ? '00' : minutes < 10 ? `0${minutes}` : minutes;
        seconds = seconds < 1 ? '00' : seconds < 10 ? `0${seconds}` : seconds;
        return `${minutes}:${seconds}`;
    }

    render() {
        return (
            <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                <Text style={this.props.style}> {this.format(this.state.timer)} </Text>
                <View style={styles.loginButtonLabelContainer}>
                    <Text
                        style={[styles.button, { marginRight: 5 }]}>
                        Didn't receive it?
                    </Text>
                    <TouchableOpacity
                        disabled={this.tickingInterval}
                        activeOpacity={this.tickingInterval ? 0.7 : 1.0}
                        onPress={() => {
                            this.resetTimeInterval();
                            this.props.resendOTP();
                        }}>
                        <Text
                            style={[styles.button, { color: this.tickingInterval ? EDColors.lightGray : EDColors.black, textDecorationLine: this.tickingInterval ? "none" : "underline" }]}>
                            {this.tickingInterval ? "Request Code Sent" : "Request New Code"}
                        </Text>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
}

export default TimerCountdown;

const styles = StyleSheet.create({
    loginButtonLabelContainer: {
        flexDirection: 'row'
    },
    button: {
        marginTop: 8,
        marginBottom: 10,
        color: EDColors.black,
        fontSize: 16,
        fontFamily: ETFonts.regular
    },
});