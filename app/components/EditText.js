import React from "react";
import {
  View,
  StyleSheet,
  TextInput,
  Platform,
  Text
} from "react-native";
import { EDColors } from "../assets/Colors";
import { ETFonts } from "../assets/FontConstants";

export default class EditText extends React.Component {
  
  constructor(props) {
    super(props);
    Text.defaultProps = Text.defaultProps || {};
    Text.defaultProps.allowFontScaling = false;
    TextInput.defaultProps = TextInput.defaultProps || {};
    TextInput.defaultProps.allowFontScaling = false;
  }

  render() {
    return (
      <View style={[ this.props.withBorder === "true"? styles.borderET: '', this.props.fromOrder === "true"? styles.orderReviewET : styles.editTextView, this.props.styles ]}>
        <TextInput
          style={{ color: this.props.fromAddress === "true"? EDColors.black : '', marginVertical: Platform.OS == 'ios' ? 10 : 0 },
            [styles.textInput]
          }
          keyboardType={this.props.keyboardType}
          secureTextEntry={this.props.secureTextEntry}
          editable={this.props.editable}
          maxLength={this.props.maxLength != undefined ? this.props.maxLength : 30}
          multiline={this.props.multiline != undefined ? this.props.multiline : false}
          autoCorrect={false} 
          autoCapitalize={"none"}
          onChangeText={userText => {
            if (this.props.onChangeText != undefined) {
              this.props.onChangeText(userText);
            }
          }}
          value={this.props.value}
          placeholderTextColor={EDColors.textboxColor}
          placeholder={this.props.hint}
          returnKeyType="done"
          allowFontScaling={false}
        />
      </View>
    );
  }
}

export const styles = StyleSheet.create({
  editTextView: {
    flexDirection: 'row',
    backgroundColor: EDColors.white,
    borderRadius: 10,
    borderWidth: 1,
    borderColor: EDColors.textboxColor,
    marginTop: 10
  },
  textInput: {
    flex: 1, 
    marginLeft: 10, 
    marginRight: 10, 
    fontSize: 16, 
    fontFamily: ETFonts.regular,
    paddingVertical: 7
  },
  orderReviewET: {
    flexDirection: 'row',
    backgroundColor: EDColors.white,
    borderTopLeftRadius: 4,
    borderTopRightRadius: 4,
    borderBottomLeftRadius: 4,
    borderBottomRightRadius: 4,
    marginTop: 10,
    paddingHorizontal: 10,
    fontSize: 16,
    elevation: 5,
    borderBottomWidth: 2,   
    borderBottomColor: EDColors.buttonFillColor
  },
  borderET: {
    borderWidth: 1,
    borderColor: EDColors.paleGray
  }
});
