import React, { Component } from "react";
import { View, Modal, Animated, StyleSheet } from "react-native";
import { Svg, Circle } from 'react-native-svg'
import PropTypes from 'prop-types';

class CircularLoading extends Component {
    constructor(props) {
        super(props);
        this.loadingSpin = new Animated.Value(0);
    }

    componentDidMount() {
        this.spinAnimation();
        console.log("loadingSpin");
    }

    spinAnimation() {
        this.loadingSpin.setValue(0);
        Animated.sequence([
            Animated.timing(
                this.loadingSpin,
                {
                    toValue: 1,
                    duration: 500
                }
            )
        ]).start(() => this.spinAnimation());
    }

    render() {
        const {
            size,
            strokeWidth,
            strokeColor,
            loading
        } = this.props;
        const degree = 270;
        const radius = (size - strokeWidth) / 2;
        const circum = radius * 2 * Math.PI;
        const svgProgress = 100 - degree;
        const spin = this.loadingSpin.interpolate({
            inputRange: [0, 1],
            outputRange: ['0deg', '360deg']
        });
        return (
            <Modal
                transparent={true}
                animationType='none'
                visible={loading}
                onRequestClose={() => {}}>
                <View style={styles.modalBackground}>
                    <Animated.View style={[styles.activityIndicatorWrapper, {height: size, width: size, borderRadius: size / 2 }, { transform: [{ rotate: spin }]}]}>
                        <Svg width={size} height={size}>
                            <Circle
                                stroke="transparent"
                                fill="none"
                                cx={size / 2}
                                cy={size / 2}
                                r={radius}
                                {...{ strokeWidth }} />

                            <Circle
                                stroke={strokeColor}
                                fill="none"
                                cx={size / 2}
                                cy={size / 2}
                                r={radius}
                                strokeDasharray={`${circum} ${circum}`}
                                strokeDashoffset={radius * Math.PI * 2 * (svgProgress / 100)}
                                strokeLinecap="round"
                                transform={`rotate(-90, ${size / 2}, ${size / 2})`}
                                {...{ strokeWidth }} />
                        </Svg>
                    </Animated.View>
                </View>
            </Modal>
        )
    }
}

CircularLoading.propTypes = {
    size: PropTypes.number.isRequired,
    strokeWidth: PropTypes.number.isRequired,
    strokeColor: PropTypes.string.isRequired,
    loading: PropTypes.bool || true
}

export default CircularLoading;

const styles = StyleSheet.create({
    modalBackground: {
        flex: 1,
        alignItems: "center",
        justifyContent: "center",
        backgroundColor: 'rgba(0, 0, 0, 0.6)'
    },
    activityIndicatorWrapper: {
        backgroundColor: "transparent",
        borderRadius: 10,
        alignItems: "center",
        justifyContent: "center"
    }
});
