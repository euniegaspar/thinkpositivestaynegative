import React, { Component } from "react";
import { Text, TouchableOpacity, StyleSheet } from "react-native";
import { EDColors } from "../assets/Colors";
import { ETFonts } from "../assets/FontConstants";

export default class EDThemeButton extends Component {

  constructor(props) {
    super(props);
    Text.defaultProps = Text.defaultProps || {};
    Text.defaultProps.allowFontScaling = false;
  }

  buttonWidth = this.props.buttonWidth || 350;
  fontSizeNew = this.props.fontSizeNew || 16;
  buttonHeight = this.props.buttonHeight || 40;
  
  render() {
    return (
      <TouchableOpacity
        pointerEvents={this.props.isDisable ? "none" : ""}
        style={[
          this.props.style || {},
          this.props.borderButton
            ? styles.themeButton
            : styles.themeButton,
          {
            width: this.buttonWidth,
            fontSize: this.fontSizeNew,
            height: this.buttonHeight
          }
        ]}
        onPress={this.props.onPress}>
        <Text
          style={
            this.props.borderButton
              ? styles.themeButtonTextBorder
              : styles.themeButtonText
          }>
          {this.props.label}
        </Text>
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  themeButton: {
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: EDColors.primary,
    borderRadius: 25
  },
  themeButtonText: {
    color: EDColors.white,
    textAlign: "center",
    fontFamily: ETFonts.bold,
    fontSize: 14
  },
  themeButtonBorder: {
    padding: 8,
    backgroundColor: EDColors.white,
    borderRadius: 10,
    borderWidth: 1,
    borderColor: EDColors.primary
  },
  themeButtonTextBorder: {
    color: EDColors.black,
    textAlign: "center",
    fontFamily: ETFonts.bold,
    fontSize: 14
  }
});
