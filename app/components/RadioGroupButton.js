import React, { Component } from 'react';
import { View, TouchableOpacity, Text, StyleSheet } from 'react-native';
import { EDColors } from "../assets/Colors";
import { ETFonts } from "../assets/FontConstants";

export default class RadioGroupButton extends Component {

    constructor(props) {
        super(props);
        this.state = {
            radioButtons: this.validate(this.props.radioButtons),
        };
    }

    validate(data) {
        let selected = false; // Variable to check if "selected: true" for more than one button.
        data.map(e => {
            e.color = e.color ? e.color : '#444';
            e.disabled = e.disabled ? e.disabled : false;
            e.label = e.label ? e.label : 'You forgot to give label';
            e.layout = e.layout ? e.layout : 'row';
            e.selected = e.selected ? e.selected : false;
            if (e.selected) {
                if (selected) {
                    e.selected = false; // Making "selected: false", if "selected: true" is assigned for more than one button.
                    console.log('Found "selected: true" for more than one button');
                } else {
                    selected = true;
                }
            }
            e.size = e.size ? e.size : 24;
            e.value = e.value ? e.value : e.label;
        });
        if (!selected) {
            data[0].selected = true;
        }
        return data;
    }

    onPress = label => {
        const radioButtons = this.state.radioButtons;
        const selectedIndex = radioButtons.findIndex(e => e.selected == true);
        const selectIndex = radioButtons.findIndex(e => e.label == label);
        if (selectedIndex != selectIndex) {
            radioButtons[selectedIndex].selected = false;
            radioButtons[selectIndex].selected = true;
            this.setState({ radioButtons });
            this.props.onPress(this.state.radioButtons);
        }
    };

    render() {
        return (
            <View>
                <View style={styles.container}>
                    {this.state.radioButtons.map(data => {
                        return (
                            <RadioButton
                                key={data.label}
                                data={data}
                                onPress={this.onPress}
                            />
                        );
                    })}
                </View>
            </View>
        );
    }
}

class RadioButton extends Component {
    render() {
        const data = this.props.data;
        const opacity = data.disabled ? 0.2 : 1;
        return (
            <TouchableOpacity
                key={data.label}
                style={[{ flexDirection: 'row', flexWrap: 'wrap' }, { opacity: opacity, marginVertical: 5 }]}
                onPress={() => {
                    data.disabled ? null : this.props.onPress(data.label);
                }}>
                <View
                    style={[{borderWidth: 2, justifyContent: 'center', alignItems: 'center'}, styles.radioCircle]}>
                    {data.selected && <View style={styles.selectedRb} />}
                </View>
                <Text style={styles.radioText}>{data.label}</Text>
            </TouchableOpacity>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        marginTop: 15,
        flexDirection: "row",
        marginLeft: 10,
        flexWrap: "wrap"
    },
    radioText: {
        marginRight: 10,
        marginLeft: 10,
        fontSize: 14,
        color: "#111111",
        fontFamily: ETFonts.regular,
    },
    radioCircle: {
        height: 16,
        width: 16,
        borderRadius: 8,
        borderWidth: 2,
        borderColor: EDColors.primary,
        alignItems: 'center',
        justifyContent: 'center',
    },
    selectedRb: {
        width: 8,
        height: 8,
        borderRadius: 4,
        backgroundColor: EDColors.primary,
    },
    result: {
        marginTop: 20,
        color: EDColors.white,
        fontWeight: '600',
        backgroundColor: '#F3FBFE',
    },
});