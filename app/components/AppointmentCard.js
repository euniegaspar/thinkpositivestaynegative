import React, { Component } from "react";
import { View, Image, Text, TouchableOpacity, StyleSheet, FlatList, TouchableNativeFeedbackBase } from "react-native";
import Assets from "../assets";
import { ETFonts } from "../assets/FontConstants";
import { EDColors } from '../assets/Colors'
import LinearGradient from 'react-native-linear-gradient';

export default class AppointmentCard extends Component {

  constructor(props) {
    super(props);
    Text.defaultProps = Text.defaultProps || {};
    Text.defaultProps.allowFontScaling = false;

    this.state = {
    };
  }

  render() {
    return (
      <TouchableOpacity
        style={styles.appntmntBtn}
        onPress={() => {
          this.props.onPress();
        }}>
        <View style={styles.clinicImgView}> 
          <Image
            style={styles.clinicImg} 
            source={this.props.clinicImg}
          />
        </View>
        <View style={styles.detailsView}>
          <Text style={styles.clinicNameTxt}>{this.props.clinicName}</Text>
          <View style={styles.bottomView}>
            <Text style={styles.clinicAddTxt}>{this.props.clinicAdd}</Text>
              {!this.props.isNoAppoinmentYet ? ( 
                <View style={styles.appntmntView}>
                  <Image
                    style={styles.calendarImg} 
                    source={Assets.appointment_inactive}
                  />
                  <Text style={styles.appntmntDateTxt}>{this.props.appoinmentDate}</Text>
                  <Text style={styles.appntmntTimeTxt}>{this.props.appoinmentTime}</Text>
                </View>
              ) : (
                <View style={[styles.appntmntView, { marginTop: 20 }]}>
                  <TouchableOpacity
                    style={styles.bookBtn}
                    onPress={() => { 
                      this.props.onPress();
                    }}>
                    <LinearGradient colors={[EDColors.secondary1Gradient, EDColors.secondary2Gradient]} start={{x: 0, y: 0}} end={{x: 1, y: 0}} style={styles.bookLG}>
                      <Text style={styles.bookNowTxt}>
                        BOOK NOW
                      </Text>
                    </LinearGradient>
                  </TouchableOpacity>
                </View>
              )}          
          </View>
        </View>
      </TouchableOpacity>            
    );
  }
}


const styles = StyleSheet.create({
  appntmntBtn: {
    flex: 1,
    flexDirection: "row", 
    borderRadius: 10, 
    padding: 8, 
    marginBottom: 10,
    borderWidth: 1, 
    borderColor: EDColors.primary
  },
  clinicImgView: {
    width: 80, 
    borderRadius: 10, 
    overflow: "hidden", 
    marginRight: 10
  },
  clinicImg: {
    width: "100%", 
    height: "100%", 
    resizeMode: "cover", 
    overflow: "hidden"
  },
  detailsView: {
    flex: 1
  },
  clinicNameTxt: {
    flex: 1, 
    fontFamily: ETFonts.bold, 
    color: EDColors.primary, 
    fontSize: 14
  },
  bottomView: {
    flexDirection: "row", 
    alignItems: "center", 
    bottom: 5
  },
  clinicAddTxt: {
    flex: 1,
    alignSelf: "flex-end",
    fontFamily: ETFonts.regular, 
    fontSize: 11, 
    color: EDColors.primaryGrey,
  },
  appntmntView: {
    alignItems: "flex-end"
  },
  calendarImg: {
    width: 20, 
    height: 20, 
    resizeMode: "contain", 
    marginBottom: 10
  },
  appntmntDateTxt: {
    fontFamily: ETFonts.regular, 
    color: EDColors.primary, 
    fontSize: 11
  },
  appntmntTimeTxt: {
    fontFamily: ETFonts.semibold, 
    color: EDColors.primary, 
    fontSize: 13
  },
  bookBtn: {
    alignSelf: "flex-end",
    elevation: 8,
    borderRadius: 20,
    overflow: "hidden",
    marginTop: 10
  },
  bookLG: {
    paddingHorizontal: 10
  },
  bookNowTxt: {
    fontSize: 12,
    fontFamily: ETFonts.bold,
    textAlign: 'center',
    margin: 5,
    color: EDColors.white
  }
})