import React, { Component } from "react";
import { View, Image, Text, TouchableOpacity, StyleSheet, FlatList } from "react-native";
import Assets from "../assets";
import { ETFonts } from "../assets/FontConstants";
import { EDColors } from '../assets/Colors'
import LinearGradient from 'react-native-linear-gradient';

export default class AppointmentListCard extends Component {

  constructor(props) {
    super(props);
    Text.defaultProps = Text.defaultProps || {};
    Text.defaultProps.allowFontScaling = false;

    this.state = {
    };

  }

  render() {
    return (
      <TouchableOpacity
        style={styles.appntmntBtn}
        onPress={() => {
          this.props.onPress();
        }}>
        <View style={styles.clinicImgView}> 
          <Image
            style={styles.clinicImg} 
            source={this.props.clinicImg}
          />
        </View>
        <View style={styles.clinicDtlsView}>
          <View style={{ flexDirection: "row" }}>
            <Text style={styles.clinicNameTxt}>{this.props.clinicName}</Text>
            {this.props.status == "Upcoming" && (
              <Image
                style={styles.checkCalendarImg} 
                source={Assets.greencheck}
              />
            )}
          </View>
          <View style={styles.rightView}>
            <Text style={styles.clinicAddTxt}>{this.props.clinicAdd}</Text>
            <View style={styles.appntmntView}>
              <Image
                style={[styles.checkCalendarImg, (this.props.status == "Missed" ? { tintColor: EDColors.red } : {})]} 
                source={Assets.appointment_inactive}
              />
              <Text style={[styles.appntmntDateTxt, (this.props.status == "Missed" ? { color: EDColors.red } : {})]}>{this.props.appoinmentDate}</Text>
              <Text style={[styles.appntmntTimeTxt, (this.props.status == "Missed" ? { color: EDColors.red } : {})]}>{this.props.appoinmentTime}</Text>
              <TouchableOpacity
                style={styles.viewBtn}
                onPress={() => { 
                  this.props.onPress();
                }}>
                <LinearGradient colors={[EDColors.secondary1Gradient, EDColors.secondary2Gradient]} start={{x: 0, y: 0}} end={{x: 1, y: 0}} style={styles.viewLG}>
                  <Text style={styles.viewTxt}>
                    {this.props.status == "Upcoming" ? 
                        "VIEW"
                      :
                        this.props.status == "Missed" ? 
                        "RESCHEDULE"
                      :
                        this.props.status == "Completed" ?
                        "VIEW RESULT"
                      :
                        "BOOK AGAIN"
                    }
                  </Text>
                </LinearGradient>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </TouchableOpacity>            
    );
  }
}

const styles = StyleSheet.create({
  appntmntBtn: {
    flex: 1,
    flexDirection: "row", 
    borderRadius: 10, 
    padding: 8, 
    marginBottom: 10,
    backgroundColor: EDColors.white,
    elevation: 2,
  },
  clinicImgView: {
    width: 100, 
    borderRadius: 10, 
    overflow: "hidden", 
    marginRight: 10
  },
  clinicImg: {
    width: "100%", 
    height: "100%", 
    resizeMode: "cover", 
    overflow: "hidden"
  },
  clinicDtlsView: {
    flex: 1
  },
  clinicNameTxt: {
    flex: 1,
    fontFamily: ETFonts.bold, 
    color: EDColors.primary, 
    fontSize: 14
  },
  clinicAddTxt: {
    flex: 1,
    fontFamily: ETFonts.regular, 
    fontSize: 12, 
    color: EDColors.black
  },
  appntmntView: {
    alignItems: "flex-end"
  },
  checkCalendarImg: {
    width: 15, 
    height: 15, 
    resizeMode: "contain", 
    marginBottom: 5
  },
  appntmntDateTxt: {
    fontFamily: ETFonts.regular, 
    color: EDColors.black, 
    fontSize: 11
  },
  appntmntTimeTxt: {
    fontFamily: ETFonts.semibold, 
    color: EDColors.primary, 
    fontSize: 14
  },
  rightView: {
    flexDirection: "row", 
    alignItems: "center"
  },
  viewBtn: {
    alignSelf: "flex-end",
    elevation: 8,
    borderRadius: 20,
    overflow: "hidden",
    marginTop: 5
  },
  viewLG: {
    paddingHorizontal: 25
  },
  viewTxt: {
    fontSize: 12,
    fontFamily: ETFonts.bold,
    textAlign: 'center',
    margin: 5,
    color: EDColors.white
  }
})