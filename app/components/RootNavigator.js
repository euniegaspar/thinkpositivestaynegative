import React from "react";
import { createStackNavigator, createBottomTabNavigator } from "react-navigation";
import LoginContainer from "../containers/LoginContainer";
import SignupContainer from "../containers/SignupContainer";
import SignupPasswordContainer from "../containers/SignupPasswordContainer";
import OTPContainer from "../containers/OTPContainer";
import OTPSuccessContainer from "../containers/OTPSuccessContainer";
import ForgotPasswordContainer from "../containers/ForgotPasswordContainer";
import ForgotPassSuccessContainer from "../containers/ForgotPassSuccessContainer";
import SplashContainer from "../containers/SplashContainer";
import HomeContainer from "../containers/HomeContainer";
import TestingCenterContainer from "../containers/TestingCenterContainer";
import BookingScheduleContainer from "../containers/BookingScheduleContainer";
import BookingInfoContainer from "../containers/BookingInfoContainer";
import BookingSummaryContainer from "../containers/BookingSummaryContainer";
import AppointmentContainer from "../containers/AppointmentContainer";
import NearbyContainer from "../containers/NearbyContainer";
import ProfileContainer from "../containers/ProfileContainer";
import SavedContainer from "../containers/SavedContainer";
import CustomTabBar from "../containers/CustomTabBar";

const MAIN_NAVIGATOR = createStackNavigator(
  {
    HomeContainer: {
      screen: HomeContainer
    },
    AppointmentContainer: {
      screen: AppointmentContainer
    },
    TestingCenterContainer: {
      screen: TestingCenterContainer
    },
    BookingScheduleContainer: {
      screen: BookingScheduleContainer
    },
    BookingInfoContainer: {
      screen: BookingInfoContainer
    },
    BookingSummaryContainer: {
      screen: BookingSummaryContainer
    },
    ProfileContainer: {
      screen: ProfileContainer
    },
    NearbyContainer: {
      screen: NearbyContainer
    },
    SavedContainer: {
      screen: SavedContainer
    }
  },
  {
    initialRouteName: "HomeContainer",
    headerMode: "none"
  }
);

const APPOINTMENT_NAVIGATOR = createStackNavigator(
  {
    AppointmentContainer: {
      screen: AppointmentContainer
    }
  },
  {
    initialRouteName: "AppointmentContainer",
    headerMode: "none"
  }
);

const SAVED_NAVIGATOR = createStackNavigator(
  {
    SavedContainer: {
      screen: SavedContainer
    }
  },
  {
    initialRouteName: "SavedContainer",
    headerMode: "none"
  }
);

const NEARBY_NAVIGATOR = createStackNavigator(
  {
    NearbyContainer: {
      screen: NearbyContainer
    }
  },
  {
    initialRouteName: "NearbyContainer",
    headerMode: "none"
  }
);


const PROFILE_NAVIGATOR = createStackNavigator(
  {
    ProfileContainer: {
      screen: ProfileContainer
    }
  },
  {
    initialRouteName: "ProfileContainer",
    headerMode: "none"
  }
);

export const HOME_SCREEN_BOTTOM_TAB = createBottomTabNavigator(
  {
    Home: {
      screen: MAIN_NAVIGATOR
    },
    Appointment: {
      screen: APPOINTMENT_NAVIGATOR
    },
    Nearby: {
      screen: NEARBY_NAVIGATOR
    },
    Saved: {
      screen: SAVED_NAVIGATOR
    },
    Profile: {
      screen: PROFILE_NAVIGATOR
    }
  },
  {
    initialRouteName: "Home",
    initialRouteParams: "Home",
    tabBarComponent: props => <CustomTabBar {...props} />
  }
);

export const BASE_STACK_NAVIGATOR = createStackNavigator(
  {
    SplashContainer: {
      screen: SplashContainer
    },
    LoginContainer: {
      screen: LoginContainer
    },
    SignupContainer: {
      screen: SignupContainer
    },
    SignupPasswordContainer: {
      screen: SignupPasswordContainer
    },
    HomeContainer: {
      screen: HOME_SCREEN_BOTTOM_TAB
    },
    OTPContainer: {
      screen: OTPContainer
    },
    OTPSuccessContainer: {
      screen: OTPSuccessContainer
    },
    ForgotPasswordContainer: {
      screen: ForgotPasswordContainer
    },
    ForgotPassSuccessContainer: {
      screen: ForgotPassSuccessContainer
    }
  },
  {
    initialRouteName: "SplashContainer",
    headerMode: "none"
  }
);