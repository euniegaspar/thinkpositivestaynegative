import React from "react";
import { Text, StyleSheet } from "react-native";
import { EDColors } from "../assets/Colors";
import { ETFonts } from "../assets/FontConstants";

export default class ETextErrorMessage extends React.Component {

  render() {
    return (
      <Text style={[styles.textLabel,this.props.errorStyle]}>
        {this.props.errorMsg}
      </Text>
    ); 
  }
}

const styles = StyleSheet.create({
  textLabel: {
    color: EDColors.error, 
    fontFamily: ETFonts.regular
  }
});
