import React, { Component } from "react";
import { View, Image, Text, TouchableOpacity, StyleSheet } from "react-native";
import Assets from "../assets";
import { ETFonts } from "../assets/FontConstants";
import { EDColors } from '../assets/Colors'
import LinearGradient from 'react-native-linear-gradient';

export default class NavBarComponent extends Component {

  constructor(props) {
    super(props);
    Text.defaultProps = Text.defaultProps || {};
    Text.defaultProps.allowFontScaling = false;

    this.state = {
    };
  }

  render() {
    return (
      <LinearGradient colors={[EDColors.primary1Gradient, EDColors.primary2Gradient]} start={{x: 0, y: 0}} end={{x: 1, y: 0}} style={{ height: this.props.withExtraHeight ? 130 : 80 }}>
        <View style={styles.navbar}>
          <TouchableOpacity
            style={styles.backBtn}
            onPress={() => {
              this.props.backButton();
            }}>
            <Image style={styles.backImg} source={Assets.backarrow}/>
          </TouchableOpacity>
          <Text style={styles.navTitleTxt}>{this.props.navtitle}</Text>
        </View>
      </LinearGradient>
    );
  }
}

const styles = StyleSheet.create({
  navbar: {
    flexDirection: "row", 
    marginHorizontal: 15, 
    alignItems: "center",
    top: 40
  },
  backBtn: {
    width: 30, 
    height: 20,
    top: 4
  },
  backImg: {
    width: 15, 
    height: 15, 
    resizeMode: "contain"
  },
  navTitleTxt: {
    fontFamily: ETFonts.semibold, 
    color: EDColors.white, 
    fontSize: 20
  },
})