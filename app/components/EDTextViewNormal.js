import React from "react";
import { Text, StyleSheet } from "react-native";
import { EDColors } from "../assets/Colors";
import { ETFonts } from "../assets/FontConstants";

export default class EDTextViewNormal extends React.Component {
  render() {
    return (
      <Text style={[styles.text, this.props.style]}>
        {this.props.text}
      </Text>
    );
  }
}

export const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  text: { 
    color: EDColors.white,
    fontSize: 16,
    marginTop: 10,
    fontFamily: ETFonts.regular
  }
});

