import React, { Component } from "react";
import { View, Image, Text, TouchableOpacity, StyleSheet, FlatList, ScrollView } from "react-native";
import Assets from "../assets";
import { ETFonts } from "../assets/FontConstants";
import { EDColors } from '../assets/Colors'
import StarRating from 'react-native-star-rating'
import LinearGradient from 'react-native-linear-gradient';

export default class TestingCenterListCard extends Component {

  constructor(props) {
    super(props);
    Text.defaultProps = Text.defaultProps || {};
    Text.defaultProps.allowFontScaling = false;

    this.state = {
    };
  }

  render() {
    return (
      <TouchableOpacity
        style={styles.clinicBtn}
        onPress={() => { 
          this.props.onPress();
        }}>
        <View style={styles.clinicImgView}> 
          <Image
            style={styles.clinicImg} 
            source={this.props.clinicImg}
          />
        </View>
        <View style={styles.detailsView}>
          <Text style={styles.clinicNameTxt}>{this.props.clinicName}</Text>
          <View style={styles.starRating}>
            <StarRating
              disabled={false}
              starSize={12}
              maxStars={5}
              rating={parseFloat(this.props.rating)}
              emptyStarColor={EDColors.yellowrating}
              fullStarColor={EDColors.yellowrating}
              selectedStar={(rating) => {}}
              starStyle={{ marginRight: 5 }}
            />
          </View>
          <View style={styles.bottomView}>
            <Image style={styles.locationImg} source={Assets.location} />
            <Text style={styles.clinicAddTxt}>{this.props.clinicAdd}</Text>
            <TouchableOpacity
              style={styles.bookBtn}
              onPress={() => { 
                this.props.onPress();
              }}>
              <LinearGradient colors={[EDColors.secondary1Gradient, EDColors.secondary2Gradient]} start={{x: 0, y: 0}} end={{x: 1, y: 0}} style={styles.bookLG}>
                <Text style={styles.bookNowTxt}>
                  BOOK NOW
                </Text>
              </LinearGradient>
            </TouchableOpacity>
          </View>
        </View>
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  clinicBtn: {
    flex: 1,
    flexDirection: "row", 
    borderRadius: 10, 
    padding: 8, 
    marginBottom: 10,
    backgroundColor: EDColors.white,
    elevation: 2,
  },
  clinicImgView: {
    width: 80, 
    borderRadius: 10, 
    overflow: "hidden", 
    marginRight: 10
  },
  clinicImg: {
    width: "100%", 
    height: "100%", 
    resizeMode: "cover", 
    overflow: "hidden"
  },
  detailsView: {
    flex: 1
  },
  clinicNameTxt: {
    fontFamily: ETFonts.bold, 
    color: EDColors.primary, 
    fontSize: 14
  },
  starRating: {
    flexDirection: 'row',
    flex: 1,
    marginTop: 5,
    width: 200
  },
  bottomView: {
    flexDirection: "row", 
    alignItems: "center",
    marginTop: 10
  },
  locationImg: {
    height: 14, 
    width: 14, 
    resizeMode: "contain", 
    marginRight: 5,
    bottom: 2
  },
  clinicAddTxt: {
    flex: 1,
    fontFamily: ETFonts.regular, 
    fontSize: 11, 
    color: EDColors.primaryGrey,
    marginRight: 5
  },
  bookBtn: {
    alignSelf: "flex-end",
    elevation: 8,
    borderRadius: 20,
    overflow: "hidden",
    marginTop: 10
  },
  bookLG: {
    paddingHorizontal: 10
  },
  bookNowTxt: {
    fontSize: 12,
    fontFamily: ETFonts.bold,
    textAlign: 'center',
    margin: 5,
    color: EDColors.white
  }
})