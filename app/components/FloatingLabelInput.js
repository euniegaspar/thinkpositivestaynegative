import React, { Component } from 'react';
import { View, Animated, StyleSheet, TextInput, Image, TouchableOpacity, Text, Platform } from 'react-native';
import { string, func, object, number } from 'prop-types';
import Assets from "../assets";
import { EDColors } from "../assets/Colors";
import { ETFonts } from "../assets/FontConstants";

class FloatingLabelInput extends Component {

    static propTypes = {
        attrName: string.isRequired,
        title: string.isRequired,
        value: string.isRequired,
        updateMasterState: func.isRequired,
        keyboardType: string,
        titleActiveSize: number, // to control size of title when field is active
        titleInActiveSize: number, // to control ssize of title when field is inactive
        titleActiveColor: string, // to control color of title when field is active
        titleInactiveColor: string, // to control color of title when field is active
        textInputStyles: object,
        otherTextInputProps: object
    }

    static defaultProps = {
        keyboardType: 'default',
        titleActiveSize: 13.5,
        titleInActiveSize: 16,
        titleActiveColor: EDColors.white,
        titleInactiveColor: EDColors.white,
        textInputStyles: {
            color: EDColors.white,
            fontSize: 16,
        },
        otherTextInputAttributes: {
            maxLength: 30
        },
        secureTextEntry: true
    }

    constructor(props) {
        super(props);
        const { value } = this.props;
        this.position = new Animated.Value(value ? 1 : 0);
        this.state = {
            isFieldActive: false,
            secureTextEntry: true
        }
        Text.defaultProps = Text.defaultProps || {};
        Text.defaultProps.allowFontScaling = false;
        TextInput.defaultProps = TextInput.defaultProps || {};
        TextInput.defaultProps.allowFontScaling = false;
    }

    _handleFocus = () => {
        if (!this.state.isFieldActive) {
            this.setState({ isFieldActive: true });
            Animated.timing(this.position, {
                toValue: 1,
                duration: 150,
            }).start();
        }
    }

    _handleBlur = () => {
        if (this.state.isFieldActive && !this.props.value) {
            this.setState({ isFieldActive: false });
            Animated.timing(this.position, {
                toValue: 0,
                duration: 150,
            }).start();
        }
    }

    _onChangeText = (updatedValue) => {
        const { attrName, updateMasterState } = this.props;
        updateMasterState(attrName, updatedValue);
    }

    _unhidePassword = () => {
        this.setState({ secureTextEntry: !this.state.secureTextEntry });
    }

    _returnAnimatedTitleStyles = () => {
        const { isFieldActive } = this.state;
        const {
            titleActiveColor, titleInactiveColor, titleActiveSize, titleInActiveSize
        } = this.props;

        return {
            top: this.position.interpolate({
                inputRange: [0, 1],
                outputRange: [14, 0],
            }),
            fontSize: isFieldActive ? titleActiveSize : titleInActiveSize,
            color: isFieldActive ? titleActiveColor : titleInactiveColor,
        }
    }

    render() {
        return (
            <View style={styles.container}>
                <Animated.Text
                    style={[styles.titleStyles, this._returnAnimatedTitleStyles()]} >
                    {this.props.title}
                </Animated.Text>
                <View style={styles.subContainer}>
                    <TextInput
                        value={this.props.value}
                        style={[styles.textInput, this.props.textInputStyles, { width: this.props.showPasswordBtn ? '100%' : '100%' }]}
                        onFocus={this._handleFocus}
                        onBlur={this._handleBlur}
                        onChangeText={this._onChangeText}
                        secureTextEntry={this.props.showPasswordBtn ? this.state.secureTextEntry : false}
                        keyboardType={this.props.showPasswordBtn ? this.state.secureTextEntry ? "default" : "visible-password" : this.props.keyboardType}
                        maxLength={this.props.maxLength}
                        {...this.props.otherTextInputProps}
                    />
                    {/* {this.props.showPasswordBtn && 
                        <TouchableOpacity
                            onPress={this._unhidePassword}>
                            <Image 
                                source={this.state.secureTextEntry ? Assets.eyeclose : Assets.eyeopen}
                                style={styles.eyeIcon} />
                        </TouchableOpacity>
                    } */}
                </View>
            </View>
        )
    }
}

export default FloatingLabelInput;

const styles = StyleSheet.create({
    container: {
        width: '100%',
        height: 50
    },
    subContainer: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    textInput: {
        marginVertical: Platform.OS == 'ios' ? 10 : 0,
        fontSize: 16,
        marginTop: 0,
        fontFamily: ETFonts.regular,
        color: EDColors.white,
        borderBottomWidth: 1,
        borderBottomColor: EDColors.white
    },
    titleStyles: {
        position: 'absolute',
        fontFamily: ETFonts.regular,
        color: EDColors.white,
        left: 4,
    },
    eyeIcon: {
        width: 20, 
        height: 20
    }
})