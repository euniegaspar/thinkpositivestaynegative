import React from "react";
import { View, StyleSheet, Dimensions } from "react-native";
import { Spinner } from "native-base";
import { EDColors } from "../assets/Colors";
import CircularLoading from '../components/CircularLoading';

export default class ProgressLoader extends React.Component {

    render() {
        const customLoading = false;
        return customLoading ? (
            <CircularLoading size={50} strokeWidth={3} strokeColor={EDColors.white} loading={this.props.isLoading} />
        ) : (
            this.props.isLoading ? (
                <View style={styles.container}>
                    <View style={styles.containerOpac} />
                        <Spinner style={styles.spinner} color={EDColors.buttonFillColor} />
                    </View>
            ) : null
        );
    }
}

const styles = StyleSheet.create({
    container: {
        position: "absolute",
        width: Dimensions.get("window").width,
        height: Dimensions.get("window").height,
        zIndex: 997
    },
    containerOpac: {
        position: "absolute",
        width: Dimensions.get("window").width,
        height: Dimensions.get("window").height,
        backgroundColor: "rgba(0,0,0,0.5)",
        zIndex: 998
    },
    spinner: {
        flex: 1,
        alignSelf: "center",
        zIndex: 1000,
        transform: [{ scaleX: 1.5 }, { scaleY: 1.5 }]
    }
});