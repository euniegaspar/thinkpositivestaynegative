import React from "react";
import { Text, StyleSheet, Image } from "react-native";
import { EDColors } from "../assets/Colors";
import { ETFonts } from "../assets/FontConstants";

export default class ETextViewNormalLable extends React.Component {

  constructor(props) {
    super(props);
    Text.defaultProps = Text.defaultProps || {};
    Text.defaultProps.allowFontScaling = false;
  }

  render() {
    return <Text style={[styles.textLbl, {...this.props.style}]}>{this.props.text}</Text>;
  }
}

const styles = StyleSheet.create({
  textLbl: {
    marginStart: 10,
    marginEnd: 10,
    color: EDColors.lightGray,
    fontSize: 16,
    marginTop: 8,
    marginLeft: 10,
    fontFamily: ETFonts.bold
  }
});
