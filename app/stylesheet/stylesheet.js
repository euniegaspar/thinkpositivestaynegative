import { StyleSheet } from "react-native";
import { EDColors } from "../assets/Colors";

export default StyleSheet.create({
  navHeader: {
    flex: 1,
    flexDirection: "column-reverse",
    alignItems: "center",
    justifyContent:'center'
  },
  navItemContainer: {
    flex: 5,
    paddingBottom: 20
  },
  navMobile: {
    flexDirection: "row",
    color: "#fff",
    alignItems: "center",
    marginTop: 10,
    marginBottom: 10
  },
  navItem: {
    color: EDColors.black,
    fontSize: 15,
    marginLeft: 40,
    marginTop: 40,
    flexDirection: "row",
    alignItems: "center"
  },
  sideMenuText: {
    marginLeft: 30,
    color: EDColors.black
  },
  sideMenuTextSelected: {
    marginLeft: 30,
    color: "yellow"
  }
});
