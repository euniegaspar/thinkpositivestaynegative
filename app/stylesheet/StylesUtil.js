import { StyleSheet } from "react-native";
import { EDColors } from "../assets/Colors";
import { ETFonts } from "../assets/FontConstants";

export const Style = StyleSheet.create({
  loginBlankView: { flex: 4 },
  loginView: {
    color: "#fff",
    marginLeft: 25,
    marginRight: 25
  },
  textview_subtitle: {
    color: EDColors.white,
    fontSize: 14,
    fontFamily: ETFonts.regular,
    textDecorationLine: 'underline',
  },
  textview: {
    color: EDColors.white,
    fontSize: 16,
    marginTop: 10,
    fontFamily: ETFonts.regular,
    textDecorationLine: 'underline'
  },
  textviewNormal: {
    color: EDColors.black,
    fontSize: 18,
    fontFamily: ETFonts.regular
  }
});
