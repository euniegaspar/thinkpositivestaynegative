import { createStore, combineReducers } from "redux";
import { userOperations } from "./reducers/UserReducer";
import { navigationOperation } from "./reducers/NavigationReducer";
import { appVersionOperation } from "./reducers/AppVersionReducer";
import { betaModeOperation } from "./reducers/BetaModeReducer";

const rootReducer = combineReducers({
    userOperations: userOperations,
    navigationReducer: navigationOperation,
    appVersionOperation: appVersionOperation,
    betaModeOperation: betaModeOperation,
});
const store = createStore(rootReducer);
export default store;