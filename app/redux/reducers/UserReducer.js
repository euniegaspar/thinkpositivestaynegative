import { TYPE_SAVE_LOGIN_DETAILS, TYPE_SAVE_LOGIN_FCM } from "../actions/User";

const initialStateUser = {
  // LOGIN DETAILS
  phoneNumberInRedux: undefined,
  countryCodeInRedux: undefined,
  userIdInRedux: undefined,
  fullnameInRedux: undefined,
  imageInRedux: undefined,
  notification: undefined
};

export function userOperations(state = initialStateUser, action) {
  console.log(action);
  switch (action.type) {
    case TYPE_SAVE_LOGIN_DETAILS: {
      return Object.assign({}, state, {
        phoneNumberInRedux: action.value.PhoneNumber,
        countryCodeInRedux: action.value.phone_code,
        userIdInRedux: action.value.UserID,
        fullnameInRedux: action.value.FirstName,
        imageInRedux: action.value.image,
        notification: action.value.notification
      });
    }
    case TYPE_SAVE_LOGIN_FCM: {
      return Object.assign({}, state, {
        token: action.value
      });
    }
    default:
      return state;
  }
}
