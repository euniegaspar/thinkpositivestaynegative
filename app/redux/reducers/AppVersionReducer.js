import { TYPE_APP_VERSION, TYPE_APP_CACHE, TYPE_APP_PERMISSIONS } from "../actions/AppVersion";

const initialStateUser = {
    app_version: undefined,
    app_cache: undefined,
    app_permissions: undefined
};

export function appVersionOperation(state = initialStateUser, action) {
    switch(action.type) {
        case TYPE_APP_VERSION: {
            return Object.assign({}, state, {
                app_version: action.value
            });
        }
        case TYPE_APP_CACHE: {
            return Object.assign({}, state, {
              app_cache: action.value
            });
        }    
        case TYPE_APP_PERMISSIONS: {
            return Object.assign({}, state, {
              app_permissions: action.value
            });
        }        
        default:
            return state;
    }
}