import { TYPE_SAVE_NAVIGATION_SELECTION, TYPE_SAVE_NAVIGATION_SELECTION_PAGE } from "../actions/Navigation";

const initalState = {
  selectedItem: "Home",
  selectedPage: ""
};

export function navigationOperation(state = initalState, action) {
  switch (action.type) {
    case TYPE_SAVE_NAVIGATION_SELECTION: {
      return Object.assign({}, state, {
        selectedItem: action.value
      });
    }
    case TYPE_SAVE_NAVIGATION_SELECTION_PAGE: {
      return Object.assign({}, state, {
        selectedPage: action.value
      });
    }
    default:
      return state;
  }
}
