import { TYPE_SAVE_BETA_MODE } from "../actions/BetaMode";

const initialState = {
    betaMode: undefined
};

export function betaModeOperation(state = initialState, action) {
  switch(action.type) {
    case TYPE_SAVE_BETA_MODE: {
        return Object.assign({}, state, {
            betaMode: action.value
        });
    }
    default:
        return state;
    }
}
