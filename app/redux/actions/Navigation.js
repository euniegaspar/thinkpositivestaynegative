export const TYPE_SAVE_NAVIGATION_SELECTION = "TYPE_SAVE_NAVIGATION_SELECTION";
export const TYPE_SAVE_NAVIGATION_SELECTION_PAGE = "TYPE_SAVE_NAVIGATION_SELECTION_PAGE";

export function saveNavigationSelection(data) {
  return {
    type: TYPE_SAVE_NAVIGATION_SELECTION,
    value: data
  };
}

export function saveNavigationSelectionPage(data) {
  return {
    type: TYPE_SAVE_NAVIGATION_SELECTION_PAGE,
    value: data
  };
}
