export const TYPE_SAVE_BETA_MODE = "TYPE_SAVE_BETA_MODE";

export function saveBetaModeInRedux(data) {
  return {
    type: TYPE_SAVE_BETA_MODE,
    value: data
  };
}
