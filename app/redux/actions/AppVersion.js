export const TYPE_APP_VERSION = "TYPE_APP_VERSION";
export const TYPE_APP_CACHE = "TYPE_APP_CACHE";
export const TYPE_APP_PERMISSIONS = "TYPE_APP_PERMISSIONS";

export function saveAppVersionInRedux(data) {
  return {
    type: TYPE_APP_VERSION,
    value: data
  }
}

export function saveAppCacheInRedux(data) {
  return {
    type: TYPE_APP_CACHE,
    value: data
  };
}

export function saveAppPermissionsInRedux(data) {
  return {
    type: TYPE_APP_PERMISSIONS,
    value: data
  }
}

  