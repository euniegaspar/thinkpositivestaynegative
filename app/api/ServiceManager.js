/**
 * Genric function to make api calls with method post
 * @param {apiPost} url  API end point to call
 * @param {apiPost} responseSuccess  Call-back function to get success response from api call
 * @param {apiPost} responseErr  Call-back function to get error response from api call
 * @param {apiPost} requestHeader  Request header to be send to api
 * @param {apiPost} body data to be send through api
 */
export async function apiPost(
  url,
  body,
  responseSuccess,
  responseError
) {
  console.log("request", JSON.stringify(body));
  promiseResult(url, "POST", body)
  .then(response => responseSuccess(response))
  .catch(error => responseError(error));
  // fetch(url, {
  //   method: "POST",
  //   headers: requestHeader,
  //   body: JSON.stringify(body)
  // })
  //   .then(errorHandler)
  //   .then(response => response.json())
  //   .then(json => responseSuccess(json))
  //   .catch(err => responseErr(err));
}

/**
 * Genric function to make api calls with method get
 * @param {apiGet} url  API end point to call
 * @param {apiGet} responseSuccess  Call-back function to get success response from api call
 * @param {apiGet} responseErr  Call-back function to get error response from api call
 * @param {apiGet} requestHeader  Request header to be send to api
 */
export async function apiGet(
  url,
  responseSuccess,
  responseError
) {
  promiseResult(url, "GET")
  .then(response => responseSuccess(response))
  .catch(error => responseError(error));
  // fetch(url, {
  //   method: "GET",
  //   headers: requestHeader
  // })
  //   .then(errorHandler)
  //   .then(response => response.json())
  //   .then(json => responseSuccess(json))
  //   .catch(err => responseErr(err));
}

/**
 * Genric function to make api calls with method delete
 * @param {apiDelete} url  API end point to call
 * @param {apiDelete} responseSuccess  Call-back function to get success response from api call
 * @param {apiDelete} responseErr  Call-back function to get error response from api call
 * @param {apiDelete} requestHeader  Request header to be send to api
 */
export function apiDelete(
  url,
  responseSuccess,
  responseError
) {
  promiseResult(url, "DELETE", body)
  .then(response => responseSuccess(response))
  .catch(error => responseError(error));
  // fetch(url, {
  //   method: "DELETE",
  //   headers: requestHeader
  // })
  //   .then(errorHandler)
  //   .then(response => (response.status == 204 ? response : response.json()))
  //   .then(json => responseSuccess(json))
  //   .catch(err => responseErr(err));
}

/**
 * Genric function to make api calls with method delete
 * @param {apiPostQs} url  API end point to call
 * @param {apiPostQs} responseSuccess  Call-back function to get success response from api call
 * @param {apiPostQs} responseErr  Call-back function to get error response from api call
 * @param {apiPostQs} requestHeader  Request header to be send to api
 */
export async function apiPostQs(
  url,
  body,
  responseSuccess,
  responseError
) {
  console.log("request", JSON.stringify(body));
  promiseResultMultiPartFormData(url, "POST", body)
  .then(response => {
    console.log(response)
    responseSuccess(response)
  })
  .catch(error => {
    console.log(error)
    responseError(error)
  });
  // fetch(url, {
  //   method: "POST",
  //   body: body,
  //   headers: requestHeader
  // })
  //   .then(errorHandler)
  //   .then(response => response.json())
  //   .then(json => responseSuccess(json))
  //   .catch(err => responseErr(err));
}

//Error Handler
/**
 *
 * @param {errorHandler} response Generic function to handle error occur in api
 */
const errorHandler = response => {
  console.log("Response ==>",response);
  if (
    (response.status >= 200 && response.status < 300) ||
    response.status == 401 ||
    response.status == 400
  ) {
    if (response.status == 204) {
      response.body = { success: "Saved" };
    }
    return Promise.resolve(response);
  } else {

    var error = new Error(response.statusText || response.status);
    error.response = response;
    return Promise.reject(error);
  }
};

async function promiseResult(
  url,
  method,
  body = null,
  requestHeader = {
    "Content-Type": "application/json"
  }) {
  
  return new Promise((resolve, reject) => {
    let timer = setTimeout(() => {
      reject("Request timeout");
    }, 60000);

    var params = {
      method: method,
      headers: requestHeader
    };
    if(body) {
      Object.assign(params, { body: JSON.stringify(body) });
    }

    fetch(url, params)
    .then(errorHandler)
    .then(response => response.json())
    .then(json => resolve(json))
    .catch(err => reject(err))
    .finally(() => clearTimeout(timer));
  })
}

async function promiseResultMultiPartFormData(
  url,
  method,
  body,
  requestHeader = {
    Accept: "application/json",
    "Content-Type": "multipart/form-data"
  }) {
  return new Promise((resolve, reject) => {
    let timer = setTimeout(() => {
      reject("Request timeout");
    }, 60000);
    fetch(url, {
      method: method,
      headers: requestHeader,
      body: body
    })
    .then(errorHandler)
    .then(response => response.json())
    .then(json => resolve(json))
    .catch(err => reject(err))
    .finally(() => clearTimeout(timer));
  })
}
