
/**
 * Genric function to make api calls with method post
 * @param {apiPost} url  API end point to call
 * @param {apiPost} responseSuccess  Call-back function to get success response from api call
 * @param {apiPost} responseErr  Call-back function to get error response from api call
 * @param {apiPost} requestHeader  Request header to be send to api
 * @param {apiPost} body data to be send through api
 */

 export async function apiPost(
  url,
  body,
  responseSuccess,
  responseError
) {
  console.log("request", JSON.stringify(body));
  promiseResult(url, "POST", body)
  .then(response => responseSuccess(response))
  .catch(error => responseError(error));
  // fetch(url, {
  //   method: "POST",
  //   headers: requestHeader,
  //   body: JSON.stringify(body)
  // })
  //   .then(errorHandler)
  //   .then(response => response.json())
  //   .then(json => responseSuccess(json))
  //   .catch(error => responseError(error));
}

export async function apiPostFormData(
  url,
  body,
  responseSuccess,
  responseError
) {
  // const formData = new FormData();
  // formData.append("restaurant_id", parseInt(this.resId));
  // formData.append("food", this.foodType);
  // formData.append("price", this.priceType);
  console.log("request", JSON.stringify(body));
  promiseResultMultiPartFormData(url, "POST", body)
  .then(response => responseSuccess(response))
  .catch(error => responseError(error));
  // fetch(url, {
  //   method: "POST",
  //   headers: requestHeader,
  //   body: body
  // })
  //   .then(errorHandler)
  //   .then(response => response.json())
  //   .then(json => responseSuccess(json))
  //   .catch(error => responseError(error));
}

export async function apiGet(
  url,
  responseSuccess,
  responseError,
) {
  promiseResult(url, "GET")
  .then(response => responseSuccess(response))
  .catch(error => responseError(error));
  // fetch(url, {
  //   method: "GET",
  //   headers: requestHeader
  // })
  //   .then(errorHandler)
  //   .then(response => response.json())
  //   .then(json => success(json))
  //   .catch(error => responseError(error));
}

export async function apiCustom(
  url,
  method,
  requestHeader,
  body,
  responseSuccess,
  responseError
) {
  promiseCustom(url, method, requestHeader, body)
  .then(response => responseSuccess(response))
  .catch(error => responseError(error));
}

async function promiseCustom(
  url,
  method,
  requestHeader,
  body) {
  return new Promise((resolve, reject) => {
    let timer = setTimeout(() => {
      reject("Request timeout");
    }, 60000);

    var params = {
      method: method,
      headers: requestHeader
    };
    if(body) {
      Object.assign(params, { body: JSON.stringify(body) });
    }

    fetch(url, params)
    .then(errorHandler)
    .then(response => response.json())
    .then(json => resolve(json))
    .catch(err => reject(err))
    .finally(() => clearTimeout(timer));
  })
}

//Error Handler
/**
 *
 * @param {errorHandler} response Generic function to handle error occur in api
 */
const errorHandler = response => {
  console.log("Response ==>", response);
  if (response.status == 1 || response.status == 200) {
    return Promise.resolve(response);
  } else {
    var error = new Error(response.statusText || response.status);
    error.response = response;
    return Promise.reject(error);
  }
};

async function promiseResult(
  url,
  method,
  body = null,
  requestHeader = {
    "Content-Type": "application/json"
  }) {

  return new Promise((resolve, reject) => {
    let timer = setTimeout(() => {
      reject("Request timeout");
    }, 60000);

    var params = {
      method: method,
      headers: requestHeader
    };
    if(body) {
      Object.assign(params, { body: JSON.stringify(body) });
    }

    fetch(url, params)
    .then(errorHandler)
    .then(response => response.json())
    .then(json => resolve(json))
    .catch(err => reject(err))
    .finally(() => clearTimeout(timer));
  })
}

async function promiseResultMultiPartFormData(
  url,
  method,
  body,
  requestHeader = {
    "Content-Type": "multipart/form-data"
  }) {
  return new Promise((resolve, reject) => {
    let timer = setTimeout(() => {
      reject("Request timeout");
    }, 60000);
    fetch(url, {
      method: method,
      headers: requestHeader,
      body: body
    })
    .then(errorHandler)
    .then(response => response.json())
    .then(json => resolve(json))
    .catch(err => reject(err))
    .finally(() => clearTimeout(timer));
  })
}
