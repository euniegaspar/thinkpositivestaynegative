import React from "react";
import {
  StatusBar,
  View,
  Text,
  TouchableOpacity,
  Image,
  ImageBackground,
  StyleSheet,
  TextInput
} from "react-native";
import Assets from "../assets";
import { getUserToken } from "../utils/AsyncStorageHelper";
import { EDColors } from "../assets/Colors";
import { ETFonts } from "../assets/FontConstants";
import LinearGradient from 'react-native-linear-gradient';

export default class OTPSuccessContainer extends React.Component {
  constructor(props) {
    super(props);
    if (Text.defaultProps == null) Text.defaultProps = {};
    Text.defaultProps.allowFontScaling = false;
  }

  componentDidMount() {
  } 

  render() {
    return (
      <ImageBackground
        source={Assets.bgSplash}
        style={styles.imgBackground}>
        <StatusBar 
          barStyle="light-content" 
          backgroundColor="transparent" 
          translucent={true}
        />
        <View style={styles.container}>
          <TouchableOpacity 
            style={styles.backBtn}
            onPress={() => {
              this.props.navigation.goBack();
            }}>
            <Image source={Assets.backarrow} style={styles.backImg} />
          </TouchableOpacity>
          <Image style={styles.logoImg} source={Assets.logo2} />
          <View style={styles.msgContainer}>
            <View style={styles.msgView}>
              <Text style={styles.msgTxt}>Your account has been{'\n'}verified successfully!</Text>
            </View>
            <TouchableOpacity
              style={styles.doneBtn}
              onPress={() => {
                this.props.navigation.navigate("HomeContainer");
              }}>
              <LinearGradient colors={[EDColors.secondary1Gradient, EDColors.secondary2Gradient]} start={{x: 0, y: 0}} end={{x: 1, y: 0}} style={styles.doneLG}>
                <Text style={styles.doneTxt}>
                  DONE
                </Text>
              </LinearGradient>
            </TouchableOpacity>  
          </View>
        </View>
      </ImageBackground>
    );
  }
}

const styles = StyleSheet.create({
  imgBackground: {
    width: "100%",
    height: "100%"
  },
  container: {
    flex: 1,
    justifyContent: "center",
    alignContent: "center"
  },
  backBtn: {
    position: "absolute", 
    left: 15, 
    top: 55
  },
  backImg: {
    height: 20, 
    width: 20, 
    resizeMode: "contain"
  },
  logoImg: {
    width: 90, 
    height: 90, 
    resizeMode: "contain",
    alignSelf: "center"
  },
  msgContainer: {
    paddingHorizontal: 60,
    marginBottom: 100
  },
  msgView: {
    alignItems: "center",
    justifyContent: "center",
    marginTop: 35,
    marginBottom: 35
  },
  msgTxt: {
    fontFamily: ETFonts.bold, 
    color: EDColors.white, 
    fontSize: 18,
    textAlign: "center"
  },
  doneBtn: {
    alignSelf: "center",
    elevation: 5,
    paddingHorizontal: 1,
    borderRadius: 20,
    marginTop: 10
  },
  doneLG: {
    paddingHorizontal: 60,
    borderRadius: 20
  },
  doneTxt: {
    fontSize: 16,
    fontFamily: ETFonts.bold,
    textAlign: 'center',
    margin: 10,
    color: EDColors.white
  }
})
