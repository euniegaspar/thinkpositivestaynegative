import React from "react";
import {
  View,
  Text,
  TouchableOpacity,
  StyleSheet,
  ScrollView,
  StatusBar,
  Image,
  FlatList
} from "react-native";
import Assets from "../assets";
import { EDColors } from "../assets/Colors";
import { connect } from "react-redux";
import { ETFonts } from "../assets/FontConstants";
import ProgressLoader from '../components/ProgressLoader';
import { saveNavigationSelection, saveNavigationSelectionPage } from "../redux/actions/Navigation";
import NavBarComponent from "../components/NavBarComponent";
import AppointmentListCard from "../components/AppointmentListCard";
import { App } from "react-native-firebase";

class AppointmentContainer extends React.Component {
  constructor(props) {
    super(props);
    if (Text.defaultProps == null) Text.defaultProps = {};
    Text.defaultProps.allowFontScaling = false;

    this.state = {
      isUpcoming: true,
      isMissed: false,
      isCompleted: false,
      isCancelled: false,
      status: "Upcoming",
      appointmentList: [
        { id: 1, clinicName: "TPSN Single Testing Pod", clinicAdd: "1964 Drive, M. Carreon Street Sta. Ana, Manila", 
          appoinmentDate: "10/01/2021", appoinmentTime: "09:00 AM", clinicImg: Assets.singlepod
        },
        { id: 2, clinicName: "TPSN Mobile Testing Pod", clinicAdd: "98-C Rosa Alvero Street, Loyola Heights, Quezon City", 
          appoinmentDate: "10/01/2021", appoinmentTime: "01:00 PM", clinicImg: Assets.mobilepod
        },
        { id: 3, clinicName: "TPSN Drive-Thru Testing Pod", clinicAdd: "98-C Rosa Alvero Street, Loyola Heights, Quezon City", 
          appoinmentDate: "10/01/2021", appoinmentTime: "01:00 PM", clinicImg: Assets.drivethru
        },
      ]
    };
  }

  componentDidMount() {
    this.props.saveNavigationSelection("Appointment"); 
    this.props.saveNavigationSelectionPage("AppointmentContainer");
  }

  render() {
    const upcomingList = [
      { id: 1, clinicName: "TPSN Single Testing Pod", clinicAdd: "1964 Drive, M. Carreon Street Sta. Ana, Manila", 
        appoinmentDate: "10/01/2021", appoinmentTime: "09:00 AM", clinicImg: Assets.singlepod
      },
      { id: 2, clinicName: "TPSN Mobile Testing Pod", clinicAdd: "98-C Rosa Alvero Street, Loyola Heights, Quezon City", 
        appoinmentDate: "10/01/2021", appoinmentTime: "01:00 PM", clinicImg: Assets.mobilepod
      },
      { id: 3, clinicName: "TPSN Drive-Thru Testing Pod", clinicAdd: "98-C Rosa Alvero Street, Loyola Heights, Quezon City", 
        appoinmentDate: "10/01/2021", appoinmentTime: "01:00 PM", clinicImg: Assets.drivethru
      },
    ];
    // const upcomingList = [];
    const missedList = [
      { id: 1, clinicName: "TPSN Mobile Testing Pod", clinicAdd: "1964 Drive, M. Carreon Street Sta. Ana, Manila", 
        appoinmentDate: "10/01/2021", appoinmentTime: "09:00 AM", clinicImg: Assets.mobilepod
      },
      { id: 2, clinicName: "TPSN Single Testing Pod", clinicAdd: "98-C Rosa Alvero Street, Loyola Heights, Quezon City", 
        appoinmentDate: "10/01/2021", appoinmentTime: "01:00 PM", clinicImg: Assets.drivethru
      },
    ];
    // const missedList = [];
    const completedList = [
      { id: 1, clinicName: "TPSN Single Testing Pod", clinicAdd: "1964 Drive, M. Carreon Street Sta. Ana, Manila", 
        appoinmentDate: "10/01/2021", appoinmentTime: "09:00 AM", clinicImg: Assets.singlepod
      },
      { id: 2, clinicName: "TPSN Drive-Thru Testing Pod", clinicAdd: "98-C Rosa Alvero Street, Loyola Heights, Quezon City", 
        appoinmentDate: "10/01/2021", appoinmentTime: "01:00 PM", clinicImg: Assets.drivethru
      },
    ];
    // const completedList = [];
    const cancelledList = [
      { id: 1, clinicName: "TPSN Drive-Thru Testing Pod", clinicAdd: "1964 Drive, M. Carreon Street Sta. Ana, Manila", 
        appoinmentDate: "10-01-2021", appoinmentTime: "09:00 AM", clinicImg: Assets.drivethru
      },
      { id: 2, clinicName: "TPSN Mobile Testing Pod", clinicAdd: "98-C Rosa Alvero Street, Loyola Heights, Quezon City", 
        appoinmentDate: "10-01-2021", appoinmentTime: "01:00 PM", clinicImg: Assets.mobilepod
      },
    ];
    // const cancelledList = [];
    return (
      <View style={styles.container}>
        <StatusBar 
          barStyle="light-content" 
          backgroundColor="transparent" 
          translucent={true}
        />
        <NavBarComponent 
          navtitle={"My Appointments"} 
          withExtraHeight={false} 
          backButton={() => {
            this.props.saveNavigationSelectionPage("HomeContainer");
            this.props.navigation.goBack();
          }}
        />
        <View style={styles.tabView}>
          <TouchableOpacity
            style={[styles.tabBtn, { borderBottomWidth: this.state.isUpcoming ? 4 : 0, borderColor: this.state.isUpcoming ? EDColors.secondary : EDColors.white }]}
            onPress={() => {
              this.setState({ isUpcoming: true, isMissed: false, isCompleted: false, isCancelled: false, appointmentList: upcomingList, status: "Upcoming" });
            }}>
            <Text style={[styles.tabTitleTxt, { color: this.state.isUpcoming ? EDColors.secondary : EDColors.primary }]}>UPCOMING</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={[styles.tabBtn, { borderBottomWidth: this.state.isMissed ? 4 : 0, borderColor: this.state.isMissed ? EDColors.secondary : EDColors.white }]}
            onPress={() => {
              this.setState({ isUpcoming: false, isMissed: true, isCompleted: false, isCancelled: false, appointmentList: missedList, status: "Missed" });
            }}>
            <Text style={[styles.tabTitleTxt, { color: this.state.isMissed ? EDColors.secondary : EDColors.primary }]}>MISSED</Text>
          </TouchableOpacity> 
          <TouchableOpacity
            style={[styles.tabBtn, { borderBottomWidth: this.state.isCompleted ? 4 : 0, borderColor: this.state.isCompleted ? EDColors.secondary : EDColors.white }]}
            onPress={() => {
              this.setState({ isUpcoming: false, isMissed: false, isCompleted: true, isCancelled: false, appointmentList: completedList, status: "Completed" });
            }}>
            <Text style={[styles.tabTitleTxt, { color: this.state.isCompleted ? EDColors.secondary : EDColors.primary }]}>COMPLETED</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={[styles.tabBtn, { borderBottomWidth: this.state.isCancelled ? 4 : 0, borderColor: this.state.isCancelled ? EDColors.secondary : EDColors.white }]}
            onPress={() => {
              this.setState({ isUpcoming: false, isMissed: false, isCompleted: false, isCancelled: true, appointmentList: cancelledList, status: "Cancelled" });
            }}>
            <Text style={[styles.tabTitleTxt, { color: this.state.isCancelled ? EDColors.secondary : EDColors.primary }]}>CANCELLED</Text>
          </TouchableOpacity>
        </View>
        <ScrollView 
          showsVerticalScrollIndicator={false}
          keyboardDismissMode='on-drag'
          contentContainerStyle={[styles.scrollView, (this.state.appointmentList != undefined && this.state.appointmentList != null && this.state.appointmentList.length > 0 ? { paddingHorizontal: 10, paddingVertical: 15 } : { alignItems: "center", justifyContent: "center" })]}>
          {this.state.appointmentList != undefined &&
            this.state.appointmentList != null &&
            this.state.appointmentList.length > 0 ? (
            <View>
              <FlatList
                horizontal={false}
                showsVerticalScrollIndicator={false}
                data={this.state.appointmentList}
                keyExtractor={(item, index) => item + index}
                renderItem={({ item, index }) => {
                  if (item != undefined) {
                    return (
                      <AppointmentListCard
                        status={this.state.status}
                        clinicImg={item.clinicImg}
                        clinicName={item.clinicName}
                        clinicAdd={item.clinicAdd}
                        appoinmentDate={item.appoinmentDate}
                        appoinmentTime={item.appoinmentTime}
                        onPress={() => {
                        }}
                      />
                    );
                  } else {
                    return null;
                  }
                }}
              /> 
            </View>
          ) : (
            <View style={styles.noDataView}>
              <Image style={styles.hospitalImg} source={Assets.hospital} />
              <Text style={styles.noBookingTxt}>You have no {this.state.status.toLowerCase()} bookings.</Text>
            </View>         
          )}
          </ScrollView>       
      </View>
    );
  }
}

export default connect(
  state => {
    return {
      titleSelected: state.navigationReducer.selectedItem,
      pageSelected: state.navigationReducer.selectedPage
    };
  },
  dispatch => {
    return {
      saveNavigationSelection: dataToSave => {
        dispatch(saveNavigationSelection(dataToSave));
      },
      saveNavigationSelectionPage: data => {
        dispatch(saveNavigationSelectionPage(data));
      }
    };
  }
)(AppointmentContainer);

const styles = StyleSheet.create({
  container: {
    flex: 1, 
    backgroundColor: EDColors.homeBackground
  },
  tabView: {
    flexDirection: "row", 
    backgroundColor: EDColors.white, 
    paddingTop: 15,
    elevation: 5
  },
  tabBtn: {
    width: "25%", 
    alignItems: "center", 
    paddingBottom: 10, 
  },
  tabTitleTxt: {
    fontSize: 12, 
    textAlign: "center",
    fontFamily: ETFonts.bold
  },
  scrollView: {
    flex: 1
  },
  noDataView: {
    alignItems: "center",
    top: -20 
  },
  hospitalImg: {
    width: 200, 
    height: 160, 
    resizeMode: "contain", 
    marginBottom: 30
  },
  noBookingTxt: {
    fontFamily: ETFonts.semibold, 
    fontSize: 15, 
    color: EDColors.primaryGrey
  }
});
