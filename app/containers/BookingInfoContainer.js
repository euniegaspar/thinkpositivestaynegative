import React from "react";
import {
  View,
  Text,
  TouchableOpacity,
  StyleSheet,
  ImageBackground,
  Image,
  ScrollView,
  TextInput,
  Picker,
  Modal
} from "react-native";
import Assets from "../assets";
import { EDColors } from "../assets/Colors";
import { connect } from "react-redux";
import { ETFonts } from "../assets/FontConstants";
import ProgressLoader from '../components/ProgressLoader';
import { saveNavigationSelection, saveNavigationSelectionPage } from "../redux/actions/Navigation";
import LinearGradient from 'react-native-linear-gradient';
import CalendarPicker from "react-native-calendar-picker";
import Moment from "moment";

class BookingInfoContainer extends React.Component {
  constructor(props) {
    super(props);
    if (Text.defaultProps == null) Text.defaultProps = {};
    Text.defaultProps.allowFontScaling = false;

    this.state = {
      availmentType: this.props.navigation.state.params.availmentType,
      testData: this.props.navigation.state.params.clinicInfoData,
      bookingDate: this.props.navigation.state.params.bookingDate,
      bookingTime: this.props.navigation.state.params.bookingTime,
      isFirstPage: true,
      serviceType: this.props.navigation.state.params.serviceType,
      firstname: "",
      lastname: "",
      middlename: "",
      mobileno: "",
      email: "",
      birthday: null,
      age: "",
      gender: 0,
      nationality: "",
      address: "",
      province: "",
      city: "",
      barangay: "",
      zipcode: "",
      isVaccinated: true,
      reason: "",
      isCalendarModal: false
    };
    this.onDateChange = this.onDateChange.bind(this);
  }

  componentDidMount() {
    this.props.saveNavigationSelection("Home"); 
    this.props.saveNavigationSelectionPage("BookingInfoContainer");
  }

  onDateChange(date) {
    this.setState({ birthday: date, isCalendarModal: false });
  }

  calendarModal() {
    return (
      <Modal
        visible={this.state.isCalendarModal}
        animationType="slide"
        transparent={true}
        onRequestClose={() => {
          this.setState({ isCalendarModal: false });
        }}>
        <View style={styles.calendarMdlView}>
          <View style={styles.calendarMdlSubView}>
            <TouchableOpacity
              style={styles.mdlCloseBtn}
              onPress={() => {
                this.setState({ isCalendarModal: false });
              }}>
              <Image source={Assets.close} style={styles.mdlCloseImg}/>
            </TouchableOpacity>
            <View>
              <CalendarPicker
                width={350}
                textStyle={styles.calendarPckr}
                onDateChange={this.onDateChange}
              />
            </View>         
          </View>         
        </View>   
      </Modal>
    )
  }

  render() {
    const { birthday } = this.state;
    const startDate = birthday ? birthday.toString() : '';
    return (
      <View style={styles.container}>
        {this.calendarModal()}
        <ImageBackground
          style={styles.headerImgBckgnd}
          source={this.state.testData.clinicImg}>
          <LinearGradient colors={[EDColors.primary1Gradient, EDColors.primary2Gradient]} start={{x: 0, y: 0}} end={{x: 1, y: 0}} style={styles.headerLG}>
          </LinearGradient>
          <TouchableOpacity
            style={styles.backBtn}
            onPress={() => {
              if (this.state.isFirstPage) {
                this.props.saveNavigationSelectionPage("BookingScheduleContainer");
                this.props.navigation.goBack();
              } else {
                this.setState({ isFirstPage: true });
              }    
            }}>
            <Image style={styles.backImg} source={Assets.backarrow}/>
          </TouchableOpacity>
          <View style={styles.detailsView}>
            <View style={styles.testingPodImgView}>
              <Image style={styles.testingPodImg} source={this.state.testData.clinicImg}/>
            </View>
            <Text style={styles.clinicNameTxt}>{this.state.testData.clinicName}</Text>
            <View style={styles.detailsBottomView}>
              <Text style={styles.bookingDateTxt}>
                {Moment(this.state.bookingDate).format("MMMM DD, YYYY")}
              </Text>
              <Text style={styles.bookingTimeTxt}>
                {this.state.bookingTime}
              </Text>
            </View>
          </View>
        </ImageBackground>
        <ScrollView 
          showsVerticalScrollIndicator={false}
          keyboardDismissMode='on-drag'
          style={styles.scrollView}>
          {this.state.isFirstPage ? (
            <View style={styles.infoView}>
              <Text style={styles.dltTxt}>Last Name *</Text>
              <TextInput
                style={styles.txtInput}
                maxLength={100}
                numberOfLines={1}
                keyboardType="default"
                value={this.state.firstname}
                placeholderTextColor={EDColors.textboxColor}
                onChangeText={text => {
                  this.setState({ firstname: text });
                }}>
              </TextInput>
              <Text style={styles.dltTxt}>First Name *</Text>
              <TextInput
                style={styles.txtInput}
                maxLength={100}
                numberOfLines={1}
                keyboardType="default"
                value={this.state.lastname}
                placeholderTextColor={EDColors.textboxColor}
                onChangeText={text => {
                  this.setState({ lastname: text });
                }}>
              </TextInput>
              <Text style={styles.dltTxt}>Middle Name *</Text>
              <TextInput
                style={styles.txtInput}
                maxLength={100}
                numberOfLines={1}
                keyboardType="default"
                value={this.state.middlename}
                placeholderTextColor={EDColors.textboxColor}
                onChangeText={text => {
                  this.setState({ middlename: text });
                }}>
              </TextInput>
              <View style={styles.flexDirectionRow}>
                <View style={styles.birthView}>
                  <Text style={styles.dltTxt}>Date of Birth *</Text>
                  <View style={[styles.txtInput, { flexDirection: "row", paddingVertical: 9 }]}>
                    <Text style={styles.placeholderTxt}>{this.state.birthday == null ? Moment(new Date()).format("MM/DD/YYYY") : Moment(this.state.birthday).format("MM/DD/YYYY")}</Text>
                    <TouchableOpacity
                      style={styles.calendarBtn}
                      onPress={() => {
                        this.setState({ isCalendarModal: true });
                      }}>
                      <Image style={styles.calendarImg} source={Assets.appointment_active}/>
                    </TouchableOpacity>
                  </View>
                </View>
                <View style={styles.ageView}>
                  <Text style={styles.dltTxt}>Age *</Text>
                  <TextInput
                    style={styles.txtInput}
                    maxLength={100}
                    numberOfLines={1}
                    keyboardType="email-address"
                    value={this.state.email}
                    placeholderTextColor={EDColors.textboxColor}
                    onChangeText={text => {
                      this.setState({ email: text });
                    }}>
                  </TextInput>
                </View>
                <View style={styles.ntlView}>
                  <Text style={styles.dltTxt}>Nationality *</Text>
                  <TextInput
                    style={styles.txtInput}
                    maxLength={100}
                    numberOfLines={1}
                    keyboardType="default"
                    value={this.state.nationality}
                    placeholderTextColor={EDColors.textboxColor}
                    onChangeText={text => {
                      this.setState({ nationality: text });
                    }}>
                  </TextInput>
                </View>
              </View>
              <Text style={styles.dltTxt}>Gender *</Text>
              <View style={styles.radioView}>
                <TouchableOpacity style={styles.radioBtn}
                  onPress={() => {
                    this.setState({ gender: 0 });
                  }}>
                  {this.state.gender == 0 &&
                    <View style={styles.radioRound}/>
                  }
                </TouchableOpacity>
                <Text style={styles.selectionTxt}>Male</Text>
                <View style={styles.radioSeperatorSpace}/>
                <TouchableOpacity style={styles.radioBtn}
                  onPress={() => {
                    this.setState({ gender: 1 });
                  }}>
                  {this.state.gender == 1 &&
                    <View style={styles.radioRound}/>
                  }
                </TouchableOpacity>
                <Text style={styles.selectionTxt}>Female</Text>
              </View>
              <Text style={styles.dltTxt}>Mobile Number *</Text>
              <TextInput
                style={styles.txtInput}
                maxLength={13}
                numberOfLines={1}
                keyboardType="numeric"
                value={this.state.mobileno}
                placeholderTextColor={EDColors.textboxColor}
                onChangeText={text => {
                  this.setState({ mobileno: text });
                }}>
              </TextInput>
              <Text style={styles.dltTxt}>Email Address *</Text>
              <TextInput
                style={styles.txtInput}
                maxLength={100}
                numberOfLines={1}
                keyboardType="email-address"
                value={this.state.email}
                placeholderTextColor={EDColors.textboxColor}
                onChangeText={text => {
                  this.setState({ email: text });
                }}>
              </TextInput>
            </View>
          ) : (
            <View style={styles.infoView}>
              <Text style={styles.dltTxt}>Address *</Text>
              <TextInput
                style={styles.txtInput}
                maxLength={200}
                numberOfLines={1}
                keyboardType="default"
                value={this.state.address}
                placeholderTextColor={EDColors.textboxColor}
                onChangeText={text => {
                  this.setState({ address: text });
                }}>
              </TextInput>
              <Text style={styles.dltTxt}>Province *</Text>
              <View style={styles.txtInput}>
                <Picker
                  selectedValue={this.state.province}
                  style={styles.picker}
                  onValueChange={(itemValue) => this.setState({ province: itemValue })}>
                    <Picker.Item label={`\t\tSelect Province`} value='' />
                    <Picker.Item label={`\t\tNueva Vizcaya`} value="0" />
                    <Picker.Item label={`\t\tNueva Ecija`} value="1" />
                    <Picker.Item label={`\t\tPangasinan`} value="2" />
                </Picker>
              </View>
              <Text style={styles.dltTxt}>City/Municipality *</Text>
              <View style={styles.txtInput}>
                <Picker
                  selectedValue={this.state.city}
                  style={styles.picker}
                  onValueChange={(itemValue) => this.setState({ city: itemValue })}>
                    <Picker.Item label={`\t\tSelect City/Municipality`} value='' />
                    <Picker.Item label={`\t\tSolano`} value="0" />
                    <Picker.Item label={`\t\tBayombong`} value="1" />
                    <Picker.Item label={`\t\tBambang`} value="2" />
                </Picker>
              </View>
              <View style={styles.flexDirectionRow}>
                <View style={styles.brgyView}>
                  <Text style={styles.dltTxt}>Barangay *</Text>
                  <View style={styles.txtInput}>
                    <Picker
                      selectedValue={this.state.barangay}
                      style={styles.picker}
                      onValueChange={(itemValue) => this.setState({ barangay: itemValue })}>
                        <Picker.Item label={`\t\tSelect Barangay`} value='' />
                        <Picker.Item label={`\t\tRoxas`} value="0" />
                        <Picker.Item label={`\t\tOsmena`} value="1" />
                        <Picker.Item label={`\t\tQuezon`} value="2" />
                    </Picker>
                  </View>
                </View>
                <View style={styles.zipCodeView}>
                  <Text style={styles.dltTxt}>ZIP Code</Text>
                  <TextInput
                    style={styles.txtInput}
                    maxLength={200}
                    numberOfLines={1}
                    keyboardType="default"
                    value={this.state.zipcode}
                    placeholderTextColor={EDColors.textboxColor}
                    onChangeText={text => {
                      this.setState({ zipcode: text });
                    }}>
                  </TextInput>
                </View>
              </View>
              <Text style={styles.dltTxt}>Fully Vaccinated?</Text>
              <View style={styles.radioView}>
                <TouchableOpacity style={styles.radioBtn}
                  onPress={() => {
                    this.setState({ isVaccinated: true });
                  }}>
                  {this.state.isVaccinated &&
                    <View style={styles.radioRound}/>
                  }
                </TouchableOpacity>
                <Text style={styles.selectionTxt}>Yes</Text>
                <View style={styles.radioSeperatorSpace}/>
                <TouchableOpacity style={styles.radioBtn}
                  onPress={() => {
                    this.setState({ isVaccinated: false });
                  }}>
                  {!this.state.isVaccinated &&
                    <View style={styles.radioRound}/>
                  }
                </TouchableOpacity>
                <Text style={styles.selectionTxt}>No</Text>
              </View>
              <Text style={styles.dltTxt}>Reason For Test</Text>
              <View style={styles.txtInput}>
                <Picker
                  selectedValue={this.state.reason}
                  style={styles.picker}
                  onValueChange={(itemValue) => this.setState({ reason: itemValue })}>
                    <Picker.Item label={`\t\tSelect`} value='' />
                    <Picker.Item label={`\t\tTravel Clearance`} value="0" />
                    <Picker.Item label={`\t\tPUI`} value="1" />
                </Picker>
              </View>
            </View>
          )}    
        </ScrollView>
        <View style={styles.footerView}>
          <View style={styles.footerTopView}>
            <Text style={styles.selectedSrvcTxt}>{this.state.serviceType == 0 ? "RT-PCR Test" : "Antigen Test"}</Text>
            <Text style={styles.totalLblTxt}>Total: </Text>
            <Text style={styles.priceTxt}>₱ 0,000.00</Text>
          </View>
          <TouchableOpacity
            style={styles.nextBtn}
            onPress={() => { 
              if (this.state.isFirstPage) {
                this.setState({ isFirstPage: false });
              } else {
                this.props.navigation.navigate("BookingSummaryContainer", {
                  testData: this.state.testData,
                  bookingDate: Moment(this.state.bookingDate).format("MM/DD/YYYY"),
                  bookingTime: this.state.bookingTime,
                  fullName: this.state.firstname + " " + this.state.middlename + " " + this.state.lastname,
                  serviceType: this.state.serviceType,
                  availmentType: this.state.availmentType
                });
              }
            }}>
            <LinearGradient colors={[EDColors.secondary1Gradient, EDColors.secondary2Gradient]} start={{x: 0, y: 0}} end={{x: 1, y: 0}} style={styles.nextLG}>
              <Text style={styles.nextNowTxt}>
                {this.state.isFirstPage ? "NEXT" : "BOOK NOW"}
              </Text>
            </LinearGradient>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

export default connect(
  state => {
    return {
      titleSelected: state.navigationReducer.selectedItem,
      pageSelected: state.navigationReducer.selectedPage
    };
  },
  dispatch => {
    return {
      saveNavigationSelection: dataToSave => {
        dispatch(saveNavigationSelection(dataToSave));
      },
      saveNavigationSelectionPage: data => {
        dispatch(saveNavigationSelectionPage(data));
      }
    };
  }
)(BookingInfoContainer);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: EDColors.homeBackground
  },
  flexDirectionRow: {
    flexDirection: "row"
  },
  headerImgBckgnd: {
    height: 250, 
    width: "100%", 
    resizeMode: "cover", 
    borderBottomLeftRadius: 30, 
    borderBottomRightRadius: 30, 
    overflow: "hidden", 
    elevation: 5
  },
  headerLG: {
    height: 250, 
    width: "100%", 
    opacity: 0.9
  },
  backBtn: {
    position: "absolute", 
    left: 15, 
    top: 45 
  },
  backImg: {
    width: 20, 
    height: 20, 
    resizeMode: "contain"
  },
  detailsView: {
    position: 'absolute', 
    top: 0, 
    left: 0, 
    right: 0, 
    bottom: 20, 
    justifyContent: "flex-end", 
    alignItems: 'center'
  },
  testingPodImgView: {
    borderWidth: 1, 
    borderColor: EDColors.white, 
    borderRadius: 10, 
    overflow: "hidden", 
    marginBottom: 5
  },
  testingPodImg: {
    width: 85, 
    height: 85, 
    resizeMode: "cover"
  },
  clinicNameTxt: {
    fontFamily: ETFonts.bold, 
    color: EDColors.white, 
    marginBottom: 10 
  },
  detailsBottomView: {
    alignItems: "center"
  },
  bookingDateTxt: {
    fontFamily: ETFonts.regular, 
    color: EDColors.white, 
    fontSize: 12, 
    textAlign: "center", 
    marginHorizontal: 30
  },
  bookingTimeTxt: {
    fontFamily: ETFonts.bold, 
    color: EDColors.white, 
    fontSize: 18, 
    textAlign: "center", 
    marginHorizontal: 30 
  },
  scrollView: {
    flex: 1, 
    marginBottom: 125, 
    borderRadius: 10
  },
  infoView: {
    backgroundColor: EDColors.white, 
    marginHorizontal: 10, 
    marginVertical: 15, 
    padding: 15, 
    borderRadius: 10
  },
  dltTxt: {
    fontFamily: ETFonts.bold, 
    fontSize: 14, 
    color: EDColors.black, 
    marginBottom: 5
  },
  txtInput: {
    borderRadius: 5, 
    borderColor: EDColors.textboxColor2, 
    borderWidth: 1, 
    paddingHorizontal: 10, 
    paddingVertical: 5,
    marginBottom: 10,
    fontFamily: ETFonts.regular
  },
  placeholderTxt: {
    fontFamily: ETFonts.regular, 
    flex: 1
  },
  birthView: {
    width: "38%"
  },
  calendarBtn: {
    width: 20, 
    height: 20
  },
  calendarImg: {
    width: 20, 
    height: 20, 
    resizeMode: "contain", 
    tintColor: EDColors.primaryGrey
  },
  ageView: {
    width: "15%", 
    marginHorizontal: 10
  },
  ntlView: {
    width: "41%"
  },
  picker: {
    marginLeft: -10,
    marginVertical: -11,
    fontFamily: ETFonts.regular,
    alignItems: "center"
  },
  radioView: {
    flexDirection: "row",
    marginBottom: 10
  },
  radioBtn: {
    height: 20, 
    width: 20, 
    borderWidth: 1, 
    borderColor: EDColors.paleGray, 
    borderRadius: 20, 
    alignItems: "center", 
    justifyContent: "center", 
    marginRight: 10
  },
  radioRound: {
    backgroundColor: EDColors.green, 
    borderRadius: 20, 
    height: 13, 
    width: 13
  },
  radioSeperatorSpace: {
    marginRight: 110 
  },
  selectionTxt: {
    fontFamily: ETFonts.regular, 
    color: EDColors.black, 
    fontSize: 13
  },
  brgyView: {
    width: "55%", 
    marginRight: 10 
  },
  zipCodeView: {
    width: "42%"
  },
  nextBtn: {
    borderRadius: 50,
    overflow: "hidden",
    marginHorizontal: 20,
    marginBottom: 20
  },
  nextLG: {
    padding: 5
  },
  nextNowTxt: {
    fontSize: 18,
    fontFamily: ETFonts.bold,
    textAlign: 'center',
    margin: 5,
    color: EDColors.white
  },
  footerView: {
    position: 'absolute', 
    left: 0, 
    right: 0, 
    bottom: 0, 
    backgroundColor: EDColors.white, 
    elevation: 10
  },
  footerTopView: {
    flexDirection: "row", 
    margin: 20
  },
  selectedSrvcTxt: {
    color: EDColors.black, 
    flex: 1, 
    fontFamily: ETFonts.roboto_regular
  },
  totalLblTxt: {
    fontFamily: ETFonts.roboto_bold, 
    color: EDColors.black, 
    marginRight: 10
  },
  priceTxt: {
    fontFamily: ETFonts.roboto_bold, 
    color: EDColors.primary
  },
  calendarMdlView: {
    flex: 1, 
    justifyContent: "center", 
    backgroundColor: "rgba(0,0,0,0.50)"
  },
  calendarMdlSubView: {
    backgroundColor: EDColors.white, 
    padding: 10, 
    margin: 20, 
    borderRadius: 6
  },
  mdlCloseBtn: {
    height: 15, 
    width: 15, 
    alignSelf: "flex-end", 
    marginBottom: 10
  },
  mdlCloseImg: {
    height: 15, 
    width: 15
  },
  calendarPckr: {
    fontFamily: ETFonts.regular
  }
});
