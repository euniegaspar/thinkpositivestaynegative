import React from "react";
import {
  StatusBar,
  View,
  Image,
  Text,
  TextInput,
  TouchableOpacity,
  ScrollView,
  StyleSheet,
  ImageBackground,
  FlatList
} from "react-native";
import { EDColors } from "../assets/Colors";
import { ETFonts } from "../assets/FontConstants";
import Assets from "../assets";
import { connect } from "react-redux";
import { saveNavigationSelection, saveNavigationSelectionPage } from "../redux/actions/Navigation";
import AppointmentCard from "../components/AppointmentCard";
import LinearGradient from 'react-native-linear-gradient';

class HomeContainer extends React.Component {
  constructor(props) {
    super(props);
    Text.defaultProps = Text.defaultProps || {};
    Text.defaultProps.allowFontScaling = false;
    TextInput.defaultProps = TextInput.defaultProps || {};
    TextInput.defaultProps.allowFontScaling = false;

    this.state = {
      username: "Juan",
      searchCenter: ""
    };
  }
  
  componentDidMount() {    
    this.props.saveNavigationSelection("Home");
    this.props.saveNavigationSelectionPage("HomeContainer");
  }

  render() {
    const appointmentList = [
      { id: 1, clinicName: "TPSN Single Testing Pod", clinicAdd: "1964 Drive, M. Carreon Street Sta. Ana, Manila", 
        appointmentDate: "10-01-2021", appointmentTime: "09:00 AM", clinicImg: Assets.singlepod
      },
      { id: 2, clinicName: "TPSN Mobile Testing Pod", clinicAdd: "98-C Rosa Alvero Street, Loyola Heights, Quezon City", 
        appointmentDate: "10-01-2021", appointmentTime: "01:00 PM", clinicImg: Assets.mobilepod
      },
    ];
    // const appointmentList = [];
    const testingCenterList = [
      { id: 1, clinicName: "TPSN Single Testing Pod", clinicAdd: "1964 Drive, M. Carreon Street Sta. Ana, Manila", 
        clinicImg: Assets.singlepod, rating: 3
      },
      { id: 2, clinicName: "TPSN Mobile Testing Pod", clinicAdd: "98-C Rosa Alvero Street, Loyola Heights, Quezon City", 
        clinicImg: Assets.mobilepod, rating: 4.5
      },
    ];
    const isNoAppoinmentYet = appointmentList != undefined && appointmentList != null && appointmentList.length > 0 ? false : true;
    return (
      <ScrollView 
        showsVerticalScrollIndicator={false}
        keyboardDismissMode='on-drag'
        style={styles.container}>
        <StatusBar 
          barStyle="light-content" 
          backgroundColor="transparent" 
          translucent={true}
        />
        <View style={styles.subContainer}>
          <View style={styles.headerContainer}>
            <Image 
              style={styles.imgHeaderBg}
              source={Assets.bgHomeHeader}/>
          </View>
          <View style={styles.topMenuContainer}>
            <View style={styles.welcomeView}>
              <Image 
                style={styles.logoImg}
                source={Assets.logo2}/>
              <Text style={styles.welcomeTxt}>Hi {this.state.username}!</Text>
            </View>
            <View style={styles.rightMenuView}> 
              <TouchableOpacity
                style={styles.testResultsBtn}
                onPress={() => { }}>
                <LinearGradient colors={[EDColors.secondary1Gradient, EDColors.secondary2Gradient]} start={{x: 0, y: 0}} end={{x: 1, y: 0}} style={styles.testResultsLG}>
                  <Text style={styles.testResultsTxt}>
                    VIEW TEST RESULTS
                  </Text>
                </LinearGradient>
              </TouchableOpacity>
              <TouchableOpacity
                style={styles.rightMenuImgBtn}
                onPress={() => {}}>
                <Image 
                  style={styles.rightMenuImg}
                  source={Assets.bell}/>
                <View style={styles.redIndicator}/>
              </TouchableOpacity>
            </View>
          </View>
          <View style={styles.searchContainer}>
            <TextInput
              style={styles.searchTI}
              maxLength={60}
              numberOfLines={1}
              keyboardType="default"
              value={this.state.searchCenter}
              placeholderTextColor={EDColors.textboxColor}
              placeholder={"Search Testing Center"}
              onChangeText={text => {
                this.setState({ searchCenter: text });
              }}>
            </TextInput>
            <TouchableOpacity
              style={styles.searchBtn}
              onPress={() => {}}>
              <Image 
                style={styles.searchImg}
                source={Assets.search}/>
            </TouchableOpacity>
          </View>
          <View style={styles.bookAppntmntView}> 
            <Text style={styles.bookAppntmntTxt}>
              Book An Appointment
            </Text>
            <View style={styles.bookView}>
              <TouchableOpacity
                style={styles.bookAppntmntCard}
                onPress={() => {
                  this.props.navigation.navigate("TestingCenterContainer", {
                    availmentType: "1" // Myself
                  }); 
                }}>
                <Image source={Assets.profile_inactive} style={[styles.usersImg, { width: 25 }]}/>
                <Text style={styles.bookTxt}>Book for{'\n'}Myself</Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={styles.bookAppntmntCard} 
                onPress={() => {
                  this.props.navigation.navigate("TestingCenterContainer", {
                    availmentType: "2" // Someone else
                  }); 
                }}>
                <Image source={Assets.users} style={[styles.usersImg, { width: 40 }]}/>
                <Text style={styles.bookTxt}>Book for{'\n'}Someone Else</Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={[styles.bookAppntmntCard, { marginRight: 0 } ]}
                onPress={() => {
                  this.props.navigation.navigate("TestingCenterContainer", {
                    availmentType: "3" // Group
                  }); 
                }}>
                <Image source={Assets.usergroup} style={[styles.usersImg, { width: 45 }]}/>
                <Text style={styles.bookTxt}>Book for{'\n'}Group/Family</Text>
              </TouchableOpacity>
            </View>
          </View>
          <View style={styles.preventionCard}>
            <Text style={styles.preventionTxt}>
              Prevention
            </Text>
            <View style={styles.preventionContainer}>
              <View style={styles.preventionView}>
                <Image source={Assets.prevention1} style={styles.preventionImg} />
                <Text style={styles.preventionsTxt}>
                  Observe Physical{'\n'}Distancing
                </Text>
              </View>
              <View style={styles.preventionView}>
                <Image source={Assets.prevention2} style={styles.preventionImg} />
                <Text style={styles.preventionsTxt}>
                  Always Wear{'\n'}A Facemask
                </Text>
              </View>
              <View style={[styles.preventionView, { marginRight: 0 }]}>
                <Image source={Assets.prevention3} style={styles.preventionImg} />
                <Text style={styles.preventionsTxt}>
                  Wash your{'\n'}Hands
                </Text>
              </View>
            </View>
          </View>
          <View style={styles.appntmntContainer}>
            <View style={styles.appntmntTop}>
              <Text style={styles.appntmntTxt}>{isNoAppoinmentYet ? "Featured Testing Center" : "Upcoming Appointments"}</Text>
              {!isNoAppoinmentYet && (
                <TouchableOpacity
                  onPress={() => {}}>
                  <Text style={styles.seeAllTxt}>See All</Text>
                </TouchableOpacity>
              )}   
            </View>
            <FlatList
              horizontal={false}
              showsVerticalScrollIndicator={false}
              data={isNoAppoinmentYet ? testingCenterList : appointmentList}
              keyExtractor={(item, index) => item + index}
              renderItem={({ item, index }) => {
                if (item != undefined) {
                  return (
                    isNoAppoinmentYet ? (
                      <AppointmentCard 
                        isNoAppoinmentYet={true}
                        clinicImg={item.clinicImg}
                        clinicName={item.clinicName}
                        clinicAdd={item.clinicAdd}
                        onPress={() => {
                          this.props.navigation.navigate("BookingScheduleContainer", {
                            testingCenterData: item
                          });
                        }}
                      />
                    ) : (
                      <AppointmentCard 
                        isNoAppoinmentYet={false}
                        clinicImg={item.clinicImg}
                        clinicName={item.clinicName}
                        clinicAdd={item.clinicAdd}
                        appoinmentDate={item.appointmentDate}
                        appoinmentTime={item.appointmentTime}
                        onPress={() => {}}
                      />
                    )                
                  );
                } else {
                  return null;
                }
              }}
            />
          </View>
          <ImageBackground
            source={Assets.footerhomebg}
            style={styles.footerContainer}>
            <View style={styles.footerSubContainer}>
              <View style={styles.footerRightView}>
                <Text style={styles.footer1Txt}>Are you feeling sick?</Text>
                <Text style={styles.footer2Txt}>If you are feeling sick of any symptoms of the COVID-19, please call or SMS for your and society betterment.</Text>
              </View>
              <View style={styles.footerLeftView}>
                <Text style={styles.footer3Txt}>Want to try our COVID-19 self-checker?</Text>
                <TouchableOpacity
                  style={styles.selfCheckerBtn}
                  onPress={() => { }}>
                  <LinearGradient colors={[EDColors.secondary1Gradient, EDColors.secondary2Gradient]} start={{x: 0, y: 0}} end={{x: 1, y: 0}} style={styles.checkerLG}>
                    <Text style={styles.selfCheckTxt}>
                      SELF-CHECKER
                    </Text>
                  </LinearGradient>
                </TouchableOpacity>
             </View>
            </View>
          </ImageBackground>
        </View>
      </ScrollView>
    );
  }
}

export default connect(
  state => {
    return {
      titleSelected: state.navigationReducer.selectedItem,
      pageSelected: state.navigationReducer.selectedPage
    };
  },
  dispatch => {
    return {
      saveNavigationSelection: dataToSave => {
        dispatch(saveNavigationSelection(dataToSave));
      },
      saveNavigationSelectionPage: data => {
        dispatch(saveNavigationSelectionPage(data));
      }
    };
  }
)(HomeContainer);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: EDColors.homeBackground
  },
  subContainer: {
    marginHorizontal: 15, 
    marginBottom: 15
  },
  headerContainer: {
    position: "absolute",
    top: 0,
    bottom: 0, 
    right: 0,
    left: 0,
    height: 250, 
    borderBottomLeftRadius: 30, 
    borderBottomRightRadius: 30, 
    marginHorizontal: -20,
    overflow: "hidden"
  },
  imgHeaderBg: {
    width: "110%", 
    height: "100%",
    resizeMode: "cover",
    right: 10
  },
  topMenuContainer: {
    flexDirection: "row", 
    marginTop: 40, 
    marginBottom: 15,
    marginHorizontal: 5
  },
  welcomeView: {
    flex: 1, 
    flexDirection: "row",
    marginLeft: 10
  },
  logoImg: {
    height: 30, 
    width: 30, 
    resizeMode: "contain", 
    marginRight: 10
  },
  welcomeTxt: {
    fontFamily: ETFonts.regular, 
    color: EDColors.white, 
    fontSize: 17
  },
  rightMenuView: {
    flexDirection: "row",
    alignItems: "center"
  },
  testResultsBtn: {
    borderRadius: 20,
    overflow: "hidden",
    elevation: 10,
    marginRight: 10
  },
  testResultsLG: {
    paddingHorizontal: 10
  },
  testResultsTxt: {
    fontSize: 12,
    fontFamily: ETFonts.roboto_bold,
    textAlign: 'center',
    marginHorizontal: 10,
    marginVertical: 8,
    color: EDColors.white
  },
  rightMenuImgBtn: {
    height: 20, 
    width: 20
  },
  rightMenuImg: {
    height: 20, 
    width: 20, 
    resizeMode: "contain"
  },
  redIndicator: {
    position: "absolute", 
    top: -2, 
    left: 16, 
    height: 5, 
    width: 5, 
    backgroundColor: EDColors.red, 
    borderRadius: 5, 
    borderColor: EDColors.white, 
    borderWidth: 1
  },
  searchContainer: {
    flexDirection: "row", 
    backgroundColor: EDColors.white, 
    borderRadius: 50, 
    paddingHorizontal: 15, 
    marginHorizontal: 5,
    marginBottom: 30,
    alignItems: "center", 
    elevation: 5
  },
  searchTI: {
    fontFamily: ETFonts.regular, 
    fontSize: 14, 
    paddingVertical: 5, 
    flex: 1
  },
  searchBtn: {
    height: 30, 
    width: 30, 
    alignItems: "center", 
    justifyContent: "center", 
    left: 5
  },
  searchImg: {
    height: 22,
    width: 22, 
    resizeMode: "contain"
  },
  bookAppntmntView: {
    alignItems: "center", 
    flexDirection: "column"
  },
  bookAppntmntTxt: {
    fontFamily: ETFonts.bold, 
    color: EDColors.white, 
    fontSize: 15
  },
  bookView: {
    flexDirection: "row", 
    justifyContent: "space-between", 
    marginTop: 15
  },
  bookAppntmntCard: {
    backgroundColor: 'rgba(255, 255, 255, 0.95)', 
    borderRadius: 20, 
    paddingHorizontal: 5,
    paddingVertical: 15, 
    alignItems: "center", 
    justifyContent: "center",
    elevation: 5, 
    marginRight: 20,
    overflow: 'hidden',
    width: "28%"
  },
  usersImg: {
    height: 25, 
    resizeMode: "contain", 
    marginBottom: 10, 
    overflow: "hidden" 
  },
  bookTxt: {
    fontFamily: ETFonts.regular, 
    textAlign: "center", 
    fontSize: 11, 
    color: EDColors.primary
  },
  preventionCard: {
    backgroundColor: EDColors.white, 
    borderRadius: 10, 
    paddingVertical: 10, 
    paddingHorizontal: 20, 
    marginTop: 20 
  },
  preventionTxt: {
    fontFamily: ETFonts.bold, 
    color: EDColors.primary, 
    marginBottom: 5 
  },
  preventionContainer: {
    flexDirection: "row", 
    alignItems: "center", 
    justifyContent: "center", 
    marginBottom: 5
  },
  preventionView: {
    marginRight: 20, 
    width: "30%", 
    alignItems: "center"
  },
  preventionImg: {
    height: 60, 
    width: 60, 
    resizeMode: "contain"
  },
  preventionsTxt: {
    textAlign: "center", 
    fontFamily: ETFonts.regular, 
    color: EDColors.primary, 
    fontSize: 11,
    marginTop: 5
  },
  appntmntContainer: {
    backgroundColor: EDColors.white, 
    padding: 10, 
    paddingBottom: 0, 
    borderRadius: 10, 
    marginVertical: 15, 
    elevation: 5
  },
  appntmntTop: {
    flexDirection: "row", 
    paddingHorizontal: 10, 
    alignItems: "center", 
    marginBottom: 10
  },
  appntmntTxt: {
    flex: 1, 
    fontFamily: ETFonts.bold, 
    color: EDColors.primary
  },
  seeAllTxt: {
    fontFamily: ETFonts.bold, 
    color: EDColors.primary, 
    fontSize: 12
  },
  footerContainer: {
    width: "100%", 
    height: 150, 
    resizeMode: "cover", 
    borderRadius: 10, 
    overflow: "hidden" 
  },
  footerSubContainer: {
    paddingHorizontal: 20, 
    paddingVertical: 10, 
    flexDirection: "row"
  },
  footerRightView: {
    width: "50%",
    marginRight: 10
  },
  footer1Txt: {
    fontFamily: ETFonts.semibold, 
    color: EDColors.white, 
    fontSize: 15, 
    marginBottom: 5
  },
  footer2Txt: {
    fontFamily: ETFonts.semibold, 
    color: EDColors.white, 
    fontSize: 12
  },
  footerLeftView: {
    width: "50%", 
    alignSelf: "flex-end",
    top: 35
  },
  footer3Txt: {
    fontFamily: ETFonts.semibold, 
    color: EDColors.white, 
    fontSize: 12, 
    textAlign: "center", 
    marginBottom: 10
  },
  selfCheckerBtn: {
    elevation: 5,
    borderRadius: 20,
    overflow: "hidden"
  },
  checkerLG: {
    paddingHorizontal: 5
  },
  selfCheckTxt: {
    fontSize: 13,
    fontFamily: ETFonts.roboto_bold,
    textAlign: 'center',
    margin: 10,
    color: EDColors.white
  }
});