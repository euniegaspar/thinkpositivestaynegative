import React from "react";
import {
  View,
  Text,
  TextInput,
  TouchableOpacity,
  StyleSheet,
  Dimensions,
  Image,
  Platform,
  PermissionsAndroid
} from "react-native";
import ProgressLoader from "../components/ProgressLoader";
import { EDColors } from "../assets/Colors";
import { ETFonts } from "../assets/FontConstants";
import { connect } from "react-redux";
import { GOOGLE_API_KEY } from "../utils/Constants";
import LocationServicesDialogBox from "react-native-android-location-services-dialog-box";
import { saveNavigationSelection, saveNavigationSelectionPage } from "../redux/actions/Navigation";
import MapView, { PROVIDER_GOOGLE, Marker, Polyline, Callout } from "react-native-maps";
import Assets from "../assets";
import { 
  isLocationEnable,
  getGeocodeLocationByCoords,
  getWatchLocation,
  stopGetWatchLocation,
  stopObserveLocation
} from "../utils/LocationCheck";
import Geocoder from "react-native-geocoding";
import BackgroundGeolocation from '@mauron85/react-native-background-geolocation';
import NavBarComponent from "../components/NavBarComponent";

let { width, height } = Dimensions.get("window");
const ASPECT_RATIO = width / height;
const LATITUDE = 16.4804;
const LONGITUDE = 121.1481;
const LATITUDE_DELTA = 0.0922;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;

class NearbyContainer extends React.Component {
  constructor(props) {
    super(props);
    Text.defaultProps = Text.defaultProps || {};
    Text.defaultProps.allowFontScaling = false;
    TextInput.defaultProps = TextInput.defaultProps || {};
    TextInput.defaultProps.allowFontScaling = false;

    this.state = {
      mapLocation: {
        latitude: LATITUDE,
        longitude: LONGITUDE,
        latitudeDelta: LATITUDE_DELTA,
        longitudeDelta: LONGITUDE_DELTA
      },
      markerLocation: {
        latitude: LATITUDE,
        longitude: LONGITUDE,
        latitudeDelta: LATITUDE_DELTA,
        longitudeDelta: LONGITUDE_DELTA
      },
      isLoading: false,
      latitude: 0.0,
      longitude: 0.0
    };
  }

  watchID = null;
  
  componentDidMount() {    
    this.props.saveNavigationSelection("Nearby");
    this.props.saveNavigationSelectionPage("NearbyContainer");

    this.requestLocationPermission();
  }

  componentWillUnmount() {
    if (Platform.OS == "android") {
      LocationServicesDialogBox.stopListener(); 
    }
    stopGetWatchLocation(this.watchID);
    stopObserveLocation();
  }

  requestLocationPermission() {
    try {
        PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION)
        .then(onfulfilled => {
            if (onfulfilled === PermissionsAndroid.RESULTS.GRANTED) {
                this.allowGeolocation();
            } else if(onfulfilled === PermissionsAndroid.RESULTS.NEVER_ASK_AGAIN) {
                BackgroundGeolocation.showAppSettings();
            } else {
                BackgroundGeolocation.showLocationSettings();
            }
        })
        .catch(onrejected => {
            this.props.onRejected(onrejected);
        });
    } catch (err) {
        console.warn(err);
    }
  }

  allowGeolocation() {
    let config = {
      desiredAccuracy: BackgroundGeolocation.HIGH_ACCURACY,
      distanceFilter: 5,
      debug: false,
      startOnBoot: false,
      stopOnTerminate: true,
      notificationsEnabled: false,
      locationProvider: BackgroundGeolocation.DISTANCE_FILTER_PROVIDER,
      interval: 5000,
      fastestInterval: 2000,
      activitiesInterval: 10000,
      stopOnStillActivity: false,
    }

    try {
      BackgroundGeolocation.configure(config, success => {
        console.log(success);
      }, failure => {
        console.log(failure);
      });

      BackgroundGeolocation.checkStatus(status => {
        if(status.locationServicesEnabled == false) {
          BackgroundGeolocation.showLocationSettings();
        }
      });

      isLocationEnable(
        success => {
          this.getCurrentAddress();
        },
        error => {
          showValidationAlert("Please allow location access from setting");
        },
        backPress => {
          console.log(backPress);
        }
      );
    } catch (error) {
      console.log(error);
    }
  }

  getCurrentLocation() {
    PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION)
    .then(onfulfilled => {
        if(onfulfilled === PermissionsAndroid.RESULTS.GRANTED) {
          BackgroundGeolocation.getCurrentLocation(location => {
            this.props.onLocation(location);
          });
        }
    })
    .catch(onrejected => {
        this.props.onRejected(onrejected);
    });
  }

  getCurrentAddress() {
    Geocoder.init(GOOGLE_API_KEY);
    this.setState({ isLoading: true });
    this.watchID = getWatchLocation(onSuccess => {
      setTimeout(() => {
        this.setState({
          ...onSuccess,
          markerLocation: {
            ...onSuccess,
            latitudeDelta: LATITUDE_DELTA,
            longitudeDelta: LONGITUDE_DELTA
          },
          mapLocation: {
            ...onSuccess,
            latitudeDelta: LATITUDE_DELTA,
            longitudeDelta: LONGITUDE_DELTA
          }
        });
        this.getAddress(onSuccess.latitude, onSuccess.longitude);
      }, 2500);
    }, onFailed => {
      this.setState({ isLoading: false });
      this.setState({ error: onFailed.message });
    });
  }

  getAddress(lat, long, addressDescription = null) {
    getGeocodeLocationByCoords(lat, long).then(addressComponent => {
      // if(addressDescription != null) {
      //   addressComponent = {...addressComponent, strAddress: addressDescription};
      // }
      this.setState({
        // ...addressComponent,
        isLoading: false
      });
      stopObserveLocation();
    })
    .catch(error => {
      this.setState({ isLoading: false });
    });
  }

  handleRegionChange = mapLocation => {
    this.setState({
      markerLocation: { latitude: mapLocation.latitude, longitude: mapLocation.longitude },
      mapLocation
    });
  };

  render() {
    return (
        <View style={styles.container}>
          <NavBarComponent 
            navtitle={"Testing Pod Locator"}   
            withExtraHeight={false} 
            backButton={() => {
              this.props.saveNavigationSelectionPage("HomeContainer");
              this.props.navigation.goBack();
            }}
          />
          <ProgressLoader isLoading={this.state.isLoading}/>
          <MapView
            style={styles.mapContainer}
            provider='google'
            mapType='standard'
            showsScale={true}
            showsBuildings={true}
            showsPointsOfInterest={true}
            zoomEnabled={true}
            showsUserLocation={true}
            zoom={100}
            region={this.state.mapLocation}
            onRegionChangeComplete={this.handleRegionChange}
            onPress={e => {
              this.setState({
                latitude: e.nativeEvent.coordinate.latitude,
                longitude: e.nativeEvent.coordinate.longitude,
                isLoading: true
              });
              this.getAddress(
                e.nativeEvent.coordinate.latitude,
                e.nativeEvent.coordinate.longitude
              );
            }}>
            <Marker
              coordinate={{
                latitude: this.state.latitude,
                longitude: this.state.longitude
              }} 
              image={Assets.yellow_location} 
              onDragEnd={e => {
                console.log('dragEnd', e.nativeEvent.coordinate);
              }}
              tracksViewChanges={true}
            />
          </MapView>
          <View style={styles.pointCurLocView}>
            <TouchableOpacity
              style={styles.pointCurLocBtn} 
              onPress={() => {
                if(this.watchID != null) {
                  stopGetWatchLocation(this.watchID);
                  this.watchID = null;
                }
                this.getCurrentAddress();
              }}>
              <Image source={Assets.locator} style={styles.pointCurLocImg}/>
            </TouchableOpacity>
          </View>
        </View>
    );
  }
}

export default connect(
  state => {
    return {
      titleSelected: state.navigationReducer.selectedItem,
      pageSelected: state.navigationReducer.selectedPage
    };
  },
  dispatch => {
    return {
      saveNavigationSelection: dataToSave => {
        dispatch(saveNavigationSelection(dataToSave));
      },
      saveNavigationSelectionPage: data => {
        dispatch(saveNavigationSelectionPage(data));
      }
    };
  }
)(NearbyContainer);

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  mapContainer: {
    flex: 1,
    height: height,
    width: width
  },
  pointCurLocView: {
    position: "absolute", 
    bottom: 20, 
    left: 20
  },
  pointCurLocBtn: {
    justifyContent: "center", 
    alignItems: "center", 
    height: 40, 
    width: 40, 
    borderRadius: 50,  
    shadowOffset: { width: 10, height: 10 },
    shadowColor: 'black',
    shadowOpacity: 1,
    elevation: 1
  },
  pointCurLocImg: {
    height: 50, 
    width: 50, 
    resizeMode: "contain"
  }
});