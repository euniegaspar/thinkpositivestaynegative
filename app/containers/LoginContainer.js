import React from "react";
import {
  StatusBar,
  View,
  Text,
  TouchableOpacity,
  Image,
  ImageBackground,
  StyleSheet,
  CheckBox,
  TextInput
} from "react-native";
import Assets from "../assets";
import { getUserToken } from "../utils/AsyncStorageHelper";
import { EDColors } from "../assets/Colors";
import { ETFonts } from "../assets/FontConstants";
import LinearGradient from 'react-native-linear-gradient';

export default class LoginContainer extends React.Component {
  constructor(props) {
    super(props);
    if (Text.defaultProps == null) Text.defaultProps = {};
    Text.defaultProps.allowFontScaling = false;

    this.state = {
      email: "",
      password: "",
      isRememberMe: false,
      isLoading: false
      // isBetaMode: this.props.betaMode == "1" ? true : false
    };
  }

  componentDidMount() {
    getUserToken(success => { }, failure => { });
  } 

  render() {
    const isNoData = this.state.email == "" || this.state.password == "" ? true : false;
    return (
      <ImageBackground
        source={Assets.bgSplash}
        style={styles.imgBackground}>
        <StatusBar 
          barStyle="light-content" 
          backgroundColor="transparent" 
          translucent={true}
        />
        <View style={styles.container}>
          <Image style={styles.logoImg} source={Assets.logo2} />
          <View style={styles.loginContainer}>
            <View style={styles.loginSubContainer}>
              <Text style={styles.dtlsTxt}>Email Address</Text>
              <View style={styles.dtlsTxtBox}>
                <TextInput
                  style={styles.txtInput}
                  maxLength={100}
                  numberOfLines={1}
                  keyboardType="email-address"
                  value={this.state.email}
                  placeholderTextColor={EDColors.textboxColor}
                  onChangeText={text => {
                    this.setState({ email: text });
                  }}>
                </TextInput>
              </View>
              <View style={styles.space} />
              <Text style={styles.dtlsTxt}>Password</Text>
              <View style={styles.dtlsTxtBox}>
                <TextInput
                  style={styles.txtInput}
                  maxLength={100}
                  numberOfLines={1}
                  keyboardType="default"
                  secureTextEntry={true}
                  value={this.state.password}
                  placeholderTextColor={EDColors.textboxColor}
                  onChangeText={text => {
                    this.setState({ password: text });
                  }}>
                </TextInput>
              </View>
              <View style={styles.rmAndFpView}>
                <TouchableOpacity 
                  style={styles.checkBox}
                  onPress={() => {
                    if (this.state.isRememberMe) {
                      this.setState({ isRememberMe: false });
                    } else {
                      this.setState({ isRememberMe: true });
                    }
                  }}>
                  {this.state.isRememberMe && 
                    <View style={styles.checkedBox}>
                      <Image style={styles.checkImg} source={Assets.check}/>
                    </View>
                  }
                </TouchableOpacity>
                <Text style={[styles.labelTxt, { marginRight: 40 }]}>Remember me</Text>
                <Text 
                  style={styles.labelTxt}
                  onPress={() => {
                    this.props.navigation.navigate("ForgotPasswordContainer");
                  }}>
                  Forgot Password?
                </Text>
              </View> 
            </View>
            <TouchableOpacity
              disabled={isNoData}
              style={styles.loginBtn}
              onPress={() => {
                this.props.navigation.navigate("HomeContainer");
              }}>
              <LinearGradient colors={[isNoData ? EDColors.secondaryGrey : EDColors.secondary1Gradient, isNoData ? EDColors.secondaryGrey : EDColors.secondary2Gradient]} start={{x: 0, y: 0}} end={{x: 1, y: 0}} style={styles.loginLG}>
                <Text style={styles.loginTxt}>
                  LOG IN
                </Text>
              </LinearGradient>
            </TouchableOpacity>
            <View style={styles.signUpView}>
              <Text style={styles.labelTxt}>Don't have an account? </Text>
              <Text 
                style={[styles.labelTxt, { textDecorationLine: "underline" }]}
                onPress={() => {
                  this.props.navigation.navigate("SignupContainer");
                }}> 
                Sign Up
              </Text>
            </View>
          </View>
        </View>
      </ImageBackground>
    );
  }
}

const styles = StyleSheet.create({
  imgBackground: {
    width: "100%",
    height: "100%"
  },
  container: {
    flex: 1,
    justifyContent: "center",
    alignContent: "center"
  },
  logoContainer: {
    alignItems: "center",
    justifyContent: "center"
  },
  logoImg: {
    width: 90, 
    height: 90, 
    resizeMode: "contain",
    alignSelf: "center",
    marginBottom: 70
  },
  loginContainer: {
    paddingHorizontal: 60,
    alignItems: "center"
  },
  loginSubContainer: {
    marginBottom: 80 
  },
  dtlsTxt: {
    fontFamily: ETFonts.regular,
    color: EDColors.white,
    fontSize: 15,
    marginBottom: 8
  },
  dtlsTxtBox: {
    backgroundColor: EDColors.white,
    borderRadius: 3
  },
  txtInput: {
    fontFamily: ETFonts.regular,
    paddingHorizontal: 10, 
    paddingVertical: 5,
  },
  space: {
    marginVertical: 5
  },
  rmAndFpView: {
    flexDirection: "row", 
    alignItems: "center",
    marginTop: 10
  },
  checkBox: {
    backgroundColor: EDColors.white,
    height: 13,
    width: 13,
    borderRadius: 3,
    marginRight: 6,
    top: 1,
    alignItems: "center",
    justifyContent: "center"
  },
  checkedBox: {
    backgroundColor: EDColors.green,
    height: 10,
    width: 10,
    borderRadius: 2,
    alignItems: "center",
    justifyContent: "center"
  },
  checkImg: {
    height: 8,
    width: 8,
    resizeMode: "contain",
    tintColor: EDColors.white
  },
  loginBtn: {
    elevation: 5,
    paddingHorizontal: 1,
    borderRadius: 20,
    marginBottom: 10
  },  
  labelTxt: {
    fontFamily: ETFonts.regular, 
    color: EDColors.white, 
    fontSize: 14
  },
  loginLG: {
    paddingHorizontal: 60,
    borderRadius: 20
  },
  signUpView: {
    flexDirection: "row", 
    marginTop: 30
  },
  loginTxt: {
    fontSize: 16,
    fontFamily: ETFonts.bold,
    textAlign: 'center',
    margin: 10,
    color: EDColors.white
  }
})
