import React from "react";
import {
  StatusBar,
  View,
  Text,
  TouchableOpacity,
  Image,
  ImageBackground,
  StyleSheet,
  TextInput
} from "react-native";
import Assets from "../assets";
import { getUserToken } from "../utils/AsyncStorageHelper";
import { EDColors } from "../assets/Colors";
import { ETFonts } from "../assets/FontConstants";
import LinearGradient from 'react-native-linear-gradient';

export default class ForgotPassSuccessContainer extends React.Component {
  constructor(props) {
    super(props);
    if (Text.defaultProps == null) Text.defaultProps = {};
    Text.defaultProps.allowFontScaling = false;

    this.state = {
      isLoading: false,
      // isBetaMode: this.props.betaMode == "1" ? true : false
    };
  }

  componentDidMount() {
  } 

  render() {
    return (
      <ImageBackground
        source={Assets.bgSplash}
        style={styles.imgBackground}>
        <StatusBar 
          barStyle="light-content" 
          backgroundColor="transparent" 
          translucent={true}
        />
        <View style={styles.container}>
          <TouchableOpacity 
            style={styles.backBtn}
            onPress={() => {
              this.props.navigation.goBack();
            }}>
            <Image source={Assets.backarrow} style={styles.backImg} />
          </TouchableOpacity>
          <View style={styles.subContainer}>
            <Text style={styles.emailSentTxt}>Email has been sent!</Text>
            <Text style={styles.confirmSentTxt}>Please check your inbox and click{'\n'}in the received link to reset a password.</Text>
            <TouchableOpacity
              style={styles.loginBtn}
              onPress={() => {
                this.props.navigation.navigate("LoginContainer");
              }}>
              <LinearGradient colors={[EDColors.secondary1Gradient, EDColors.secondary2Gradient]} start={{x: 0, y: 0}} end={{x: 1, y: 0}} style={styles.loginLG}>
                <Text style={styles.loginTxt}>
                  LOG IN
                </Text>
              </LinearGradient>
            </TouchableOpacity> 
            <View style={styles.noLinkView}>
              <View style={styles.flexDirectionRow}>
                <Text style={styles.noNoTxt}>Didn't receive the link? </Text>
                <Text 
                  style={[styles.noNoTxt, { textDecorationLine: "underline" }]}
                  onPress={() => {
                    this.props.navigation.navigate("ForgotPasswordContainer")
                  }}> 
                  Resend
                </Text>
              </View>
            </View> 
            </View>
        </View>
      </ImageBackground>
    );
  }
}

const styles = StyleSheet.create({
  imgBackground: {
    width: "100%",
    height: "100%"
  },
  container: {
    flex: 1,
    justifyContent: "center",
    alignContent: "center"
  },
  flexDirectionRow: {
    flexDirection: "row"
  },
  backBtn: {
    position: "absolute", 
    left: 15, 
    top: 55
  },
  backImg: {
    height: 20, 
    width: 20, 
    resizeMode: "contain"
  },
  subContainer: {
    paddingHorizontal: 60,
  },
  emailSentTxt: {
    color: EDColors.white,
    fontFamily: ETFonts.bold,
    fontSize: 17,
    marginBottom: 20,
    alignSelf: "center"
  },
  confirmSentTxt: {
    color: EDColors.white,
    fontFamily: ETFonts.regular,
    fontSize: 15,
    marginBottom: 45,
    textAlign: "center"
  },
  loginBtn: {
    alignSelf: "center",
    elevation: 5,
    paddingHorizontal: 1,
    borderRadius: 20,
    marginTop: 50
  },
  loginLG: {
    paddingHorizontal: 60,
    borderRadius: 20
  },
  loginTxt: {
    fontSize: 16,
    fontFamily: ETFonts.bold,
    textAlign: 'center',
    margin: 10,
    color: EDColors.white
  },
  noLinkView: {
    alignItems: "center",
    justifyContent: "center",
    marginTop: 40,
    marginBottom: 30
  },
  noNoTxt: {
    fontFamily: ETFonts.regular, 
    color: EDColors.white, 
    fontSize: 14
  }
})
