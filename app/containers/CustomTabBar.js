// created by Eunie - 10/18/21

import React from "react";
import {
  StyleSheet,
  FlatList,
  Image,
  TouchableOpacity,
  Text,
  View
} from "react-native";
import Assets from "../assets"; 
import { connect } from "react-redux";
import { saveNavigationSelection, saveNavigationSelectionPage } from "../redux/actions/Navigation";
import { EDColors } from "../assets/Colors";
import { NavigationActions } from "react-navigation";

class CustomTabBar extends React.PureComponent {
  
  constructor(props) {
    super(props);
    Text.defaultProps = Text.defaultProps || {};
    Text.defaultProps.allowFontScaling = false;
    (this.arrayFinalSideMenu = []),
      (this.sideMenuData = {
        routes: [
          "Home",
          "Appointment",
          "Nearby",
          "Saved",
          "Profile",
        ],
        screenNames: [
          "Home",
          "Appointment",
          "Nearby",
          "Saved",
          "Profile"
        ],
        icons: [
          Assets.home_inactive,
          Assets.appointment_inactive,
          Assets.nearby_inactive,
          Assets.heart_inactive,
          Assets.profile_inactive
        ],
        iconsSelected: [
          Assets.home_active,
          Assets.appointment_active,
          Assets.nearby_active,
          Assets.heart_active,
          Assets.profile_active
        ]
      });
  }

  state = {
    tabName: "Home",
    pageName: ""
  };

  componentDidMount() { 
   this.setState({ tabName: this.props.titleSelected, pageName: this.props.pageSelected });
  }

  componentDidUpdate() {
    this.setState({ tabName: this.props.titleSelected, pageName: this.props.pageSelected });
    console.log("componentDidUpdate ::::::::: "+this.state.pageName);
  }

  render() {
    this.arrayFinalSideMenu = this.sideMenuData.screenNames;
    const showTabBar = (this.state.pageName == "HomeContainer" || this.state.pageName == "AppointmentContainer" 
                    || this.state.pageName == "NearbyContainer" || this.state.pageName == "SavedContainer" 
                    || this.state.pageName == "ProfileContainer") ? true : false;
    return (
      <View style={[styles.container, { display: showTabBar ? "flex" : "none" }]}>
        <View style={styles.subContainer}>
          <FlatList
            horizontal={true}
            showsVerticalScrollIndicator={false}
            data={this.arrayFinalSideMenu}
            extraData={this.state}
            keyExtractor={(item, index) => item + index}
            renderItem={({ item, index }) => {
              if (item != undefined) {
                return (
                  <TouchableOpacity
                    style={styles.navMenuListContainer}
                    onPress={() => {
                      this.props.saveNavigationSelection(
                        this.sideMenuData.routes[index]
                      );
                      this.props.navigation.dispatch(
                        NavigationActions.navigate({
                          routeName: this.sideMenuData.routes[index],
                          params: {
                            routeName: this.arrayFinalSideMenu[index]
                          },
                          action: NavigationActions.navigate({
                            routeName: this.sideMenuData.routes[index],
                            params: {
                              routeName: this.arrayFinalSideMenu[index]
                            } 
                          })
                        })
                      );
                    }}>
                    <Image
                      style={styles.navMenuListImg}
                      source={
                        this.state.tabName == this.sideMenuData.routes[index] 
                          ? this.sideMenuData.iconsSelected[index]
                          : this.sideMenuData.icons[index]
                      }
                      resizeMode="contain"
                    />
                  </TouchableOpacity>
                );
              } else {
                return null;
              }
            }}
          />
        </View>
      </View>
    );
  }
}

export default connect(
  state => {
    return {
      titleSelected: state.navigationReducer.selectedItem,
      pageSelected: state.navigationReducer.selectedPage
    };
  },
  dispatch => {
    return {
      saveNavigationSelection: dataToSave => {
        dispatch(saveNavigationSelection(dataToSave));
      },
      saveNavigationSelectionPage: data => {
        dispatch(saveNavigationSelectionPage(data));
      }
    };
  }
)(CustomTabBar);

export const styles = StyleSheet.create({
  container: {
    height: 65,
    justifyContent: "center",
    alignItems: "center",
    shadowOffset: { width: 10, height: 10 },
    shadowColor: 'black',
    shadowOpacity: 1,
    elevation: 10
  },
  subContainer: {
    width: "100%",
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: EDColors.white,
    shadowOffset: { width: 10, height: 10 },
    shadowColor: 'black',
    shadowOpacity: 1,
    elevation: 10
  },
  navMenuListContainer: {
    flex: 1,
    alignItems: "center",
    marginTop: 20,
    marginBottom: 20,
    marginHorizontal: 25
  },
  navMenuListImg: {
    width: 25, 
    height: 25
  }
});


