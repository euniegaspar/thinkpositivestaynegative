import React from "react";
import {
  View,
  Text,
  TextInput,
  StyleSheet,
} from "react-native";
import { EDColors } from "../assets/Colors";
import { ETFonts } from "../assets/FontConstants";
import { connect } from "react-redux";
import { saveNavigationSelection, saveNavigationSelectionPage } from "../redux/actions/Navigation";
import Assets from "../assets";
import NavBarComponent from "../components/NavBarComponent";

class SavedContainer extends React.Component {
  constructor(props) {
    super(props);
    Text.defaultProps = Text.defaultProps || {};
    Text.defaultProps.allowFontScaling = false;
    TextInput.defaultProps = TextInput.defaultProps || {};
    TextInput.defaultProps.allowFontScaling = false;

    this.state = {}
  }

  componentDidMount() {    
    this.props.saveNavigationSelection("Saved");
    this.props.saveNavigationSelectionPage("SavedContainer");
  }

  render() {
    return (
        <View style={styles.container}>
          <NavBarComponent 
            navtitle={"Saved"}   
            withExtraHeight={false} 
            backButton={() => {
              this.props.saveNavigationSelectionPage("HomeContainer");
              this.props.navigation.goBack();
            }}
          />
        </View>
    );
  }
}

export default connect(
  state => {
    return {
      titleSelected: state.navigationReducer.selectedItem,
      pageSelected: state.navigationReducer.selectedPage
    };
  },
  dispatch => {
    return {
      saveNavigationSelection: dataToSave => {
        dispatch(saveNavigationSelection(dataToSave));
      },
      saveNavigationSelectionPage: data => {
        dispatch(saveNavigationSelectionPage(data));
      }
    };
  }
)(SavedContainer);

const styles = StyleSheet.create({
  container: {
    flex: 1
  }
});