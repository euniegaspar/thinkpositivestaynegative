import React from "react";
import {
  View,
  Text,
  TouchableOpacity,
  StyleSheet,
  ImageBackground,
  Image,
  ScrollView,
  FlatList
} from "react-native";
import Assets from "../assets";
import { EDColors } from "../assets/Colors";
import { connect } from "react-redux";
import { ETFonts } from "../assets/FontConstants";
import ProgressLoader from '../components/ProgressLoader';
import { saveNavigationSelection, saveNavigationSelectionPage } from "../redux/actions/Navigation";
import LinearGradient from 'react-native-linear-gradient';
import StarRating from "react-native-star-rating";

class BookingScheduleContainer extends React.Component {
  constructor(props) {
    super(props);
    if (Text.defaultProps == null) Text.defaultProps = {};
    Text.defaultProps.allowFontScaling = false;

    this.state = {
      availmentType: this.props.navigation.state.params.availmentType,
      testData: this.props.navigation.state.params.testingCenterData,
      isSaved: false,
      serviceType: 0,
      bookingDate: new Date(),
      selectedBookingTime: "",
    };
  }

  componentDidMount() {
    this.props.saveNavigationSelection("Home"); 
    this.props.saveNavigationSelectionPage("BookingScheduleContainer");
  }

  render() {
    const selectedBookingTime = this.state.selectedBookingTime;
    return (
      <View style={styles.container}>
        <ImageBackground
          style={styles.headerImgBckgnd}
          source={this.state.testData.clinicImg}>
          <LinearGradient colors={[EDColors.primary1Gradient, EDColors.primary2Gradient]} start={{x: 0, y: 0}} end={{x: 1, y: 0}} style={styles.headerLG}>
          </LinearGradient>
          <TouchableOpacity
            style={styles.backBtn}
            onPress={() => {
              this.props.saveNavigationSelectionPage("TestingCenterContainer");
              this.props.navigation.goBack();
            }}>
            <Image style={styles.backImg} source={Assets.backarrow}/>
          </TouchableOpacity>
          <View style={styles.detailsView}>
            <View style={styles.testingPodImgView}>
              <Image style={styles.testingPodImg} source={this.state.testData.clinicImg}/>
            </View>
            <Text style={styles.clinicNameTxt}>{this.state.testData.clinicName}</Text>
            <StarRating
              disabled={false}
              starSize={12}
              maxStars={5}
              rating={parseFloat(this.state.testData.rating)}
              emptyStarColor={EDColors.yellowrating}
              fullStarColor={EDColors.yellowrating}
              selectedStar={(rating) => {}}
              starStyle={styles.starRating}
            />
            <View style={styles.detailsBottomView}>
              <View style={{ width: 30 }}/>
              <Text style={styles.clinicAddTxt}>
                {this.state.testData.clinicAdd}
              </Text>
              <TouchableOpacity
                style={styles.heartBtn}
                onPress={() => {
                  this.state.isSaved ? 
                    this.setState({ isSaved: false })
                  : 
                    this.setState({ isSaved: true }) 
                }}>
                <Image source={this.state.isSaved ? Assets.saved_active : Assets.saved_inactive} style={styles.heartImg} />
              </TouchableOpacity>
            </View>
          </View>
        </ImageBackground>
        <ScrollView
          showsVerticalScrollIndicator={false}
          keyboardDismissMode='on-drag'
          style={styles.scrollView}>
          <View style={styles.scheduleView}>
            <Text style={styles.scheduleTxt}>Select Schedule</Text>
            <TouchableOpacity
              style={styles.calendarBtn}
              onPress={() => {}}>
              <Text style={styles.dateTxt}>October 2021</Text>
              <Image source={Assets.arrow_down} style={styles.arrowImg} />
            </TouchableOpacity>
          </View>
          <View style={styles.slotsView}>
            <Text style={styles.slotsLblTxt}>Morning Slots</Text>
            <FlatList
              showsVerticalScrollIndicator={false}
              data={this.state.testData.amslots}
              extraData={this.state.selectedBookingTime}
              numColumns={4} 
              keyExtractor={(item, index) => item + index}
              renderItem={({ item, index }) => (
                <TouchableOpacity
                  style={[styles.slotsBtn, { backgroundColor: selectedBookingTime == item ? EDColors.primary : EDColors.white }]}
                  onPress={() => {
                    this.setState({ selectedBookingTime: item });
                  }}>
                  <Text style={[styles.timeTxt, { color: selectedBookingTime == item ? EDColors.white : EDColors.black }]}>
                    {item}
                  </Text>  
                </TouchableOpacity>
              )}   
            />
            <Text style={[styles.slotsLblTxt, { marginTop: 5 }]}>Afternoon Slots</Text>
            <FlatList
              showsVerticalScrollIndicator={false}
              data={this.state.testData.pmslots}
              extraData={this.state.selectedBookingTime}
              numColumns={4}
              keyExtractor={(item, index) => item + index}
              renderItem={({ item, index }) => (
                <TouchableOpacity
                  style={[styles.slotsBtn, { backgroundColor: selectedBookingTime == item ? EDColors.primary : EDColors.white }]}
                  onPress={() => {
                    this.setState({ selectedBookingTime: item });
                  }}>
                  <Text style={[styles.timeTxt, { color: selectedBookingTime == item ? EDColors.white : EDColors.black }]}>
                    {item}
                  </Text>  
                </TouchableOpacity>
              )}  
            />
          </View>
          <View style={styles.serviceView}>
            <Text style={styles.serviceTxt}>Service Selection</Text>
            <View style={styles.serviceRadioView}>
              <TouchableOpacity style={styles.radioBtn}
                onPress={() => {
                  this.setState({ serviceType: 0 });
                }}>
                {this.state.serviceType == 0 &&
                  <View style={styles.radioRound}/>
                }
              </TouchableOpacity>
              <Text style={styles.selectionTxt}>RT-PCR Test</Text>
              <View style={{ marginRight: 50 }}/>
              <TouchableOpacity style={styles.radioBtn}
                onPress={() => {
                  this.setState({ serviceType: 1 });
                }}>
                {this.state.serviceType == 1 &&
                  <View style={styles.radioRound}/>
                }
              </TouchableOpacity>
              <Text style={styles.selectionTxt}>Antigen Test</Text>
            </View>
          </View>
        </ScrollView>
        <View style={styles.footerView}>
          <View style={styles.footerTopView}>
            <Text style={styles.selectedSrvcTxt}>{this.state.serviceType == 0 ? "RT-PCR Test" : "Antigen Test"}</Text>
            <Text style={styles.totalLblTxt}>Total: </Text>
            <Text style={styles.priceTxt}>₱ 0,000.00</Text>
          </View>
          <TouchableOpacity
            style={styles.nextBtn}
            onPress={() => { 
              this.props.navigation.navigate("BookingInfoContainer", {
                clinicInfoData: this.state.testData,
                availmentType: this.state.availmentType,
                bookingDate: this.state.bookingDate,
                bookingTime: this.state.selectedBookingTime,
                serviceType: this.state.serviceType
              });
            }}>
            <LinearGradient colors={[EDColors.secondary1Gradient, EDColors.secondary2Gradient]} start={{x: 0, y: 0}} end={{x: 1, y: 0}} style={styles.nextLG}>
              <Text style={styles.nextNowTxt}>
                NEXT
              </Text>
            </LinearGradient>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

export default connect(
  state => {
    return {
      titleSelected: state.navigationReducer.selectedItem,
      pageSelected: state.navigationReducer.selectedPage
    };
  },
  dispatch => {
    return {
      saveNavigationSelection: dataToSave => {
        dispatch(saveNavigationSelection(dataToSave));
      },
      saveNavigationSelectionPage: data => {
        dispatch(saveNavigationSelectionPage(data));
      }
    };
  }
)(BookingScheduleContainer);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: EDColors.homeBackground
  },
  headerImgBckgnd: {
    height: 250, 
    width: "100%", 
    resizeMode: "cover", 
    borderBottomLeftRadius: 30, 
    borderBottomRightRadius: 30, 
    overflow: "hidden", 
    elevation: 5
  },
  headerLG: {
    height: 250, 
    width: "100%", 
    opacity: 0.9
  },
  backBtn: {
    position: "absolute", 
    left: 15, 
    top: 45 
  },
  backImg: {
    width: 20, 
    height: 20, 
    resizeMode: "contain"
  },
  detailsView: {
    position: 'absolute', 
    top: 0, 
    left: 0, 
    right: 0, 
    bottom: 20, 
    justifyContent: "flex-end", 
    alignItems: 'center'
  },
  testingPodImgView: {
    borderWidth: 1, 
    borderColor: EDColors.white, 
    borderRadius: 10, 
    overflow: "hidden", 
    marginBottom: 5
  },
  testingPodImg: {
    width: 85, 
    height: 85, 
    resizeMode: "cover"
  },
  clinicNameTxt: {
    fontFamily: ETFonts.bold, 
    color: EDColors.white, 
    marginBottom: 10 
  },
  starRating: {
    marginRight: 5, 
    marginBottom: 10
  },
  detailsBottomView: {
    flexDirection: "row" 
  },
  clinicAddTxt: {
    fontFamily: ETFonts.regular, 
    color: EDColors.white, 
    fontSize: 12, 
    width: "45%", 
    textAlign: "center", 
    marginHorizontal: 30
  },
  heartBtn: {
    height: 35, 
    width: 35 
  },
  heartImg: {
    height: 35, 
    width: 35, 
    resizeMode: "contain"
  },
  scrollView: {
    flex: 1, 
    marginBottom: 125, 
    borderRadius: 10
  },
  scheduleView: {
    backgroundColor: EDColors.white, 
    marginHorizontal: 10, 
    marginVertical: 15, 
    paddingHorizontal: 15, 
    paddingVertical: 10, 
    borderRadius: 10
  },
  scheduleTxt: {
    fontFamily: ETFonts.bold, 
    color: EDColors.black, 
    fontSize: 14
  },
  calendarBtn: {
    flexDirection: "row", 
    alignItems: "center",
    marginTop: 10
  },
  dateTxt: {
    color: EDColors.primary, 
    fontSize: 14, 
    fontFamily: ETFonts.bold, 
    marginRight: 5
  },
  arrowImg: {
    top: 1, 
    width: 10, 
    height: 10, 
    resizeMode: "contain"
  },
  slotsView: {
    marginHorizontal: 20
  },
  slotsLblTxt: {
    fontFamily: ETFonts.bold, 
    color: EDColors.black 
  },
  slotsBtn: {
    flex: 1,
    flexDirection: 'column',
    alignItems: "center",
    paddingVertical: 5,
    margin: 10,
    borderRadius: 3
  },
  timeTxt: {
    fontFamily: ETFonts.bold, 
    fontSize: 13
  },
  serviceView: {
    backgroundColor: EDColors.white, 
    marginHorizontal: 10, 
    marginVertical: 15, 
    paddingHorizontal: 25, 
    paddingVertical: 10, 
    borderRadius: 10
  },
  serviceTxt: {
    fontFamily: ETFonts.bold, 
    color: EDColors.black, 
    marginBottom: 10
  },
  serviceRadioView: {
    flexDirection: "row" 
  },
  radioBtn: {
    height: 20, 
    width: 20, 
    borderWidth: 1, 
    borderColor: EDColors.paleGray, 
    borderRadius: 20, 
    alignItems: "center", 
    justifyContent: "center", 
    marginRight: 10
  },
  radioRound: {
    backgroundColor: EDColors.green, 
    borderRadius: 20, 
    height: 13, 
    width: 13
  },
  selectionTxt: {
    fontFamily: ETFonts.regular, 
    color: EDColors.black, 
    fontSize: 13
  },
  nextBtn: {
    borderRadius: 50,
    overflow: "hidden",
    marginHorizontal: 20,
    marginBottom: 20
  },
  nextLG: {
    padding: 5
  },
  nextNowTxt: {
    fontSize: 18,
    fontFamily: ETFonts.bold,
    textAlign: 'center',
    margin: 5,
    color: EDColors.white
  },
  footerView: {
    position: 'absolute', 
    left: 0, 
    right: 0, 
    bottom: 0, 
    backgroundColor: EDColors.white, 
    elevation: 10
  },
  footerTopView: {
    flexDirection: "row", 
    margin: 20
  },
  selectedSrvcTxt: {
    color: EDColors.black, 
    flex: 1, 
    fontFamily: ETFonts.roboto_regular
  },
  totalLblTxt: {
    fontFamily: ETFonts.roboto_bold, 
    color: EDColors.black, 
    marginRight: 10
  },
  priceTxt: {
    fontFamily: ETFonts.roboto_bold, 
    color: EDColors.primary
  }
});
