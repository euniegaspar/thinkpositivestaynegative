import React from "react";
import {
  StatusBar,
  View,
  Text,
  TouchableOpacity,
  Image,
  ImageBackground,
  StyleSheet,
  TextInput
} from "react-native";
import Assets from "../assets";
import { getUserToken } from "../utils/AsyncStorageHelper";
import { EDColors } from "../assets/Colors";
import { ETFonts } from "../assets/FontConstants";
import LinearGradient from 'react-native-linear-gradient';

export default class SignupContainer extends React.Component {
  constructor(props) {
    super(props);
    if (Text.defaultProps == null) Text.defaultProps = {};
    Text.defaultProps.allowFontScaling = false;

    this.state = {
      lastname: "",
      firstname: "",
      middlename: "",
      email: "",
      mobileno: "",
      isLoading: false
      // isBetaMode: this.props.betaMode == "1" ? true : false
    };
  }

  componentDidMount() {
    getUserToken(success => { }, failure => { });
  } 

  render() {
    const isNoData = this.state.lastname == "" || this.state.firstname == "" || this.state.middlename == "" || this.state.email == "" || this.state.mobileno == "" ? true : false;
    return (
      <ImageBackground
        source={Assets.bgSplash}
        style={styles.imgBackground}>
        <StatusBar 
          barStyle="light-content" 
          backgroundColor="transparent" 
          translucent={true}
        />
        <View style={styles.container}>
          <Image style={styles.logoImg} source={Assets.logo2} />
          <View style={styles.signUpContainer}>
            <View style={styles.signUpSubContainer}>
              <Text style={styles.dtlsTxt}>Last Name</Text>
              <View style={styles.dtlsTxtBox}>
                <TextInput
                  style={styles.txtInput}
                  maxLength={100}
                  numberOfLines={1}
                  keyboardType="default"
                  value={this.state.lastname}
                  placeholderTextColor={EDColors.textboxColor}
                  onChangeText={text => {
                    this.setState({ lastname: text });
                  }}>
                </TextInput>
              </View>
              <View style={styles.space} />
              <Text style={styles.dtlsTxt}>First Name</Text>
              <View style={styles.dtlsTxtBox}>
                <TextInput
                  style={styles.txtInput}
                  maxLength={100}
                  numberOfLines={1}
                  keyboardType="default"
                  value={this.state.firstname}
                  placeholderTextColor={EDColors.textboxColor}
                  onChangeText={text => {
                    this.setState({ firstname: text });
                  }}>
                </TextInput>
              </View>
              <View style={styles.space} />
              <Text style={styles.dtlsTxt}>Middle Name</Text>
              <View style={styles.dtlsTxtBox}>
                <TextInput
                  style={styles.txtInput}
                  maxLength={100}
                  numberOfLines={1}
                  keyboardType="default"
                  value={this.state.middlename}
                  placeholderTextColor={EDColors.textboxColor}
                  onChangeText={text => {
                    this.setState({ middlename: text });
                  }}>
                </TextInput>
              </View>
              <View style={styles.space} />
              <Text style={styles.dtlsTxt}>Email Address</Text>
              <View style={styles.dtlsTxtBox}>
                <TextInput
                  style={styles.txtInput}
                  maxLength={100}
                  numberOfLines={1}
                  keyboardType="email-address"
                  value={this.state.email}
                  placeholderTextColor={EDColors.textboxColor}
                  onChangeText={text => {
                    this.setState({ email: text });
                  }}>
                </TextInput>
              </View>
              <View style={styles.space} />
              <Text style={styles.dtlsTxt}>Mobile Number</Text>
              <View style={styles.dtlsTxtBox}>
                <TextInput
                  style={styles.txtInput}
                  maxLength={100}
                  numberOfLines={1}
                  keyboardType="numeric"
                  value={this.state.mobileno}
                  placeholderTextColor={EDColors.textboxColor}
                  onChangeText={text => {
                    this.setState({ mobileno: text });
                  }}>
                </TextInput>
              </View>
            </View>
            <TouchableOpacity
              disabled={isNoData}
              style={styles.nextBtn}
              onPress={() => {
                this.props.navigation.navigate("SignupPasswordContainer", {
                  lastname: this.state.lastname,
                  firstname: this.state.firstname,
                  middlename: this.state.middlename,
                  email: this.state.email,
                  mobileno: this.state.mobileno
                });
              }}>
              <LinearGradient colors={[isNoData ? EDColors.secondaryGrey : EDColors.secondary1Gradient, isNoData ? EDColors.secondaryGrey : EDColors.secondary2Gradient]} start={{x: 0, y: 0}} end={{x: 1, y: 0}} style={styles.nextLG}>
                <Text style={styles.nextTxt}>
                  NEXT
                </Text>
              </LinearGradient>
            </TouchableOpacity>
            <View style={styles.alreadyLoginView}>
              <Text style={styles.labelTxt}>Already have an account? </Text>
              <Text 
                style={[styles.labelTxt, { textDecorationLine: "underline" }]}
                onPress={() => {
                  this.props.navigation.navigate("LoginContainer")
                }}> 
                Log In
              </Text>
            </View>
          </View>
        </View>
      </ImageBackground>
    );
  }
}

const styles = StyleSheet.create({
  imgBackground: {
    width: "100%",
    height: "100%"
  },
  container: {
    flex: 1,
    justifyContent: "center",
    alignContent: "center"
  },
  logoImg: {
    width: 90, 
    height: 90, 
    resizeMode: "contain",
    alignSelf: "center",
    marginBottom: 40
  },
  signUpContainer: {
    paddingHorizontal: 60
  },
  signUpSubContainer: {
    marginBottom: 50
  },
  dtlsTxt: {
    fontFamily: ETFonts.regular,
    color: EDColors.white,
    fontSize: 15,
    marginBottom: 8
  },
  dtlsTxtBox: {
    backgroundColor: EDColors.white,
    borderRadius: 3
  },
  txtInput: {
    fontFamily: ETFonts.regular,
    paddingHorizontal: 10, 
    paddingVertical: 5,
  },
  space: {
    marginVertical: 5
  },
  labelTxt: {
    fontFamily: ETFonts.regular, 
    color: EDColors.white, 
    fontSize: 14
  },
  nextBtn: {
    alignSelf: "center",
    elevation: 5,
    paddingHorizontal: 1,
    borderRadius: 20,
    marginBottom: 10
  },
  nextLG: {
    paddingHorizontal: 60,
    borderRadius: 20
  },
  nextTxt: {
    fontSize: 16,
    fontFamily: ETFonts.bold,
    textAlign: 'center',
    margin: 10,
    color: EDColors.white
  },
  alreadyLoginView: {
    flexDirection: "row", 
    marginTop: 30,
    alignSelf: "center"
  }
})
