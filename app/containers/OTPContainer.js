import React from "react";
import {
  StatusBar,
  View,
  Text,
  TouchableOpacity,
  Image,
  ImageBackground,
  StyleSheet,
  TextInput
} from "react-native";
import Assets from "../assets";
import { getUserToken } from "../utils/AsyncStorageHelper";
import { EDColors } from "../assets/Colors";
import { ETFonts } from "../assets/FontConstants";
import LinearGradient from 'react-native-linear-gradient';

export default class OTPContainer extends React.Component {
  constructor(props) {
    super(props);
    if (Text.defaultProps == null) Text.defaultProps = {};
    Text.defaultProps.allowFontScaling = false;

    this.state = {
      pin1: "",
      pin2: "",
      pin3: "",
      pin4: "",
      pin5: "",
      pin6: "",
      isLoading: false,
      mobileno: this.props.navigation.state.params.mobileno
      // isBetaMode: this.props.betaMode == "1" ? true : false
    };
  }

  componentDidMount() {
    getUserToken(success => { }, failure => { });
  } 

  render() {
    const { pin1, pin2, pin3, pin4, pin5, pin6 } = this.state;
    const isNoData = pin1 == "" || pin2 == "" || pin3 == "" || pin4 == "" || pin5 == "" || pin6 == "" ? true : false;
    return (
      <ImageBackground
        source={Assets.bgSplash}
        style={styles.imgBackground}>
        <StatusBar 
          barStyle="light-content" 
          backgroundColor="transparent" 
          translucent={true}
        />
        <View style={styles.container}>
          <TouchableOpacity 
            style={styles.backBtn}
            onPress={() => {
              this.props.navigation.goBack();
            }}>
            <Image source={Assets.backarrow} style={styles.backImg} />
          </TouchableOpacity>
          <View style={styles.subContainer}>
            <Text style={styles.otpTxt}>OTP Verification</Text>
            <Text style={styles.enterOtpTxt}>Please enter the verification code{'\n'}sent to {this.state.mobileno}</Text>
            <View style={styles.otpContainer}>
              <View style={styles.dtlsTxtBox}>
                <TextInput
                  style={styles.txtInput}
                  ref={'pin1ref'}
                  returnKeyLabel='done'
                  returnKeyType='done'
                  keyboardType='numeric'
                  onChangeText={(pin1) => {
                    this.setState({ pin1: pin1.replace(/\s/g, '') })
                    if (pin1.replace(/\s/g, '') != '') {
                      this.refs.pin2ref.focus();
                    }
                  }}
                  value={pin1}
                  maxLength={1}>
                </TextInput>
              </View>
              <View style={styles.space} />
              <View style={styles.dtlsTxtBox}>
                <TextInput
                  style={styles.txtInput}
                  ref={'pin2ref'}
                  returnKeyLabel='done'
                  returnKeyType='done'
                  keyboardType='numeric'
                  onChangeText={(pin2) => {
                    this.setState({ pin2: pin2.replace(/\s/g, '') })
                    let regSpace= new RegExp(/\s/);
                    if (regSpace.test(pin2)) {
                      this.refs.pin2ref.focus();
                    } else if (pin2 != '') {
                      this.refs.pin3ref.focus();
                    } else {
                      this.refs.pin1ref.focus();
                    }
                  }}
                  value={pin2}
                  maxLength={1}>
                </TextInput>
              </View>
              <View style={styles.space} />
              <View style={styles.dtlsTxtBox}>
                <TextInput
                  style={styles.txtInput}
                  ref={'pin3ref'}
                  returnKeyLabel='done'
                  returnKeyType='done'
                  keyboardType='numeric'
                  onChangeText={(pin3) => {
                    this.setState({ pin3: pin3.replace(/\s/g, '') })
                    let regSpace= new RegExp(/\s/);
                    if (regSpace.test(pin3)) {
                      this.refs.pin3ref.focus();
                    } else if (pin3 != '') {
                      this.refs.pin4ref.focus();
                    } else {
                      this.refs.pin2ref.focus();
                    }
                  }}
                  value={pin3}
                  maxLength={1}>
                </TextInput>
              </View>
              <View style={styles.space} />
              <View style={styles.dtlsTxtBox}>
                <TextInput
                  style={styles.txtInput}
                  ref={'pin4ref'}
                  returnKeyLabel='done'
                  returnKeyType='done'
                  keyboardType='numeric'
                  onChangeText={(pin4) => {
                    this.setState({ pin4: pin4.replace(/\s/g, '') })
                    let regSpace= new RegExp(/\s/);
                    if (regSpace.test(pin4)) {
                      this.refs.pin4ref.focus();
                    } else if (pin4 != '') {
                      this.refs.pin5ref.focus();
                    } else {
                      this.refs.pin3ref.focus();
                    }
                  }}
                  value={pin4}
                  maxLength={1}>
                </TextInput>
              </View>
              <View style={styles.space} />
              <View style={styles.dtlsTxtBox}>
                <TextInput
                  style={styles.txtInput}
                  ref={'pin5ref'}
                  returnKeyLabel='done'
                  returnKeyType='done'
                  keyboardType='numeric'
                  onChangeText={(pin5) => {
                    this.setState({ pin5: pin5.replace(/\s/g, '') })
                    let regSpace= new RegExp(/\s/);
                    if (regSpace.test(pin5)) {
                      this.refs.pin5ref.focus();
                    } else if (pin5 != '') {
                      this.refs.pin6ref.focus();
                    } else {
                      this.refs.pin4ref.focus();
                    }
                  }}
                  value={pin5}
                  maxLength={1}>
                </TextInput>
              </View>
              <View style={styles.space} />
              <View style={[styles.dtlsTxtBox, { marginRight: 0 }]}>
                <TextInput
                  style={styles.txtInput}
                  ref={'pin6ref'}
                  returnKeyLabel='done'
                  returnKeyType='done'
                  keyboardType='numeric'
                  onChangeText={(pin6) => {
                    this.setState({ pin6: pin6.replace(/\s/g, '') })
                    let regSpace= new RegExp(/\s/);
                    if (regSpace.test(pin6)) {
                      this.refs.pin6ref.focus();
                    } else if (pin6 != '') {
                    } else {
                      this.refs.pin5ref.focus();
                    }
                  }}
                  value={pin6}
                  maxLength={1}>
                </TextInput>
              </View>
            </View>
            <TouchableOpacity
              disabled={isNoData}
              style={styles.verifyBtn}
              onPress={() => {
                this.props.navigation.navigate("OTPSuccessContainer");
              }}>
              <LinearGradient colors={[isNoData ? EDColors.secondaryGrey : EDColors.secondary1Gradient, isNoData ? EDColors.secondaryGrey : EDColors.secondary2Gradient]} start={{x: 0, y: 0}} end={{x: 1, y: 0}} style={styles.verifyLG}>
                <Text style={styles.verifyTxt}>
                  VERIFY
                </Text>
              </LinearGradient>
            </TouchableOpacity>  
            <View style={styles.noOtpView}>
              <View style={styles.flexDirectionRow}>
                <Text style={styles.noOtpTxt}>Didn't receive the OTP? </Text>
                <Text 
                  style={[styles.noOtpTxt, { textDecorationLine: "underline" }]}
                  onPress={() => {
                  }}> 
                  Resend
                </Text>
              </View>
            </View>
          </View>
        </View>
      </ImageBackground>
    );
  }
}

const styles = StyleSheet.create({
  imgBackground: {
    width: "100%",
    height: "100%"
  },
  container: {
    flex: 1,
    justifyContent: "center",
    alignContent: "center"
  },
  flexDirectionRow: {
    flexDirection: "row"
  },
  backBtn: {
    position: "absolute", 
    left: 15, 
    top: 55
  },
  backImg: {
    height: 20, 
    width: 20, 
    resizeMode: "contain"
  },
  subContainer: {
    paddingHorizontal: 60,
    alignItems: "center"
  },
  otpContainer: {
    flexDirection: "row",
    alignSelf: "center",
    marginBottom: 100
  },
  otpTxt: {
    color: EDColors.white,
    fontFamily: ETFonts.bold,
    fontSize: 17,
    marginBottom: 20
  },
  enterOtpTxt: {
    color: EDColors.white,
    fontFamily: ETFonts.regular,
    fontSize: 15,
    marginBottom: 45,
    textAlign: "center"
  },
  dtlsTxtBox: {
    backgroundColor: EDColors.white,
    borderRadius: 6,
    marginRight: 10,
    elevation: 5
  },
  txtInput: {
    textAlign: "center",
    fontFamily: ETFonts.regular,
    paddingHorizontal: 10,
    height: 40,
    color: EDColors.black,
    fontSize: 15
  },
  space: {
    marginVertical: 5
  },
  labelTxt: {
    fontFamily: ETFonts.regular, 
    color: EDColors.white, 
    fontSize: 14
  },
  verifyBtn: {
    alignSelf: "center",
    elevation: 5,
    paddingHorizontal: 1,
    borderRadius: 20,
    marginTop: 10
  },
  verifyLG: {
    paddingHorizontal: 60,
    borderRadius: 20
  },
  verifyTxt: {
    fontSize: 16,
    fontFamily: ETFonts.bold,
    textAlign: 'center',
    margin: 10,
    color: EDColors.white
  },
  noOtpView: {
    alignItems: "center",
    justifyContent: "center",
    marginTop: 30,
    marginBottom: 30
  },
  noOtpTxt: {
    fontFamily: ETFonts.regular, 
    color: EDColors.white, 
    fontSize: 14
  },
})
