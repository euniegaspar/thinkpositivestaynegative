import React from "react";
import {
  StatusBar,
  View,
  Text,
  TouchableOpacity,
  Image,
  ImageBackground,
  StyleSheet,
  TextInput
} from "react-native";
import Assets from "../assets";
import { getUserToken } from "../utils/AsyncStorageHelper";
import { EDColors } from "../assets/Colors";
import { ETFonts } from "../assets/FontConstants";
import LinearGradient from 'react-native-linear-gradient';

export default class ForgotPasswordContainer extends React.Component {
  constructor(props) {
    super(props);
    if (Text.defaultProps == null) Text.defaultProps = {};
    Text.defaultProps.allowFontScaling = false;

    this.state = {
      email: "",
      isLoading: false,
      // isBetaMode: this.props.betaMode == "1" ? true : false
    };
  }

  componentDidMount() {
    getUserToken(success => { }, failure => { });
  } 

  render() {
    const isNoData = this.state.email == "" ? true : false;
    return (
      <ImageBackground
        source={Assets.bgSplash}
        style={styles.imgBackground}>
        <StatusBar 
          barStyle="light-content" 
          backgroundColor="transparent" 
          translucent={true}
        />
        <View style={styles.container}>
          <TouchableOpacity 
            style={styles.backBtn}
            onPress={() => {
              this.props.navigation.goBack();
            }}>
            <Image source={Assets.backarrow} style={styles.backImg} />
          </TouchableOpacity>
          <View style={styles.subContainer}>
            <Text style={styles.fpTxt}>Forgot your password?</Text>
            <Text style={styles.enterFpTxt}>Enter your registered email below{'\n'}to receive password reset instruction</Text>
            <View style={styles.emailContainer}>
              <Text style={styles.emailTxt}>Email Address</Text>
              <View style={styles.emailTxtBox}>
                <TextInput
                  style={styles.txtInput}
                  maxLength={100}
                  numberOfLines={1}
                  keyboardType="email-address"
                  value={this.state.email}
                  placeholderTextColor={EDColors.textboxColor}
                  onChangeText={text => {
                    this.setState({ email: text });
                  }}>
                </TextInput>
              </View>
            </View>
            <TouchableOpacity
              disabled={isNoData}
              style={styles.sendBtn}
              onPress={() => {
                this.props.navigation.navigate("ForgotPassSuccessContainer");
              }}>
              <LinearGradient colors={[isNoData ? EDColors.secondaryGrey : EDColors.secondary1Gradient, isNoData ? EDColors.secondaryGrey : EDColors.secondary2Gradient]} start={{x: 0, y: 0}} end={{x: 1, y: 0}} style={styles.sendLG}>
                <Text style={styles.sendTxt}>
                  SEND
                </Text>
              </LinearGradient>
            </TouchableOpacity>  
            </View>
        </View>
      </ImageBackground>
    );
  }
}

const styles = StyleSheet.create({
  imgBackground: {
    width: "100%",
    height: "100%"
  },
  container: {
    flex: 1,
    justifyContent: "center",
    alignContent: "center"
  },
  flexDirectionRow: {
    flexDirection: "row"
  },
  backBtn: {
    position: "absolute", 
    left: 15, 
    top: 55
  },
  backImg: {
    height: 20, 
    width: 20, 
    resizeMode: "contain"
  },
  subContainer: {
    paddingHorizontal: 60,
  },
  emailContainer: {
    marginBottom: 50
  },
  emailTxt: {
    fontFamily: ETFonts.regular,
    color: EDColors.white,
    fontSize: 15,
    marginBottom: 8
  },
  fpTxt: {
    color: EDColors.white,
    fontFamily: ETFonts.bold,
    fontSize: 17,
    marginBottom: 20,
    alignSelf: "center"
  },
  enterFpTxt: {
    color: EDColors.white,
    fontFamily: ETFonts.regular,
    fontSize: 15,
    marginBottom: 45,
    textAlign: "center"
  },
  emailTxtBox: {
    backgroundColor: EDColors.white,
    borderRadius: 3,
    marginRight: 10
  },
  txtInput: {
    fontFamily: ETFonts.regular,
    paddingHorizontal: 10, 
    paddingVertical: 5,
  },
  sendBtn: {
    alignSelf: "center",
    elevation: 5,
    paddingHorizontal: 1,
    borderRadius: 20,
    marginTop: 10
  },
  sendLG: {
    paddingHorizontal: 60,
    borderRadius: 20
  },
  sendTxt: {
    fontSize: 16,
    fontFamily: ETFonts.bold,
    textAlign: 'center',
    margin: 10,
    color: EDColors.white
  }
})
