import React from "react";
import {
  View,
  Text,
  TouchableOpacity,
  StyleSheet,
  StatusBar,
  ScrollView,
  Image
} from "react-native";
import Assets from "../assets";
import { EDColors } from "../assets/Colors";
import { connect } from "react-redux";
import { ETFonts } from "../assets/FontConstants";
import ProgressLoader from '../components/ProgressLoader';
import { saveNavigationSelection, saveNavigationSelectionPage } from "../redux/actions/Navigation";
import NavBarComponent from "../components/NavBarComponent";
import LinearGradient from 'react-native-linear-gradient';

class BookingSummaryContainer extends React.Component {
  constructor(props) {
    super(props);
    if (Text.defaultProps == null) Text.defaultProps = {};
    Text.defaultProps.allowFontScaling = false;

    this.state = {
      testData: this.props.navigation.state.params.testData,
      bookingDate: this.props.navigation.state.params.bookingDate,
      bookingTime: this.props.navigation.state.params.bookingTime,
      fullName: this.props.navigation.state.params.fullName,
      serviceType: this.props.navigation.state.params.serviceType,
      availmentType: this.props.navigation.state.params.availmentType,
      bookingID: "12345567890"
    };
  }

  componentDidMount() {
    this.props.saveNavigationSelection("Home"); 
    this.props.saveNavigationSelectionPage("BookingSummaryContainer");
  }

  render() {
    return (
      <View style={styles.container}>
        <ScrollView
          showsVerticalScrollIndicator={false}
          keyboardDismissMode='on-drag'
          contentContainerStyle={styles.scrollView}>
          <NavBarComponent 
            navtitle={"Booking Summary"}
            withExtraHeight={true} 
            backButton={() => {
              this.props.saveNavigationSelectionPage("BookingInfoContainer");
              this.props.navigation.goBack();
            }}
          />
          <View style={styles.subContainer}>
            <View style={styles.appntmntDtlsView}>
              <Text style={styles.appntmntDtlsTxt}>Appointment Details</Text>
              <View style={styles.testingPodDtlsView}>
                <View style={styles.testingPodImgView}>
                  <Image style={styles.testingPodImg} source={Assets.singlepod}/>
                </View>
                <View>
                  <Text>{this.state.testData.clinicName}</Text>
                  <View>
                    <Text>
                      Location
                    </Text>
                    <Text>
                      {this.state.testData.clinicAdd}
                    </Text>
                  </View>
                </View>
              </View>
              <View>
                <Text>Time and Date</Text>
                <Text>{this.state.bookingDate} - {this.state.bookingTime}</Text>
              </View>
              <View>
                <Text>Booking ID:</Text>
                <Text>{this.state.bookingID}</Text>
              </View>
            </View>
            <View style={[styles.appntmntDtlsView, { marginTop: 10 }]}>
              <Text style={styles.appntmntDtlsTxt}>Patient's Information</Text>
              <View>
                <Text>
                </Text>
                <Text>
                </Text>
              </View>
              <View>
                <Text>
                </Text>
                <Text>
                </Text>
              </View>
              <View>
                <Text>
                </Text>
                <Text>
                </Text>
              </View>
            </View>
            <View style={[styles.appntmntDtlsView, { marginTop: 10 }]}>
              <Text style={styles.appntmntDtlsTxt}>Data Privacy and Affidavit of Undertaking</Text>
              <Text>
              </Text>
              <Text>
              </Text>
              <Text>
              </Text>  
            </View>
          </View>
        </ScrollView>
        <View style={styles.footerView}>
          <View style={styles.footerTopView}>
            <Text style={styles.selectedSrvcTxt}>{this.state.serviceType == 0 ? "RT-PCR Test" : "Antigen Test"}</Text>
            <Text style={styles.totalLblTxt}>Total: </Text>
            <Text style={styles.priceTxt}>₱ 0,000.00</Text>
          </View>
          <TouchableOpacity
            style={styles.nextBtn}
            onPress={() => { 
            }}>
            <LinearGradient colors={[EDColors.secondary1Gradient, EDColors.secondary2Gradient]} start={{x: 0, y: 0}} end={{x: 1, y: 0}} style={styles.nextLG}>
              <Text style={styles.nextNowTxt}>
                PROCEED TO PAYMENT
              </Text>
            </LinearGradient>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

export default connect(
  state => {
    return {
      titleSelected: state.navigationReducer.selectedItem,
      pageSelected: state.navigationReducer.selectedPage
    };
  },
  dispatch => {
    return {
      saveNavigationSelection: dataToSave => {
        dispatch(saveNavigationSelection(dataToSave));
      },
      saveNavigationSelectionPage: data => {
        dispatch(saveNavigationSelectionPage(data));
      }
    };
  }
)(BookingSummaryContainer);

const styles = StyleSheet.create({
  container: {
    flex: 1, 
    backgroundColor: EDColors.homeBackground
  },
  scrollView: {
    flex: 1
  },
  subContainer: {
    marginHorizontal: 20
  },
  appntmntDtlsView: {
    backgroundColor: EDColors.white, 
    borderRadius: 20, 
    top: -45, 
    padding: 15
  },
  appntmntDtlsTxt: {
    fontFamily: ETFonts.bold,
    color: EDColors.black,
    fontSize: 15
  },
  testingPodDtlsView: {
    flexDirection: "row",
    marginTop: 10,
    marginHorizontal: 3
  },
  testingPodImgView: {
    width: 80,
    height: 80,
    borderRadius: 8,
    overflow: "hidden"
  },
  testingPodImg: {
    width: "100%",
    height: "100%",
    resizeMode: "cover"
  },
  footerView: {
    position: 'absolute', 
    left: 0, 
    right: 0, 
    bottom: 0, 
    backgroundColor: EDColors.white, 
    elevation: 10
  },
  footerTopView: {
    flexDirection: "row", 
    margin: 20
  },
  selectedSrvcTxt: {
    color: EDColors.black, 
    flex: 1, 
    fontFamily: ETFonts.roboto_regular
  },
  totalLblTxt: {
    fontFamily: ETFonts.roboto_bold, 
    color: EDColors.black, 
    marginRight: 10
  },
  priceTxt: {
    fontFamily: ETFonts.roboto_bold, 
    color: EDColors.primary
  },
  nextBtn: {
    borderRadius: 50,
    overflow: "hidden",
    marginHorizontal: 20,
    marginBottom: 20
  },
  nextLG: {
    padding: 5
  },
  nextNowTxt: {
    fontSize: 18,
    fontFamily: ETFonts.bold,
    textAlign: 'center',
    margin: 5,
    color: EDColors.white
  },

});
