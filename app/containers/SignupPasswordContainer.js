import React from "react";
import {
  StatusBar,
  View,
  Text,
  TouchableOpacity,
  Image,
  ImageBackground,
  StyleSheet,
  TextInput
} from "react-native";
import Assets from "../assets";
import { getUserToken } from "../utils/AsyncStorageHelper";
import { EDColors } from "../assets/Colors";
import { ETFonts } from "../assets/FontConstants";
import LinearGradient from 'react-native-linear-gradient';

export default class SignupPasswordContainer extends React.Component {
  constructor(props) {
    super(props);
    if (Text.defaultProps == null) Text.defaultProps = {};
    Text.defaultProps.allowFontScaling = false;

    this.state = {
      password: "",
      confirmpassword: "",
      isLoading: false,
      lastname: this.props.navigation.state.params.lastname,
      firstname: this.props.navigation.state.params.firstname,
      middlename: this.props.navigation.state.params.middlename,
      email: this.props.navigation.state.params.email,
      mobileno: this.props.navigation.state.params.mobileno,
      // isBetaMode: this.props.betaMode == "1" ? true : false
    };
  }

  componentDidMount() {
    getUserToken(success => { }, failure => { });
  } 

  render() {
    const isNoData = this.state.password == "" || this.state.confirmpassword == "" ? true : false;
    return (
      <ImageBackground
        source={Assets.bgSplash}
        style={styles.imgBackground}>
        <StatusBar 
          barStyle="light-content" 
          backgroundColor="transparent" 
          translucent={true}
        />
        <View style={styles.container}>
          <TouchableOpacity 
            style={styles.backBtn}
            onPress={() => {
              this.props.navigation.goBack();
            }}>
            <Image source={Assets.backarrow} style={styles.backImg} />
          </TouchableOpacity>
          <Image style={styles.logoImg} source={Assets.logo2} />
          <View style={styles.signUpContainer}>
            <View style={styles.signUpSubContainer}>
              <Text style={styles.dtlsTxt}>Password</Text>
              <View style={styles.dtlsTxtBox}>
                <TextInput
                  style={styles.txtInput}
                  maxLength={100}
                  numberOfLines={1}
                  keyboardType="default"
                  secureTextEntry={true}
                  value={this.state.password}
                  placeholderTextColor={EDColors.textboxColor}
                  onChangeText={text => {
                    this.setState({ password: text });
                  }}>
                </TextInput>
              </View>
              <View style={styles.space} />
              <Text style={styles.dtlsTxt}>Confirm Password</Text>
              <View style={styles.dtlsTxtBox}>
                <TextInput
                  style={styles.txtInput}
                  maxLength={100}
                  numberOfLines={1}
                  keyboardType="default"
                  secureTextEntry={true}
                  value={this.state.confirmpassword}
                  placeholderTextColor={EDColors.textboxColor}
                  onChangeText={text => {
                    this.setState({ confirmpassword: text });
                  }}>
                </TextInput>
              </View>
            </View>
            <View style={styles.termsView}>
              <View style={styles.flexDirectionRow}>
                <Text style={styles.tacTxt}>By clicking Continue, you agree to our </Text>
                <Text 
                  style={[styles.tacTxt, { textDecorationLine: "underline" }]}
                  onPress={() => {
                    this.props.navigation.navigate("LoginContainer")
                  }}> 
                  Terms of Service
                </Text>
              </View>
              <View style={styles.flexDirectionRow}>
                <Text style={styles.tacTxt}> and that you have read our </Text>
                <Text 
                  style={[styles.tacTxt, { textDecorationLine: "underline" }]}
                  onPress={() => {
                    this.props.navigation.navigate("LoginContainer")
                  }}> 
                  Privacy Policy.
                </Text>
              </View>
            </View>
            <TouchableOpacity
              disabled={isNoData}
              style={styles.continueBtn}
              onPress={() => {
                this.props.navigation.navigate("OTPContainer", {
                  mobileno: this.state.mobileno
                });
              }}>
              <LinearGradient colors={[isNoData ? EDColors.secondaryGrey : EDColors.secondary1Gradient, isNoData ? EDColors.secondaryGrey : EDColors.secondary2Gradient]} start={{x: 0, y: 0}} end={{x: 1, y: 0}} style={styles.continueLG}>
                <Text style={styles.continueTxt}>
                  CONTINUE
                </Text>
              </LinearGradient>
            </TouchableOpacity>  
            </View>
        </View>
      </ImageBackground>
    );
  }
}

const styles = StyleSheet.create({
  imgBackground: {
    width: "100%",
    height: "100%"
  },
  container: {
    flex: 1,
    justifyContent: "center",
    alignContent: "center"
  },
  flexDirectionRow: {
    flexDirection: "row"
  },
  backBtn: {
    position: "absolute", 
    left: 15, 
    top: 55
  },
  backImg: {
    height: 20, 
    width: 20, 
    resizeMode: "contain"
  },
  logoImg: {
    width: 90, 
    height: 90, 
    resizeMode: "contain",
    alignSelf: "center",
    marginBottom: 40
  },
  signUpContainer: {
    paddingHorizontal: 60
  },
  signUpSubContainer: {
    marginBottom: 100
  },
  dtlsTxt: {
    fontFamily: ETFonts.regular,
    color: EDColors.white,
    fontSize: 15,
    marginBottom: 8
  },
  dtlsTxtBox: {
    backgroundColor: EDColors.white,
    borderRadius: 3
  },
  txtInput: {
    fontFamily: ETFonts.regular,
    paddingHorizontal: 10, 
    paddingVertical: 5,
  },
  space: {
    marginVertical: 5
  },
  labelTxt: {
    fontFamily: ETFonts.regular, 
    color: EDColors.white, 
    fontSize: 14
  },
  tacTxt: {
    fontFamily: ETFonts.regular, 
    color: EDColors.white, 
    fontSize: 13
  },
  continueBtn: {
    alignSelf: "center",
    elevation: 5,
    paddingHorizontal: 1,
    borderRadius: 20,
    marginTop: 10
  },
  continueLG: {
    paddingHorizontal: 60,
    borderRadius: 20
  },
  continueTxt: {
    fontSize: 16,
    fontFamily: ETFonts.bold,
    textAlign: 'center',
    margin: 10,
    color: EDColors.white
  },
  termsView: {
    alignItems: "center",
    justifyContent: "center",
    marginTop: 30,
    marginBottom: 30
  }
})
