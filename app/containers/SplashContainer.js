import React from "react";
import {
  View,
  Image,
  Platform,
  AppState,
  Linking,
  ImageBackground,
  StyleSheet,
  Text,
  StatusBar
} from "react-native";
import Assets from "../assets";
import { PermissionsAndroid } from "react-native";
import { getUserToken, getUserFCM } from "../utils/AsyncStorageHelper";
import { StackActions, NavigationActions } from "react-navigation";
import { connect } from "react-redux";
import { saveUserDetailsInRedux, saveUserFCMInRedux } from "../redux/actions/User";
import { saveBetaModeInRedux } from "../redux/actions/BetaMode";
import { NOTIFICATION_TYPE } from "../utils/Constants";
import NavigationService from "../../NavigationService";
import LocationServicesDialogBox from "react-native-android-location-services-dialog-box";
import { showDialogue } from "../utils/CMAlert";
import Geolocation from "@react-native-community/geolocation";
import { ETFonts } from "../assets/FontConstants";
import { EDColors } from "../assets/Colors";
import { getSecretSettings } from "../utils/AsyncStorageHelper";

var redirectType = "";

class SplashContainer extends React.Component {
  constructor(props) {
    super(props);

    this.userObj = undefined;
    redirectType = this.props.screenProps;
    this.isOpenSetting = false
  }

  state = {
    isRefresh: false,
    isLocationAccess: true,
    appState: AppState.currentState,
    isBetaMode: this.props.betaMode == "1" ? true : false,
    isShow: false
  };

  componentDidMount() {
    AppState.addEventListener('change', this._handleAppStateChange);
    if (Platform.OS == "android") {
      this.requestLocationPermission(this.props)
    } else {
      this.checkLocationIOS()
    }
    try{ // 1 beta 0 prod
      getSecretSettings(success => {
        if(success == "1"){
          this.props.saveBetaMode("1");
        } else {
          this.props.saveBetaMode("0");
        }
      }, failure => {
        this.props.saveBetaMode("0");
      });
    }catch (e){
      this.props.saveBetaMode("0");
    }
    this.setState({ isShow: true });
  }

  componentWillUnmount() {
    AppState.removeEventListener('change', this._handleAppStateChange);
    if (Platform.OS == "android") {
     LocationServicesDialogBox.stopListener(); 
    }
  }

  _handleAppStateChange = (nextAppState) => {
    if (this.state.appState.match(/inactive|background/) && nextAppState === 'active') {
      if (this.isOpenSetting == true) {
        this.checkLocationIOS()
      }
    }
    this.setState({ appState: nextAppState });
  }

  checkLocationIOS() {
    Geolocation.getCurrentPosition(() => {
      console.log("ios GRANTED")
      this.requestLocationPermission(this.props);
    },
      () => {
        console.log("ios DENIED")
        showDialogue("Please allow location access from setting", [{
          "text": "OK", onPress: () => {
            this.isOpenSetting = true
            Linking.openURL("app-settings:");
          }
        }])

      },
    )
  }

  _checkData = (props) => {
    setTimeout(() => {
      getUserToken(
        success => {
          if (
            success != undefined &&
            success.PhoneNumber != "" &&
            success.PhoneNumber != undefined
          ) {
            props.saveCredentials(success);
            if (redirectType === NOTIFICATION_TYPE) {
              redirectType = undefined;
              NavigationService.navigate("Notification");
            } else {
              getUserFCM(
                success => {
                  props.saveToken(success)
                  props.navigation.dispatch(
                    StackActions.reset({
                      index: 0,
                      actions: [
                        NavigationActions.navigate({ routeName: "HomeContainer" })
                      ]
                    })
                  );
                },
                failure => {
                }
              )
            }
          } else {
            props.navigation.dispatch(
              StackActions.reset({
                index: 0,
                actions: [
                  NavigationActions.navigate({ routeName: "LoginContainer" })
                ]
              })
            );
          }
        },
        failure => {
          props.navigation.dispatch(
            StackActions.reset({
              index: 0,
              actions: [
                NavigationActions.navigate({ routeName: "LoginContainer" })
              ]
            })
          );
        }
      );

      if (
        this.userObj != undefined &&
        this.userObj.PhoneNumber != "" &&
        this.userObj.PhoneNumber != undefined
      ) {
      } else {
      }
    }, 2000);
  }

  requestLocationPermission = async (prop) => {
    if (Platform.OS == "ios") {
      this._checkData(prop);
      return;
    }
   else
    {
      this._checkData(prop);
      return;
    }
  }

  requestLocationPermission() {
    try {
        PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION)
        .then(onfulfilled => {
            if (onfulfilled === PermissionsAndroid.RESULTS.GRANTED) {
                this.allowGeolocation();
            } else if(onfulfilled === PermissionsAndroid.RESULTS.NEVER_ASK_AGAIN) {
                BackgroundGeolocation.showAppSettings();
            } else {
                BackgroundGeolocation.showLocationSettings();
            }
        })
        .catch(onrejected => {
            this.props.onRejected(onrejected);
        });
    } catch (err) {
        console.warn(err);
    }
  }

  allowGeolocation() {
    let config = {
      desiredAccuracy: BackgroundGeolocation.HIGH_ACCURACY,
      distanceFilter: 5,
      debug: false,
      startOnBoot: false,
      stopOnTerminate: true,
      notificationsEnabled: false,
      locationProvider: BackgroundGeolocation.DISTANCE_FILTER_PROVIDER,
      interval: 5000,
      fastestInterval: 2000,
      activitiesInterval: 10000,
      stopOnStillActivity: false
    }

    try {
        BackgroundGeolocation.configure(config, success => {
          console.log(success);
        }, failure => {
          console.log(failure);
        });

        BackgroundGeolocation.checkStatus(status => {
          if(status.locationServicesEnabled == false) {
            BackgroundGeolocation.showLocationSettings();
          }
        });

        BackgroundGeolocation.getCurrentLocation(location => {
          this.props.onLocation(location);
        });
    } catch (error) {
        console.log(error);
    }
  }
  
  render() {
    return (
      <ImageBackground 
        source={Assets.bgSplash}
        style={styles.imgBackground}>
        <StatusBar 
          barStyle="light-content" 
          backgroundColor="transparent" 
          translucent={true}
        />
        <View style={styles.container}>
          <Image
            style={styles.imgLogo}
            source={Assets.logo}
          />
        </View>
        <View style={styles.detailsTxt}>
            <Text style={[styles.text, { fontSize: 11, fontFamily: ETFonts.regular }]}>A COVID-19 ANTIGEN AND RT PCR SWAB TESTING POD</Text>
            <Text style={[styles.text, { fontSize: 25, fontFamily: ETFonts.bold }]}>#PaTestKaMuna</Text>
            <Text style={[styles.text, { fontSize: 11, fontFamily: ETFonts.regular }]}>A national movement for affordable, accessible, {'\n'} and convenient ANTIGEN Swab and RT PCR Swab Testing facilities.</Text>
          </View>
      </ImageBackground>
    );
  }
}

export default connect(
  state => {
    return {
      betaMode: state.betaModeOperation.betaMode
    };
  },
  dispatch => {
    return {
      saveCredentials: detailToSave => {
        dispatch(saveUserDetailsInRedux(detailToSave));
      },
      saveToken: token => {
        dispatch(saveUserFCMInRedux(token))
      },
      saveBetaMode: betaMode => {
        dispatch(saveBetaModeInRedux(betaMode));
      }
    };
  }
)(SplashContainer);

const styles = StyleSheet.create({
  imgBackground: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    width: "100%",
    height: "100%"
  },
  container: {
    alignItems: "center", 
    justifyContent: "center", 
    marginHorizontal: 25
  },
  imgLogo: {
    width: 150, 
    height: 150,
    resizeMode: "contain"
  },
  detailsTxt: {
    marginTop: 50,
    alignItems: "center", 
    justifyContent: "center",
  },
  text: {
    color: EDColors.white,
    textAlign: "center",
    marginBottom: 5
  }
})
