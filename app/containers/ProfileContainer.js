import React from "react";
import {
  View,
  Text,
  Image,
  TouchableOpacity,
  StyleSheet,
} from "react-native";
import Assets from "../assets";
import { EDColors } from "../assets/Colors";
import { connect } from "react-redux";
import { ETFonts } from "../assets/FontConstants";
import ProgressLoader from '../components/ProgressLoader';
import { saveNavigationSelection, saveNavigationSelectionPage } from "../redux/actions/Navigation";

class ProfileContainer extends React.Component {
  constructor(props) {
    super(props);
    if (Text.defaultProps == null) Text.defaultProps = {};
    Text.defaultProps.allowFontScaling = false;
  }

  state = {
    name: "Juan Dela Cruz"
  };
  
  componentDidMount() {
    this.props.saveNavigationSelection("Profile"); 
    this.props.saveNavigationSelectionPage("ProfileContainer");
  }

  render() {
    return (
     <View style={styles.container}>
     
     </View>
    );
  }
}

export default connect(
  state => {
    return {
        titleSelected: state.navigationReducer.selectedItem,
        pageSelected: state.navigationReducer.selectedPage
    };
  },
  dispatch => {
    return {
        saveNavigationSelection: dataToSave => {
            dispatch(saveNavigationSelection(dataToSave));
        },
        saveNavigationSelectionPage: data => {
            dispatch(saveNavigationSelectionPage(data));
        }
    };
  }
)(ProfileContainer);

const styles = StyleSheet.create({
  container: {
    flex: 1, 
    backgroundColor: EDColors.homeBackground
  }
});
