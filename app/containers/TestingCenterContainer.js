import React from "react";
import {
  View,
  Text,
  TouchableOpacity,
  StyleSheet,
  StatusBar,
  ScrollView,
  FlatList,
  Image
} from "react-native";
import Assets from "../assets";
import { EDColors } from "../assets/Colors";
import { connect } from "react-redux";
import { ETFonts } from "../assets/FontConstants";
import ProgressLoader from '../components/ProgressLoader';
import { saveNavigationSelection, saveNavigationSelectionPage } from "../redux/actions/Navigation";
import NavBarComponent from "../components/NavBarComponent";
import TestingCenterListCard from "../components/TestingCenterListCard";
import LinearGradient from 'react-native-linear-gradient';

class TestingCenterContainer extends React.Component {
  constructor(props) {
    super(props);
    if (Text.defaultProps == null) Text.defaultProps = {};
    Text.defaultProps.allowFontScaling = false;

    this.state = {
      isOnsite: true,
      isDriveThru: false,
      availmentType: this.props.navigation.state.params.availmentType
    };
  }

  componentDidMount() {
    this.props.saveNavigationSelection("Home"); 
    this.props.saveNavigationSelectionPage("TestingCenterContainer");
  }

  render() {
    const testingCenterOnsite = [
      { id: 1, clinicName: "TPSN Single Testing Pod", clinicAdd: "1964 Drive, M. Carreon Street Sta. Ana, Manila", 
        rating: 5, clinicImg: Assets.singlepod, category: "Onsite", 
        amslots: ["8:00 AM", "8:30 AM", "9:00 AM", "9:30 AM", "10:00 AM", "10:30 AM", "11:00 AM", "11:30 AM"],
        pmslots: ["12:00 PM", "12:30 PM", "1:00 PM", "1:30 PM", "2:00 PM", "2:30 PM", "3:00 PM", "3:30 PM", "4:00 PM", "4:30 PM", "5:00 PM", "5:30 PM"]
      },
      { id: 2, clinicName: "TPSN Mobile Testing Pod", clinicAdd: "98-C Rosa Alvero Street, Loyola Heights, Quezon City", 
        rating: 3.5, clinicImg: Assets.mobilepod, category: "Onsite",
        amslots: ["8:00 AM", "8:30 AM", "9:00 AM", "9:30 AM", "10:00 AM", "10:30 AM", "11:00 AM", "11:30 AM"],
        pmslots: ["12:00 PM", "12:30 PM", "1:00 PM", "1:30 PM", "2:00 PM", "2:30 PM", "3:00 PM", "3:30 PM", "4:00 PM", "4:30 PM", "5:00 PM", "5:30 PM"]
      },
      { id: 3, clinicName: "TPSN Single Testing Pod", clinicAdd: "98-C Rosa Alvero Street, Loyola Heights, Quezon City", 
        rating: 3, clinicImg: Assets.singlepod, category: "Onsite",
        amslots: ["8:00 AM", "8:30 AM", "9:00 AM", "9:30 AM", "10:00 AM", "10:30 AM", "11:00 AM", "11:30 AM"],
        pmslots: ["12:00 PM", "12:30 PM", "1:00 PM", "1:30 PM", "2:00 PM", "2:30 PM", "3:00 PM", "3:30 PM", "4:00 PM", "4:30 PM", "5:00 PM", "5:30 PM"]
      },
      { id: 4, clinicName: "TPSN Single Testing Pod", clinicAdd: "98-C Rosa Alvero Street, Loyola Heights, Quezon City", 
        rating: 4.5, clinicImg: Assets.singlepod, category: "Onsite",
        amslots: ["8:00 AM", "8:30 AM", "9:00 AM", "9:30 AM", "10:00 AM", "10:30 AM", "11:00 AM", "11:30 AM"],
        pmslots: ["12:00 PM", "12:30 PM", "1:00 PM", "1:30 PM", "2:00 PM", "2:30 PM", "3:00 PM", "3:30 PM", "4:00 PM", "4:30 PM", "5:00 PM", "5:30 PM"]
      },
      { id: 5, clinicName: "TPSN Single Testing Pod", clinicAdd: "98-C Rosa Alvero Street, Loyola Heights, Quezon City", 
        rating: 5, clinicImg: Assets.singlepod, category: "Onsite",
        amslots: ["8:00 AM", "8:30 AM", "9:00 AM", "9:30 AM", "10:00 AM", "10:30 AM", "11:00 AM", "11:30 AM"],
        pmslots: ["12:00 PM", "12:30 PM", "1:00 PM", "1:30 PM", "2:00 PM", "2:30 PM", "3:00 PM", "3:30 PM", "4:00 PM", "4:30 PM", "5:00 PM", "5:30 PM"]
      },
      { id: 6, clinicName: "TPSN Single Testing Pod", clinicAdd: "98-C Rosa Alvero Street, Loyola Heights, Quezon City", 
        rating: 5, clinicImg: Assets.singlepod, category: "Onsite",
        amslots: ["8:00 AM", "8:30 AM", "9:00 AM", "9:30 AM", "10:00 AM", "10:30 AM", "11:00 AM", "11:30 AM"],
        pmslots: ["12:00 PM", "12:30 PM", "1:00 PM", "1:30 PM", "2:00 PM", "2:30 PM", "3:00 PM", "3:30 PM", "4:00 PM", "4:30 PM", "5:00 PM", "5:30 PM"]
      },
      { id: 7, clinicName: "TPSN Mobile Testing Pod", clinicAdd: "98-C Rosa Alvero Street, Loyola Heights, Quezon City", 
        rating: 1, clinicImg: Assets.mobilepod, category: "Onsite",
        amslots: ["8:00 AM", "8:30 AM", "9:00 AM", "9:30 AM", "10:00 AM", "10:30 AM", "11:00 AM", "11:30 AM"],
        pmslots: ["12:00 PM", "12:30 PM", "1:00 PM", "1:30 PM", "2:00 PM", "2:30 PM", "3:00 PM", "3:30 PM", "4:00 PM", "4:30 PM", "5:00 PM", "5:30 PM"]
      },
      { id: 8, clinicName: "TPSN Single Testing Pod", clinicAdd: "98-C Rosa Alvero Street, Loyola Heights, Quezon City", 
        rating: 4, clinicImg: Assets.singlepod, category: "Onsite",
        amslots: ["8:00 AM", "8:30 AM", "9:00 AM", "9:30 AM", "10:00 AM", "10:30 AM", "11:00 AM", "11:30 AM"],
        pmslots: ["12:00 PM", "12:30 PM", "1:00 PM", "1:30 PM", "2:00 PM", "2:30 PM", "3:00 PM", "3:30 PM", "4:00 PM", "4:30 PM", "5:00 PM", "5:30 PM"]
      },
    ];
    const testingCenterDriveThru = [
      { id: 1, clinicName: "TPSN Drive-Thru Testing Pod", clinicAdd: "1964 Drive, M. Carreon Street Sta. Ana, Manila", 
        rating: 5, clinicImg: Assets.drivethru, category: "DriveThru",
        amslots: ["8:00 AM", "8:30 AM", "9:00 AM", "9:30 AM", "10:00 AM", "10:30 AM", "11:00 AM", "11:30 AM"],
        pmslots: ["12:00 PM", "12:30 PM", "1:00 PM", "1:30 PM", "2:00 PM", "2:30 PM", "3:00 PM", "3:30 PM", "4:00 PM", "4:30 PM", "5:00 PM", "5:30 PM"]
      },
      { id: 2, clinicName: "TPSN Drive-Thru Testing Pod", clinicAdd: "98-C Rosa Alvero Street, Loyola Heights, Quezon City", 
        rating: 3.5, clinicImg: Assets.drivethru, category: "DriveThru",
        amslots: ["8:00 AM", "8:30 AM", "9:00 AM", "9:30 AM", "10:00 AM", "10:30 AM", "11:00 AM", "11:30 AM"],
        pmslots: ["12:00 PM", "12:30 PM", "1:00 PM", "1:30 PM", "2:00 PM", "2:30 PM", "3:00 PM", "3:30 PM", "4:00 PM", "4:30 PM", "5:00 PM", "5:30 PM"]
      },
      { id: 3, clinicName: "TPSN Drive-Thru Testing Pod", clinicAdd: "98-C Rosa Alvero Street, Loyola Heights, Quezon City", 
        rating: 3, clinicImg: Assets.drivethru, category: "DriveThru",
        amslots: ["8:00 AM", "8:30 AM", "9:00 AM", "9:30 AM", "10:00 AM", "10:30 AM", "11:00 AM", "11:30 AM"],
        pmslots: ["12:00 PM", "12:30 PM", "1:00 PM", "1:30 PM", "2:00 PM", "2:30 PM", "3:00 PM", "3:30 PM", "4:00 PM", "4:30 PM", "5:00 PM", "5:30 PM"]
      },
      { id: 4, clinicName: "TPSN Drive-Thru Testing Pod", clinicAdd: "98-C Rosa Alvero Street, Loyola Heights, Quezon City", 
        rating: 4.5, clinicImg: Assets.drivethru, category: "DriveThru",
        amslots: ["8:00 AM", "8:30 AM", "9:00 AM", "9:30 AM", "10:00 AM", "10:30 AM", "11:00 AM", "11:30 AM"],
        pmslots: ["12:00 PM", "12:30 PM", "1:00 PM", "1:30 PM", "2:00 PM", "2:30 PM", "3:00 PM", "3:30 PM", "4:00 PM", "4:30 PM", "5:00 PM", "5:30 PM"]
      },
      { id: 5, clinicName: "TPSN Drive-Thru Testing Pod", clinicAdd: "98-C Rosa Alvero Street, Loyola Heights, Quezon City", 
        rating: 5, clinicImg: Assets.drivethru, category: "DriveThru",
        amslots: ["8:00 AM", "8:30 AM", "9:00 AM", "9:30 AM", "10:00 AM", "10:30 AM", "11:00 AM", "11:30 AM"],
        pmslots: ["12:00 PM", "12:30 PM", "1:00 PM", "1:30 PM", "2:00 PM", "2:30 PM", "3:00 PM", "3:30 PM", "4:00 PM", "4:30 PM", "5:00 PM", "5:30 PM"]
      }
    ];
    // const testingCenterOnsite = undefined;
    // const testingCenterDriveThru = undefined;
    const testingCenterData = this.state.isOnsite ? testingCenterOnsite : testingCenterDriveThru;
    const noData = testingCenterData != undefined && testingCenterData != null && testingCenterData.length > 0 ? false : true;
    return (
      <View style={styles.container}>
        <NavBarComponent 
          navtitle={"Select Testing Center"} 
          withExtraHeight={false} 
          backButton={() => {
            this.props.saveNavigationSelectionPage("HomeContainer");
            this.props.navigation.goBack();
          }}
        />
        <View style={styles.tabView}>
          <TouchableOpacity
            style={[styles.tabBtn, { borderBottomWidth: this.state.isOnsite ? 4 : 0, borderColor: this.state.isOnsite ? EDColors.secondary : EDColors.white }]}
            onPress={() => {
              this.setState({ isOnsite: true, isDriveThru: false });
            }}>
            <Text style={[styles.tabTitleTxt, { color: this.state.isOnsite ? EDColors.secondary : EDColors.primary }]}>ON-SITE</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={[styles.tabBtn, { borderBottomWidth: this.state.isDriveThru ? 4 : 0, borderColor: this.state.isDriveThru ? EDColors.secondary : EDColors.white }]}
            onPress={() => {
              this.setState({ isOnsite: false, isDriveThru: true });
            }}>
            <Text style={[styles.tabTitleTxt, { color: this.state.isDriveThru ? EDColors.secondary : EDColors.primary }]}>DRIVE-THRU</Text>
          </TouchableOpacity> 
        </View>
        <ScrollView
          showsVerticalScrollIndicator={false}
          keyboardDismissMode='on-drag'
          contentContainerStyle={[styles.clinicContainer, (noData ? { alignItems: "center", justifyContent: "center" } : { paddingHorizontal: 10, paddingVertical: 15 })]}>
          {!noData ? (
            <View>
              <FlatList
                horizontal={false}
                showsVerticalScrollIndicator={false}
                data={testingCenterData}
                keyExtractor={(item, index) => item + index}
                renderItem={({ item, index }) => {
                  if (item != undefined) {
                    return (
                      <TestingCenterListCard 
                        clinicImg={item.clinicImg}
                        clinicName={item.clinicName}
                        clinicAdd={item.clinicAdd}
                        rating={item.rating}
                        onPress={() => {
                          this.props.navigation.navigate("BookingScheduleContainer", {
                            availmentType: this.props.availmentType,
                            testingCenterData: item
                          });
                        }}
                      />
                      );
                    } else {
                      return null;
                    }
                  }}
                />
                <View style={styles.floatingMenu}>
                  <LinearGradient colors={[EDColors.primary1Gradient, EDColors.primary2Gradient]} start={{x: 0, y: 0}} end={{x: 1, y: 0}} style={styles.floatingMenuLG}>
                    <View style={styles.rowCenterView}>
                      <TouchableOpacity
                        style={styles.rowCenterView}
                        onPress={() => {
                          this.props.navigation.navigate("NearbyContainer");
                        }}>
                        <Image style={styles.mapImg} source={Assets.map}/>
                        <Text style={styles.mapTxt}>
                          Map
                        </Text>
                      </TouchableOpacity>  
                      <Text style={styles.lineSeparatorTxt}> | </Text>
                      <TouchableOpacity
                        style={styles.rowCenterView}
                        onPress={() => {
                          this.props.navigation.navigate("SavedContainer");
                        }}>
                        <Image style={styles.savedImg} source={Assets.heart_active}/>
                        <Text style={styles.savedTxt}>
                          Saved
                        </Text>
                      </TouchableOpacity>  
                    </View> 
                  </LinearGradient>
                </View>
            </View>
            ) : (
              <View style={styles.noDataView}>
                <Image style={styles.hospitalImg} source={Assets.hospital} />
                <Text style={styles.noBookingTxt}>No {this.state.isOnsite ? "On-site" : "Drive-thru" } testing centers.</Text>
              </View>         
            )}
        </ScrollView>
      </View>
    );
  }
}

export default connect(
  state => {
    return {
      titleSelected: state.navigationReducer.selectedItem,
      pageSelected: state.navigationReducer.selectedPage
    };
  },
  dispatch => {
    return {
      saveNavigationSelection: dataToSave => {
        dispatch(saveNavigationSelection(dataToSave));
      },
      saveNavigationSelectionPage: data => {
        dispatch(saveNavigationSelectionPage(data));
      }
    };
  }
)(TestingCenterContainer);

const styles = StyleSheet.create({
  container: {
    flex: 1, 
    backgroundColor: EDColors.homeBackground
  },
  tabView: {
    flexDirection: "row", 
    backgroundColor: EDColors.white, 
    paddingTop: 15,
    elevation: 5
  },
  tabBtn: {
    width: "50%", 
    alignItems: "center", 
    paddingBottom: 10, 
  },
  tabTitleTxt: {
    fontSize: 13, 
    textAlign: "center",
    fontFamily: ETFonts.bold
  },
  clinicContainer: {
    flex: 1
  },
  noDataView: {
    alignItems: "center",
    top: -20 
  },
  hospitalImg: {
    width: 200, 
    height: 160, 
    resizeMode: "contain", 
    marginBottom: 30
  },
  noBookingTxt: {
    fontFamily: ETFonts.semibold, 
    fontSize: 15, 
    color: EDColors.primaryGrey
  },
  floatingMenu: {
    position: "absolute",
    top: 0, 
    bottom: 50, 
    right: 0, 
    left: 0, 
    alignItems: "center", 
    justifyContent: "flex-end"
  },
  floatingMenuLG: {
    paddingVertical: 5, 
    paddingHorizontal: 10, 
    borderRadius: 20, 
    elevation: 5, 
    overflow: "hidden"
  },
  rowCenterView: {
    flexDirection: "row", 
    alignItems: "center"
  },
  mapImg: {
    height: 20, 
    width: 20, 
    resizeMode: 'contain', 
    marginRight: 8, 
    marginLeft: 5
  },
  mapTxt: {
    fontFamily: ETFonts.regular, 
    color: EDColors.white, 
    fontSize: 15, 
    marginRight: 5
  },
  lineSeparatorTxt: {
    color: EDColors.white, 
    fontSize: 20, 
    marginRight: 5
  },
  savedImg: {
    height: 17, 
    width: 17, 
    resizeMode: 'contain', 
    tintColor: EDColors.white, 
    marginRight: 8, 
    top: 1
  },
  savedTxt: {
    fontFamily: ETFonts.regular, 
    color: EDColors.white, 
    fontSize: 15, 
    marginRight: 5 
  }
});
