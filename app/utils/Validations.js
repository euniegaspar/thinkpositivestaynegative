import { Messages } from "../utils/Messages";

export default class Validations {
  isUrl(strToCheck) {
    var regexp = /(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/;
    return regexp.test(strToCheck);
  }
  // This function is used for checking type od EDTextField and accordingly secures the text entry
  checkingFieldType = fieldType => {
    if (fieldType === "password") {
      return true;
    } else {
      return false;
    }
  };

  checkForEmpty = text => {
    if (text.length == 0) {
      return {
        isEmpty: true,
        validationErrorMessage: "asdasdasdasasdas"
      };
    }
  };

  // Function for performing email validations
  validateEmail = text => {
    console.log(text);

    let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    
    if (reg.test(text) === false) {
      return {
        validEmail: false,
        validationErrorMessage: Messages.validEmail
      };
    } else {
      return {
        validEmail: true,
        validationErrorMessage: "Email is Correct"
      };
    }
  };

  // Function for performing Password validations
  validatePassword = text => {


    let reg = /^([a-zA-Z0-9@*#]{8,15})$/;

    if (reg.test(text) === false) {
      return {
        validPassword: false,
        validationErrorMessage: Messages.validPassword
      };
    } else {
      return {
        validPassword: true,
        validationErrorMessage: "Password is Correct"
      };
    }
  };
}

export function versionChecker(currentVersion, latestVersion) {
  var valid = false;
  var oldParts = currentVersion.split('.');
  var newParts = latestVersion.split('.');
  for (var i = 0;i < newParts.length; i++) {
    let a = ~~newParts[i];
    let b = ~~oldParts[i];
    if(a > b) valid = true;
    if(a < b) valid = false;
  }
  return valid;
}

export function compareVersions(v1, comparator, v2) {
  var comparator = comparator == '=' ? '==' : comparator;
  if(['==','===','<','<=','>','>=','!=','!=='].indexOf(comparator) == -1) {
      throw new Error('Invalid comparator. ' + comparator);
  }
  var v1parts = v1.split('.'), v2parts = v2.split('.');
  var maxLen = Math.max(v1parts.length, v2parts.length);
  var part1, part2;
  var cmp = 0;
  for(var i = 0; i < maxLen && !cmp; i++) {
      part1 = parseInt(v1parts[i], 10) || 0;
      part2 = parseInt(v2parts[i], 10) || 0;
      if(part1 < part2)
          cmp = 1;
      if(part1 > part2)
          cmp = -1;
  }
  return eval('0' + comparator + cmp);
}
