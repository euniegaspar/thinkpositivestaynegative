import AsyncStorage from "@react-native-community/async-storage";

export async function flushAllData(onSuccess, onFailure) {
  try {
    let keys = ['restID', 'lastNotification', 'login', 'cartList', 'defaultAddress', 'restaurants'];
      await AsyncStorage.multiRemove(keys).then(
      response => onSuccess(response),
      error => onFailure(error)
    )
  } catch (error) {
    onFailure(error);
  }
}

export async function saveUserLogin(details, onSuccess, onFailure) {
  try {
    await AsyncStorage.setItem("login", JSON.stringify(details)).then(
      response => onSuccess(response),
      error => onFailure(error)
    );
  } catch (error) {
    onFailure(error);
  }
}

export async function saveUserFCM(details, onSuccess, onFailure) {
  try {
    await AsyncStorage.setItem("token", JSON.stringify(details)).then(
      response => onSuccess(response),
      error => onFailure(error)
    );
  } catch (error) {
    onFailure(error);
  }
}

export async function saveCurrentLocation(details, onSuccess, onFailure) {
  try {
    await AsyncStorage.setItem("current_location", JSON.stringify(details)).then(
      response => onSuccess(response),
      error => onFailure(error)
    );
  } catch (error) {
    onFailure(error);
  }
}

export async function getUserFCM(onSuccess, onFailure) {
  try {
    AsyncStorage.getItem("token").then(response => {
      if (response != "" && response != null && response != undefined) {
        onSuccess(JSON.parse(response));
      } else {
        onFailure("Token Null");
      }
    }, error => onFailure(error));
  } catch (error) {
    onFailure(error);
  }
}

export async function clearLogin(onSuccess, onError) {
  try {
    await AsyncStorage.removeItem("login").then(response => {
      onSuccess(response);
    }, error => {
      onError(error);
    }
    );
  } catch (error) {
    onError(error);
  }
}

export async function getUserToken(onSuccess, onFailure) {
  try {
    await AsyncStorage.getItem("login").then(response => {
      if (response != "" && response != null && response != undefined) {
        onSuccess(JSON.parse(response));
      } else {
        onFailure("Token Null");
      }
    }, error => onFailure(error));
  } catch (error) {
    onFailure(error);
  }
}

export async function clearCartData(onSuccess, onError) {
  try {
    await AsyncStorage.removeItem("cartList").then(response => {
      onSuccess(response);
    }, error => {
      onError(error);
    });
  } catch (error) {
    onError(error);
  }
}

export async function getCurrentLocation(onSuccess, onRestaurantIdNotFound, onFailure) {
  try {
    await AsyncStorage.getItem("current_location").then(response => {
      if (response != "" && response != null && response != undefined) {
        onSuccess(JSON.parse(response));
      } else {
        onRestaurantIdNotFound("Token Null");
      }
    }, error => {
      onFailure(error);
    });
  } catch (error) {
    onFailure(error);
  }
}

export async function saveDefaultAddress(details, onSuccess, onFailure) {
  try {
    await AsyncStorage.setItem("defaultAddress", JSON.stringify(details)).then(
      response => onSuccess(response),
      error => onFailure(error)
    );
  } catch (error) {
    onFailure(error);
  }
}

export async function getDefaultAddress(onSuccess, onFailure) {
  try {
    await AsyncStorage.getItem("defaultAddress").then(response => {
      if (response != "" && response != null && response != undefined) {
        onSuccess(JSON.parse(response));
      } else {
        onFailure("Default address null");
      }
    }, error => {
      onFailure(error)
    });
  } catch (error) {
    onFailure(error);
  }
}

export async function clearDefaultAddress(onSuccess, onError) {
  try {
    await AsyncStorage.removeItem("defaultAddress").then(response => {
      onSuccess(response);
    }, error => {
      onError(error);
    }
    );
  } catch (error) {
    onError(error);
  }
}


export async function removeCurrentLocation(onSuccess, onFailure) {
  try {
    await AsyncStorage.removeItem("current_location").then(response => {
      onSuccess(response);
    }, error => {
      onFailure(error)
    });
  } catch (error) {
    onFailure(error);
  }
}

export async function saveSecretSettings(details, onSuccess, onFailure) {
  try {
    await AsyncStorage.setItem("isBeta", details).then(
      response => onSuccess(response),
      error => onFailure(error)
    );
  } catch (error) {
    onFailure(error);
  }
}

export async function getSecretSettings(onSuccess, onFailure) {
  try {
    await AsyncStorage.getItem("isBeta").then(response => {
      if (response != "" && response != null && response != undefined) {
        onSuccess(JSON.parse(response));
      } else {
        onFailure("Null");
      }
    }, error => {
      onFailure(error)
    });
  } catch (error) {
    onFailure(error);
  }
}








