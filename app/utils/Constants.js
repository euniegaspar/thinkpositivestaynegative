import Moment from "moment";
import { getSecretSettings } from "./AsyncStorageHelper";
import { Alert } from "react-native";
import AsyncStorage from "@react-native-community/async-storage";

// API URL CONSTANTS

// export const MQTT_BROKER_URL = "mqtt://13.213.3.201";
// export const MQTT_BROKER_PORT = 1883;
// export const MQTT_USER = "admin";
// export const MQTT_PASSWORD = "hive1234";

export const BASE_URL = "https://rest.tpsn.com/v1/api/";
export const BASE_URL_OTP = "https://rest.tpsn.com/v1/sms_twilio_api/";
export const BASE_URL_BETA = "https://beta.tpsn.com/v1/api/";
export const BASE_URL_PAYMENT = "https://paynets.lbteksystems.com/api/payment/";
export const MQTT_BROKER_URL = "broker.hivemq.com";
export const MQTT_BROKER_PORT = 8000;

export const REGISTRATION_URL = BASE_URL + "registration";
export const LOGIN_URL = BASE_URL + "login";
export const LOGOUT_URL = BASE_URL + "logout";

export const REGISTRATION_URL_BETA = BASE_URL_BETA + "registration";
export const LOGIN_URL_BETA = BASE_URL_BETA + "login";
export const LOGOUT_URL_BETA = BASE_URL_BETA + "logout";

export const PESO_SIGN = "₱ ";
export const PLUS_SIGN = "\uFF0B";
export const MINUS_SIGN = "\uFF0D";
export const SMALL_ORDER_FEE = 0;
export const DEBUG = false;

//Paynets
export const GET_PAYMENT_METHODS = BASE_URL_PAYMENT + "getPaymentMethod";
export const PAY_ME = BASE_URL_PAYMENT + "payme";

// ALERT CONSTANTS
export const RESERVE_STATIC = "/reserve";
export const DOCUMENTS_STATIC = "/documents";
export const APP_NAME = "Think Positive, Stay Negative";
export const DEFAULT_ALERT_TITLE = APP_NAME;
export const AlertButtons = {
  ok: "OK",
  cancel: "Cancel",
  notNow: "Not now",
  yes: "Yes",
  no: "No"
};

// REQUESTS CONSTANTS
export const RequestKeys = {
  contentType: "Content-Type",
  json: "application/json",
  authorization: "Authorization",
  bearer: "Bearer"
};

// STORAGE CONSTANTS
export const StorageKeys = {
  user_details: "UserDetails"
};

// REDUX CONSTANTS
export const ACCESS_TOKEN = "ACCESS_TOKEN";
export const RESPONSE_FAIL = 0;
export const RESPONSE_SUCCESS = 1;
export const COUPON_ERROR = 2;
export const GOOGLE_API_KEY = "AIzaSyDRlhoc8E5pUxskyC5nFmoMLQzoL_8ExMc";
export const TOMTOM_API_KEY = "PqxmBQj25N2XkBXLhtxuRgp40p5rxXNH";

//NOTIFICATION_TYPE
export const ORDER_TYPE = "orderNotification";
export const NOTIFICATION_TYPE = "notification";
export const DEFAULT_TYPE = "default";

//CMS PAGE
export const ABOUT_US = 1;
export const CONTACT_US = 2;
export const PRIVACY_POLICY = 3;

export const funGetTime = date => {
  var d = new Date(date);
  return Moment(d).format("LT");
};

export const checkNull = data => {
  if (data != undefined && data != "") {
    return data;
  } else {
    return;
  }
};

export const funGetDate = date => {
  var d = new Date(date);
  return Moment(d).format("DD-MM-YYYY");
};

export const funGetFullDate = date => {
  // var d = new Date(date);
  return Moment(date).format("Do MMMM, YYYY");
};

export const funGetFullDate2 = date => {
  // var d = new Date(date);
  return Moment(date).format("MMMM DD, YYYY");
};

export const funGetTomorrowDate = () => {
  var d = new Date();
  var newDate = Moment(d).add(1, "day");

  return new Date(newDate);
};

export function funGetDateStr(date, formats) {
  if (formats == undefined) {
    formats = "DD-MM-YYYY";
  }
  Moment.locale("en");

  var d = new Date("" + date.replaceAll("-", "/"));

  return Moment(d).format(formats);
}

export function funGetTimeStr(date) {
  Moment.locale("en");

  var d = new Date("" + date.replaceAll("-", "/"));

  return Moment(d).format("LT");
}

String.prototype.replaceAll = function(search, replacement) {
  var target = this;
  return target.split(search).join(replacement);
};

export function removeChars(str) {
  var target = "";

  var strArray = str.split(",");
  const rowLen = strArray.length;
  try {
    if (strArray != undefined)
      strArray.map((data, i) => {
        if (data.trim() === "" || data.trim() === undefined) {
        } else {
          if (rowLen === i + 1) {
            target = target + data;
          } else {
            target = target + data + ",";
          }
        }
      });
  } catch (e) {}
  return target;
}

export function capitalize(string) {
  return string.charAt(0).toUpperCase() + string.slice(1);
}

export function capitalizeAll(string) {
  return string.toUpperCase();
}

export function capiString(str) {
  var splitStr = str.toLowerCase().split(" ");
  for (var i = 0; i < splitStr.length; i++) {
    // You do not need to check if i is larger than splitStr length, as your for does that for you
    // Assign it back to the array
    splitStr[i] =
      splitStr[i].charAt(0).toUpperCase() + splitStr[i].substring(1);
  }
  // Directly return the joined string
  return splitStr.join(" ");
}


export function tConv24(time24) {
  var ts = time24;
  var H = +ts.substr(0, 2);
  var h = (H % 12) || 12;
  h = (h < 10)?("0"+h):h;  // leading 0 at the left for 1 digit hours
  var ampm = H < 12 ? " AM" : " PM";
  ts = h + ts.substr(2, 3) + ampm;
  return ts;
};
