import LocationServicesDialogBox from "react-native-android-location-services-dialog-box";
import Geocoder from 'react-native-geocoding';
import Geolocation from '@react-native-community/geolocation';
import { DeviceEventEmitter } from "react-native";

export const isLocationEnable = (onSuccess, onError, onBackPress) => {
  LocationServicesDialogBox.checkLocationServicesIsEnabled({
    message:
      "<h2>Use Location ?</h2>This app wants to change your device settings:<br/><br/>Use GPS, Wi-Fi, and cell network for location<br/><br/>",
    ok: "YES",
    cancel: "NO",
    enableHighAccuracy: true, // true => GPS AND NETWORK PROVIDER, false => GPS OR NETWORK PROVIDER
    showDialog: true, // false => Opens the Location access page directly
    openLocationServices: true, // false => Directly catch method is called if location services are turned off
    preventOutSideTouch: false, //true => To prevent the location services popup from closing when it is clicked outside
    preventBackClick: false, //true => To prevent the location services popup from closing when it is clicked back button
    providerListener: true // true ==> Trigger "locationProviderStatusChange" listener when the location state changes
  })
    .then(success => {
      onSuccess(success);
    })
    .catch(error => {
      onError(error);
      console.log(error.message);
    });
};

export const getLocation = () => {
  return new Promise(
      (resolve, reject) => {
          Geolocation.getCurrentPosition(
            position => {
              console.log("get current location", position);
              resolve({ latitude: position.coords.latitude, longitude: position.coords.longitude });
            },
            error => {
              console.log("get current location", error);
              reject(error);
            },
            { enableHighAccuracy: true, timeout: 10000, maximumAge: 1000 }
          );
      }
  );
}

export const getWatchLocation = (onSuccess, onFailed) => {
  return Geolocation.watchPosition(position => {
    onSuccess({ latitude: position.coords.latitude, longitude: position.coords.longitude });
  },
  error => onFailed(error));
}

export const stopGetWatchLocation = (watchID) => {
  watchID != null && Geolocation.clearWatch(watchID);
}

export const stopObserveLocation = () => {
  Geolocation.stopObserving();
}

export const getGeocodeLocationByName = (locationName) => {
  return new Promise(
      (resolve, reject) => {
          Geocoder.from(locationName)
              .then(json => {
                  console.log(json);
                  var city = null;
                  var zipCode = null;
                  if (json.results.length !== 0) {
                    city = json.results[0].address_components.filter(
                      x => x.types.filter(t => t == "administrative_area_level_1").length > 0
                    )
                    city = city.length !== 0 ? city[0].long_name : "";
          
                    zipCode = json.results[0].address_components.filter(
                      x => x.types.filter(t => t == "postal_code").length > 0
                    )
                    zipCode = zipCode.length !== 0 ? zipCode[0].short_name : "";
                  }
                  var strAddress = json.results[0].formatted_address;
                  resolve({strAddress: strAddress, city: city, zipCode: zipCode});
              })
              .catch(error => reject(error));
      }
  );
}

export const getGeocodeLocationByCoords = (lat, long) => {
  return new Promise(
      (resolve, reject) => {
          Geocoder.from(lat, long)
              .then(json => {
                console.log("addresses", json);
                var city = null;
                var zipCode = null;
                if (json.results.length !== 0) {
                  city = json.results[0].address_components.filter(
                    x => x.types.filter(t => t == "administrative_area_level_1").length > 0
                  )
                  city = city.length !== 0 ? city[0].long_name : "";
        
                  zipCode = json.results[0].address_components.filter(
                    x => x.types.filter(t => t == "postal_code").length > 0
                  )
                  zipCode = zipCode.length !== 0 ? zipCode[0].short_name : "";
                }
                var strAddress = json.results[0].formatted_address;
                resolve({strAddress: strAddress, city: city, zipCode: zipCode});
              })
              .catch(error => reject(error));
      }
  );
}
