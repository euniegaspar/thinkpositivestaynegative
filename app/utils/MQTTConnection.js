import init from 'react_native_mqtt';
import MQTT from 'sp-react-native-mqtt';
import AsyncStorage from '@react-native-community/async-storage'
import DeviceInfo from 'react-native-device-info';
import { MQTT_USER, MQTT_PASSWORD } from './Constants';

init({
    size: 10000,
    storageBackend: AsyncStorage,
    defaultExpires: 1000 * 3600 * 24,
    enableCache: true,
    reconnect: true,
    sync: {
    }
});

const defaultConnectionOptions = {
    reconnect: true,
    cleanSession: true,
    mqttVersion: 3,
    keepAliveInterval: 60,
    timeout: 60
}

const IS_TCP_CONNECTION = false;

export default class MQTTConnection {
    constructor() {
        this.mqtt = null;
        this.QOS = 1,
        this.RETAIN = true;
    }

    connect(host, port, options = null) {
        if(options) {
            this.QOS = options.qos;
            this.RETAIN = options.retain;
        }

        let currentTime = +new Date();
        let clientID = currentTime + DeviceInfo.getUniqueId();
        console.log('ClientID: ', clientID);

        if(IS_TCP_CONNECTION) {
            const options = {
                uri: `${host+':'+port}`,
                clientId: clientID,
                keepalive: 60,
                protocol: 'mqtt',
                protocolLevel: 3,
                clean: true,
                auth: true,
                user: MQTT_USER,
                pass: MQTT_PASSWORD,
                automaticReconnect: true
            };
            MQTT.createClient(options).then(this.onMQTTSuccess).catch(this.onMQTTFailure);
        } else {
            this.mqtt = new Paho.MQTT.Client(host, port, clientID);
            this.mqtt.onConnectionLost = (error) => {
                this.onMQTTLost(error);
            }
            this.mqtt.onMessageArrived = (message) => {
                this.onMQTTMessageArrived(message);
            }
            this.mqtt.onMessageDelivered = (message) => {
                this.onMQTTMessageDelivered(message);
            }

            const connectOptions = options ? options : defaultConnectionOptions;

            this.mqtt.connect({
                onSuccess: this.onMQTTSuccess,
                onFailure: this.onMQTTFailure,
                ...connectOptions
            })
        }
    }

    onMQTTSuccess = (success) => {
        if(IS_TCP_CONNECTION) {
            this.mqtt = success;
            success.on('closed', this.onMQTTClose);
            success.on('error', this.onMQTTLost);
            success.on('message', this.onMQTTMessageArrived);
            success.on('connect', this.onMQTTConnect);
            success.connect();
        } else {
            this.onMQTTConnect(success);
        }
    }

    onMQTTFailure = (error) => {
        this.onMQTTLost(error);
    }

    onMQTTClose = () => {
        this.onMQTTCloseConnection();
    }

    subscribeChannel(channel, onMQTTSuccessSubscription, onMQTTFailureSubscription) {
        if(IS_TCP_CONNECTION) {
            if(!this.mqtt) return false;
            this.mqtt.on('connect', function() {
                this.mqtt.subscribe(channel, this.QOS);
            });
        } else {
            if(!this.mqtt || !this.mqtt.isConnected()) {
                return false;
            }
            this.mqtt.subscribe(channel, 
                { 
                    qos: this.QOS, 
                    onSuccess: onMQTTSuccessSubscription,
                    onFailure: onMQTTFailureSubscription
                }
            );
        }
    }

    unsubscribeChannel(channel, onMQTTSuccessUnsubscription, onMQTTFailureUnsubscription) {
        if(IS_TCP_CONNECTION) {
            if(!this.mqtt) return;
            this.mqtt.on('connect', function() {
                this.mqtt.unsubscribe(channel);
            });
        } else {
            if(!this.mqtt || !this.mqtt.isConnected()) {
                return;
            }
            this.mqtt.unsubscribe(channel, 
                { 
                    qos: this.QOS, 
                    onSuccess: onMQTTSuccessUnsubscription,
                    onFailure: onMQTTFailureUnsubscription
                }
            );
        }
    }

    send(channel = null, payload) {
        if(IS_TCP_CONNECTION) {
            if(!this.mqtt) return false;
    
            if(!channel || !payload) {
                return false;
            }

            console.log(`MQTTConnection send publish channel: ${channel}, payload: ${payload}, qos: ${this.QOS}, retain: ${this.RETAIN}`);
            this.mqtt.on('connect', function() {
                this.mqtt.publish(channel, payload, this.QOS, this.RETAIN);
            });
        } else {
            if(!this.mqtt || !this.mqtt.isConnected()) {
                return;
            }
    
            if(!channel || !payload) {
                return false;
            }
    
            console.log(`MQTTConnection send publish channel: ${channel}, payload: ${payload}, qos: ${this.QOS}, retain: ${this.RETAIN}`);
            this.mqtt.publish(channel, payload, this.QOS, this.RETAIN);
        }
    }

    close() {
        if(IS_TCP_CONNECTION) {
            console.log(`MQTTConnection is connected and successfully closed`);
            if(this.mqtt) {
                this.mqtt.disconnect();
            }
        } else {
            console.log(`MQTTConnection ${this.mqtt.isConnected()} and successfully closed`);
            if(this.mqtt && this.mqtt.isConnected()) {
                this.mqtt && this.mqtt.disconnect();
                this.mqtt = null;
            }
        }
    }
}

MQTTConnection.prototype.onMQTTConnect = null;
MQTTConnection.prototype.onMQTTLost = null;
MQTTConnection.prototype.onMQTTCloseConnection = null;
MQTTConnection.prototype.onMQTTMessageArrived = null;
MQTTConnection.prototype.onMQTTMessageDelivered = null;