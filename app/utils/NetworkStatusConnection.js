// import { NetInfo } from "react-native";
import NetInfo from "@react-native-community/netinfo";

export const netStatus = (callback) => {
  NetInfo.fetch().then(state => {
    callback(state.isConnected);
  });
};

export const netStatusEvent = (callback) => {
  NetInfo.addEventListener(status => {
    callback(status);
  });
};

// export const netStatusEvent = (callback) => {
//   NetInfo.addEventListener("connectionChange", status => {
//     callback(status);
//   });
// };

