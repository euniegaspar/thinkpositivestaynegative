import { NavigationActions, StackActions } from "react-navigation";

let _navigator;

function setTopLevelNavigator(navigatorRef) {
  _navigator = navigatorRef;
}

function navigateToSpecificRoute(specifiedRouteName, param) {
  _navigator.dispatch(
    StackActions.reset({
      index: 0,
      key: null,
      actions: [NavigationActions.navigate({
        routeName: ("HomeContainer"),
        params: (param != undefined && param != null) ? param : null
      })]
    })
  );

  _navigator.dispatch(
    NavigationActions.navigate({
      routeName: specifiedRouteName,
      params: (param != undefined && param != null) ? param : null
    })
  );
}

function navigate(routeName, action) {
  _navigator.dispatch(
    NavigationActions.navigate({
      routeName,
      action,
    }));
}

export default {
  navigate,
  setTopLevelNavigator,
  navigateToSpecificRoute
};